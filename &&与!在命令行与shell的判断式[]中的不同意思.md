---
title: &&与||在命令行与shell的判断式[]中的不同意思
date: 2021-07-26 16:37:43
tags: 
categories: 解决错误问题
---

<!--more-->

在命令行情境下：

> command1&&command2 
> 当command1执行成功即返回值0才执行command2
>
>command1||command2
>当command2执行错误即返回值非0才执行command2
> 
> command1&&command2||command3

[]判断式下:

> [ "zz" == "zz" -a "zz" == "zzzzz"  ]相当于[ "zz" == "zz" ] && [ "zz" == "zzzzz"]
> [ "zz" == "zz" -o "zz" == "zzzzz"  ]相当于[ "zz" == "zz" ] || [ "zz" == "zzzzz"]

