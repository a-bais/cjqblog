---
title: ansible的copy模块的content和backup探究
date: 2021-08-01 11:07:31
tags: 
categories: 解决错误问题 ansible学习笔记
---

<!--more-->

copy常用的参数:
src
dest
backup
content
group
owner
mode

注意content参数使用时，src参数不能存在
如:

> ansible web -i inventory.ini -m copy -a "dest=~/test content='yesyes' group=apache owner=apache mode=0644 backup=yes"

值得留意的还有content可以和backup一起使用，即目标文件覆盖同时又给它修改前进行备份，备份的文件名通常带有时间

