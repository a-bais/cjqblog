---
title: linux四剑客(核心基础)与正则表达式
date: 2022-04-11 00:48:56
tags: shell 正则表达式
categories: 
---

<!--more-->

@[TOC]
# grep

```bash
grep -v "^#" /usr/local/nginx/conf/nginx.conf.default | grep -v "^$"(以$开头)
过滤出非注释和非空行
```
-v反向匹配，|后的语句作用过滤空行
-a 以文本文件方式搜索
-i 忽略大小写
-l 查询多文件时只输出包含匹配字符的文件名
-c 计算找到的符合行的次数
-n 行号
-v 反向匹配
-h查询多文件时不显示文件名
-s 不显示不存在或无匹配文件的错误信息
-E grep -E "a|b"
.........................
grep "[0-9]" test.txt 匹配0到9任意一个字符
grep --color "[0-9][0-9]" test.txt
egrep是grep的升级版，也是行匹配
egrep --color "[0-9]{1}" test.txt 其中{1}意思匹配一次
{1,3}匹配1到3次，{，3}最多匹配3次，{1，}至少匹配1次
...
(\与.之间没有空格, $前没有空格)
egrep --color "[0-9]{1,3}\ .[0-9]{1,3}\ .[0-9]{1,3}\ .[0-9]{1,3} $" test.txt
优化:egrep --color "([0-9]{1,3}\ .){3}[0-9]{1,3} $" test.txt
\是转义字符

grep正则表达式元字符集: 
^ 锚定行的开始
$ 锚定行的结束
. 匹配一个非换行符的
*匹配零或多个先前字符
.*任意字符
[]匹配一个指定范围内的字符如[Gg]
[^]匹配一个不在指定范围内的字符如[Gg]
/(../)标记匹配字符如'/(love/)',love标记为1
/<锚定单词的开始
/>锚定单词的结束，如‘grep/>’匹配包含以grep结尾的单词的行
/w匹配文字和数字字符，即[A-Za-z0-9]
# find

> find [path] [option] [action]

```bash
find / -name "*txt",通配符,在/下找名字以.txt为后缀的文件
 find / -name "*txt“ -type -d ，type指定文件类型这里是目录，type f普通文件,详细的类型man查看
 -iname:忽略大小写
 -maxdepth:最大查找深度，根目录为第一级，再到一级目录
 -mindepth:最小查找深度
 -user:按用户名查询
 -group:按组查询
 
find / -mtime 0 ,0代表当前的时间，意思**从现在开始到24小时前**有被变更过内容的文件都会显示(mtime修改时间，ctime状态改变时间，atime访问时间)
find / -mtime 3 3天前的那24小时内内容变更过的文件
find /etc -newer /etc/passwd
-mtime n:意思n天前的【一天之内】被修改过内容的文件
-mtime +n:列出n天之前(不含n天本身)被修改过内容的文件
-mtime -n:列出n天之内(含n天本身)bei修改过内容的文件
-newer file：file是一个已经存在的文件，找比file新的
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/f5c5544cf6fe4525a7e347b443a082e2.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash
(以下的\与;是没有空格的，另外建议用-exec而不是|xargs,-exec支持更广，两者效率差不多)
find / -name "*.txt" -type d -mtime -1 | xargs rm -rf {} \ ;
意思是将find的输出的相关文件删除掉
find / -name "*.txt" -type d -mtime -1 -exec cp -r {} /tmp \ ;(常考题)
ls | xargs rm -rf {}
将find输出的文件递归cp到/tmp下
find / -name "*.txt" -type d -mtime -1 -size +1k
指定文件的大小 k,M,1024k等等写法
find / -name "*.txt" -type d -mtime -1 -size +1k -perm 755
-perm指定权限(掩码默认022)
find / -type f -exec chmod -R 644\ ;
统一递归修改(子目录可以继承)对应类型的文件的权限
find / -name "*.txt" -type d -mtime -1 -size +1k -perm 755 -exec mv {} \ ;
移动的意思
```
# awk

# sed
# 正则表达式
