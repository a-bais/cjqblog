---
title: redis中仅限在交互式中使用的命令
date: 2021-09-14 17:05:37
tags: redis
categories: 数据库mysql mariadb redis
---

<!--more-->

keys *:列出所有key(生产环境一般不能用，太多key用到内存会导致宕机)
type key:查看一个key类型
del key:删除一个key(可以一次删除多个)
flushdb:清除缓存

附:keys 也可以单独查看某个key，如keys k1
