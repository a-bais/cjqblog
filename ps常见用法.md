---
title: ps常见用法
date: 2021-08-15 11:30:30
tags: 
categories: 解决错误问题
---

<!--more-->

注意ps的选项参数有三种输入方式，-的有无区别很大
![在这里插入图片描述](https://img-blog.csdnimg.cn/1d651c911b854b03b26223950c7774fd.png)
-e    显示所有进程,环境变量
-f    全格式
ps -ef ===ps -e -f
***********************************
a   显示所有进程
u   按用户名和启动时间来显示进程
x   显示无控制终端的进程
ps aux

-l   长格式
![在这里插入图片描述](https://img-blog.csdnimg.cn/c7759ff250aa4566a2549a65d62794e5.png)
ps axo，自定义列名，axo顺序不能乱
![在这里插入图片描述](https://img-blog.csdnimg.cn/491fee068dc34845add3f9c643481e42.png)
ps aux --sort 列名
ps aux --sort=+%cpu,升序
ps aux --sort=-%-pid,降序

*********************

