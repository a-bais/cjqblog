---
title: lnmp拆分出php注意事项
date: 2021-08-12 14:43:26
tags: php nginx mysql
categories: 解决错误问题 重点实操记录 Nginx学习笔记
---

<!--more-->

nginx的配置文件/etc/nginx/conf.d/wecenter:
fastcgi_pass 192.168.160.160:9000,这个ip是php服务器的IP
————————————————————————
php安装了php-fpm,并且netstat -tunlp | grep 9000,开启了监听端口，用于监听nginx来连接php的php-fpm服务的连接请求。
php也要与mysql服务器整合如果mysql不是跟nginx在同一台服务器上的话，也就是说mysql服务器创建一个php用来登录mysql的账号了。
创建主和组，uid和gid要与nginx的主与组一致，chown -R www.www /web/wecenter,这个目录文件与nginx下边的/web/wecenter一致。
最重要的，/etc/php-fpm.d/www.conf
![在这里插入图片描述](https://img-blog.csdnimg.cn/d489296684e94cae84faf92da1760981.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
多个nginx服务器则用,隔开
![在这里插入图片描述](https://img-blog.csdnimg.cn/f1f831433c61438282b0960ed9265bfd.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

