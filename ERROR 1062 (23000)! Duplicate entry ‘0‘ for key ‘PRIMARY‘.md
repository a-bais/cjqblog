---
title: ERROR 1062 (23000): Duplicate entry ‘0‘ for key ‘PRIMARY‘
date: 2021-10-02 21:54:03
tags: mysql
categories: 数据库mysql mariadb redis
---

<!--more-->

问题解析:主键有重复的'0',而主键性质是not null+unqiue，所以冲突错误

解决方法:
加上个设置auto_increment

问题出现场景：已有数据表，里边有字段存在，且字段已有多个值，知识后指定个新字段为主键会出错
