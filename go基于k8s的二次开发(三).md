---
title: go基于k8s的二次开发(三)
date: 2022-06-14 22:56:58
tags: golang kubernetes 云原生
categories: go和微服务零散
---

<!--more-->

@[TOC]
# 修改资源
## 通过Update方法
这里用修改deployment为案例

```bash
package main

import (
        "context"
        "fmt"
        appsV1 "k8s.io/api/apps/v1"
        coreV1 "k8s.io/api/core/v1"
        metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
        "k8s.io/client-go/kubernetes"
        "k8s.io/client-go/tools/clientcmd"
)
func editDeploy() {
        configPath := "etc/kube.conf"
        config, _ := clientcmd.BuildConfigFromFlags("", configPath)
        clientset, _ := kubernetes.NewForConfig(config)
        namespace:="default"
        var replicas int32=1
        name:="nginx"
        deployment, _ := clientset.AppsV1().Deployments(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
        
		deployment.Spec.Replicas=&replicas
        deployment.Spec.Template.Spec.Containers[0].Image="nginx:1.10"
        deployment,err:=clientset.AppsV1().Deployments(namespace).Update(context.TODO(),deployment,metaV1.UpdateOptions{})

        fmt.Println(deployment,err)
}
```
## 通过接口直接修改副本数目

```bash
package main

import (
        "context"
        "fmt"
        appsV1 "k8s.io/api/apps/v1"
        coreV1 "k8s.io/api/core/v1"
        metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
        "k8s.io/client-go/kubernetes"
        "k8s.io/client-go/tools/clientcmd"
)
func editReplicas() {
        configPath := "etc/kube.conf"
        config, _ := clientcmd.BuildConfigFromFlags("", configPath)
        clientset, _ := kubernetes.NewForConfig(config)
        var replicas int32=1
        name:="nginx"
        namespace:="default"
        scale, _ := clientset.AppsV1().Deployments(namespace).GetScale(context.TODO(),name, metaV1.GetOptions{})
        scale.Spec.Replicas=2

        scale,err:=clientset.AppsV1().Deployments(namespace).UpdateScale(context.TODO(),name,scale,metaV1.UpdateOptions{})

        fmt.Println(scale,err)
}
```
# 删除资源

```bash
package main

import (
        "context"
        "fmt"
        appsV1 "k8s.io/api/apps/v1"
        coreV1 "k8s.io/api/core/v1"
        metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
        "k8s.io/client-go/kubernetes"
        "k8s.io/client-go/tools/clientcmd"
)
func deleteResource() {
        configPath := "etc/kube.conf"
        config, _ := clientcmd.BuildConfigFromFlags("", configPath)
        clientset, _ := kubernetes.NewForConfig(config)
        var replicas int32=1
        name,servicename:="nginx","nginx-service"
        namespace:="default"
        clientset.AppsV1().Deployments(namespace).Delete(context.TODO(),name, metaV1.DeleteOptions{})
        clientset.CoreV1.Services(namespace).Delete(context.TODO(),servicename,metaV1.DeleteOptions{})
        //clienset.ExtensionsV1beta1().Ingress()
}
```

