---
title: yum-config-manager配置mysql源的细节即理解
date: 2021-09-28 13:42:57
tags: mysql
categories: 解决错误问题 主要#笔记资料
---

<!--more-->

配置mysql源方式很多没有人选择去官网按步骤来配置，但官网配置下来的yum源默认开启的是mysql80,
当我们向下载mysql57时，要将mysql80源关闭，开启mysql57源（两个不能同时开启会冲突）

安装了mysql的yum源的rpm包后（无非就是高个yum源配置文件，可以自己网上找到配置文件直接复制下来），/etc/yum.repos.d/下一般会有两个文件**mysql-community.repo和mysql-community-source.repo**,用哪个源，可以直接修改这两个文件中的enabled和disabled参数。

yum提供了个管理工具yum-config-manager来方便我们管理yum源，其实它操作就是那两个配置文件。

默认开启mysql80，来关闭mysql80然后开启mysql57，以yum安装mysql57：

> yum install -y yum-utils 
> yum-config-manager --disable mysql80-community 
> yum-config-manager --enable mysql57-community 
> yum repolist

附:
![在这里插入图片描述](https://img-blog.csdnimg.cn/31f46d188ed44950a0ecffbd062cae51.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
yum repolist enabled | grep mysql
yum repolistg disabled | grep mysql

yum install -y mysql-community-server mysql：
![在这里插入图片描述](https://img-blog.csdnimg.cn/81891f83e3ca468ea64e7343b961a4e9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
安装
依赖安装
依赖升级
替代

