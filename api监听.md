---
title: api监听
date: 2022-03-04 18:10:07
tags: docker 容器 运维
categories: k8s
---

<!--more-->

api-server监听资源，其实就是监听资源的http访问方式，所以其中有一种判断前往api-server的请求是否合法的方式就是http认证，也就是验证ca证书。
私钥key->证书请求文件csr->kubernetes的ca签名的自签名证书

kubernetes一般支持普通用户和sa，其中sa可以有kubernetes直接管理
sa的secret挂载到pod中的目录下一般有个ca.crt。
