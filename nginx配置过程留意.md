---
title: nginx配置过程留意
date: 2021-08-06 22:46:29
tags: 
categories: 重点实操记录 Nginx学习笔记 nginx细节配置记录
---

<!--more-->

1./etc/nginx/conf.d/default.conf挺好用有时候

2.已经用yum安装的nginx又yum remove后，部分配置问价还留了下来，尤其是/etc/nginx/conf.d/下的我们自定义的文件

3.nginx默认端口也是80和aache的httpd可能会在使用时有冲突比如第一次启动时，所以建议配置好后再启动软件好些

4.浏览器访问时最好用URL也就是域名

5.当location /download{ 这里边允许列出目录索引，同时相应目录下建立了文件 }，
可以直接这样访问对应文件know.com/download/ir1

6.设置ip的连接限制中，limit_conn_zone设置的同一个zone限制在/etc/nginx/nginx.conf和/etc/nginx/conf.d/*.conf文件下不要同时存在

7.自定义的log_format要注意位置大概在log_format main下，默认的access_log上

8.error_log定义时，后面填写的一般是错误日志等级，自定义的日志模式名一般不能调用在这里，应该还是与等级设置有关

9.例如设置日志在/nginx_log/know_access.log和/nginx_log/know_error.log下，建立/nginx后，用systemctl reload nginx后，/nginx底下的两个文件会自动建立

```bash
10.输入ip访问会可能是nginx的默认页面。这个ip一般是本机的网卡，例如只有一张网卡192.168.160.138
基于端口区分时也有这样的情况，本机ip:80,页面也是默认的nginx的页面
综上两种，我有测试了下，发现与端口有关，因为输入ip默认的端口时80，实验表明页面的情况很可能与80端口有关，另外猜测与浏览器的缓存也有关系。当我将第一个网站know.com的配置文件know.conf基于端口区分虚拟主机修改成了100，输入192.168.160.138:100是页面是我自定义的页面了。
基于域名区分虚拟主机就没什么好说的了，常规的逻辑思维了，/etc/hosts,这里可以使一ip对应一域名来设置，也可以是一个本机ip对应了所有域名，不过这时需注意，输入别的对应域名页面一般不会是相应配置文件中的自定义的页面而是nginx的默认页面
注意:我自己在conf.d下配置的第一个know.conf,使用域名know.com访问时页面是我自定义的index.html的页面，但使用本机ip或者使用仿真出来的网络接口ip访问，这些ip都是know.conf配置文件中设置，显示的都是默认的nginx页面，我排查了一下，应该和nginx.conf.rpmsave的默认设置有关
注意第一个配置的网站的默认优先性
127.0.0.1=本机ip例如192.168.160.138，一张网卡
```

11.ifconfig会生成仿真的网络接口，但asystenctl restart network后就消失了

12.输入域名测试时，如果域名为合法申请了，需要写入/etc/hosts

13.nginx -t 检测的只是语法是否错误，配置文件中的业务逻辑错误并不能查出来

14./etc/hosts用来指定相关的ip与域名

15.location使用时注意=的默认使用性
