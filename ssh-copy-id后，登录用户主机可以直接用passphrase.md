---
title: ssh-copy-id后，登录用户主机可以直接用passphrase
date: 2021-07-31 10:12:21
tags: 1024程序员节
categories: 解决错误问题
---

<!--more-->

ssh-keygen -t rsa -P '123456' -f ~/.ssh/id_rsa
ssh-copy-id -i ~/.ssh/id_rsa.pub root@192.168.160.160
建立起连接后，ssh远程登录用户主机就可以直接使用令牌123456而不一定需要密码
