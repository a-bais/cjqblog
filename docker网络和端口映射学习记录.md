---
title: docker网络和端口映射学习记录
date: 2021-08-26 20:26:31
tags: docker
categories: docker学习笔记
---

<!--more-->

安装好docker后悔默认添加一块网卡如docker0和docker容器进行连接

```bash
docker container inspect 容器id             获取容器id
container可有可无
主要看"network"{}这一段

也可以先docker exec -it 容器id /bin/bash
在查看ip比如 hostname -I
```
首先安装好docker后会多出来一块网卡叫做docker0，与容器进行互联，运行一个镜像都会开启一个容器，一个镜像可以运行多次，每当运行后都会产生新的ip地址新的配置参数，生成ip地址后，宿主机会多一块vethxxx的网卡，vethxxx的网卡连接docker容器中的ip地址和docker网桥，docker网桥再连接宿主的docker0网卡，当docker容器需要上网时，docker0网卡就会与宿主机网卡ens33或者其他的网卡接口进行内核转发实现nat地址转换，最终由eth网卡去访问互联网，最后依次返回。
![在这里插入图片描述](https://img-blog.csdnimg.cn/bbe4631c33f64200839d07a0190ec11d.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_Q1NETiBA6Zi_55m977yM,size_43,color_FFFFFF,t_70,g_se,x_16)
如果内核转发参数设置为0则docker容器无法上网

**docker端口映射**

> -p hostport:conport 一对一映射
> -p ip:hostport:conport 指定ip映射，可以是其他ip地址，不常用
> -p ip::conport 映射成随机端口
> -p hostport:conport:udp 映射成udp端口
> -P随机映射 docker run  -d -p 21:21 nginx:latest

容器内部端口，可以映射到宿主机多个端口
