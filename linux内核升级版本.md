---
title: linux内核升级版本
date: 2021-10-27 19:19:05
tags: k8s
categories: k8s 总复习:云原生运维开发知识点
---

<!--more-->

# 操作
3.10版本有一些bug对k8运行不稳定，挑剔的话，这里来为k8s部署之前升级内核版本

```bash
[root@k8s-master01 journald.conf.d]# uname -r
3.10.0-1062.el7.x86_64

 rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm	下载用于更新内核的yum源
```

要检查/boot/grub2/grub.cfg中对应内核menuentry是否包含initrd16的配置，没有则重新安装
yum repolist或者yum makecache

```bash
[root@k8s-master01 journald.conf.d]# yum repolist
已加载插件：fastestmirror
Loading mirror speeds from cached hostfile
 * base: ftp.sjtu.edu.cn
 * elrepo: mirrors.tuna.tsinghua.edu.cn
 * extras: mirrors.163.com
 * updates: mirrors.163.com
elrepo                                                                                                                                                                     | 3.0 kB  00:00:00     
elrepo/primary_db                                                                                                                                                          | 431 kB  00:00:06     
源标识                                                                     源名称                                                                                                           状态
base/7/x86_64                                                              CentOS-7 - Base                                                                                                  10,072
elrepo                                                                     ELRepo.org Community Enterprise Linux Repository - el7                                                              134
extras/7/x86_64                                                            CentOS-7 - Extras                                                                                                   500
updates/7/x86_64                                                           CentOS-7 - Updates                                                                                                2,902
repolist: 13,608
[root@k8s-master01 journald.conf.d]# cd /etc/yum.repos.d/
[root@k8s-master01 yum.repos.d]# ls
CentOS-Base.repo  CentOS-CR.repo  CentOS-Debuginfo.repo  CentOS-fasttrack.repo  CentOS-Media.repo  CentOS-Sources.repo  CentOS-Vault.repo  elrepo.repo
```

yum --enablerepo=elrepo-kernel install -y kernel-lt 安装内核，kernel-lt，长期维护版
这里的--enablerepo是指定下载时用的源，而不是像yum-config-manager --enable 那样设置一个源的开启，所以这里的/etc/yum.repos.d/elrepo.repo文件下的[elrepo]的enabled=0不变，一样是关闭的

内核安装好后需要设置1默认启动选项重启后才会生效

```bash
awk -F\' '$1=="menuentry "{print i++ " : " $2}' /etc/grub2.cfg，找到对应内核对应的序列号

GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
GRUB_DEFAULT=saved，这里可以设置为0，看下边截图，0对应我下载的最新内核
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="spectre_v2=retpoline rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet"
GRUB_DISABLE_RECOVERY="true"
```

```bash
grub2-set-default "CentOs Linux (5.4.155-1.el7.elrepo.x86_64) 7 (Core)"或者grub2-set-default 0
```

注意这里括号里的内核版本是你自己的，yum info kernel-lt可以查看

```bash
reboot
```
重启选择自己想要的内核进入即可，内核升级完成

![在这里插入图片描述](https://img-blog.csdnimg.cn/10cb52af614f4bef864c4016cfe82eec.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/4660afdf818344c9903007620d420663.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e08dc9a15d164578947c885597b1293c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/3c2cbfe1617143d29533d381071c6155.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/8ec55a411165412e8355dabe0d34a5f1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
kube-proxy开启ipvs的前置条件

```bash
modprobe br_netfilter
touch /etc/sysconfig/modules/ipvs.modules
 cat > /etc/sysconfig/modules/ipvs.modules <<EOF
> #!/bin/bash
> modprobe -- ip_vs
> modprobe -- ip_vs_rr
> modprobe -- ip_vs_wrr
> modprobe -- ip_vs_sh
> modprobe -- nf_conntrack_ipv4
> EOF
> 

 chmod 755 /etc/sysconfig/modules/ipvs.modules
 bash /etc/sysconfig/modules/ipvs.modules
 modprobe: FATAL: Module nf_conntrack_ipv4 not found.	查了一下这个错误是因为nf_conntrack_ipv4改为nf_conntrck了，所以修改配置文件对应这里的内容，重新执行脚本
 
 lsmod | grep -e ip_vs -e nf_conntrack_ipv4（应该是nf_conntrack）
```
注意高版本中不再是nf_conntrack_ipv4而是nf_conntrack
连接跟踪
![在这里插入图片描述](https://img-blog.csdnimg.cn/01e8e370104d48fe9644ac04d6e827be.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2aed98a213dc4d449caafc810ea872c7.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

# 参考
[Linux内核4.4版本yum方式升级详细过程](https://blog.csdn.net/weixin_43860781/article/details/103990136)
