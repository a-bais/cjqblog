---
title: nginx配置实操:常用模块之访问限制(限制)
date: 2021-09-23 22:24:52
tags: nginx
categories: nginx配置实操（yum方式安装）
---

<!--more-->

访问限制一般有两种:
1.连接数限制
2.请求数限制

**连接数限制**
正规的配置是配置在/etc/nginx/nginx.conf(即配置在http模块中，所有站点均可调用)
![在这里插入图片描述](https://img-blog.csdnimg.cn/c10d870e825e40c0a9d66b9a6449ee22.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
然后直接在对应的网站配置文件里边调用即可
也可以在不同的站点的配置文件中配置
![在这里插入图片描述](https://img-blog.csdnimg.cn/a7eb57711d4345dabf703d2866b6b18b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
$binary_remote_addr长度4字节固定，即32位，用来存储ipv4
$remote_addr长度7-15字节，适合用来存储ipv6
10m bytes=10*1024k bytes=10*1024*8 bits
然后用ab命令进行压力测试即可(后续写一篇关于ab命令的博文)


**请求数限制**
正规的写法也是写在/etc/nginx/nginx.conf下，然后不同的站点在调用
这里记录的是/etc/nginx/conf.d/*.conf下的写法
![在这里插入图片描述](https://img-blog.csdnimg.cn/ece4ece789904817867bb1de0a7227c4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
一样用ab命令来测试

```bash
ab -n 10000 -c 10 http://a1.com/           #注意有/,也可以写成http://a1.com/，在这里的配置文件这两者匹配是一致的

Server Software:        nginx/1.20.1		#服务器软件名称及版本信息
Server Hostname:        a1.com				#服务器主机名
Server Port:            80					#服务器端口

Document Path:          /					#供策士的URL路径
Document Length:        3 bytes				#供策士的URL返回的文档大小

Concurrency Level:      10					#并发数
Time taken for tests:   1.141 seconds		#压力测试消耗的总时间
Complete requests:      10000				#压力测试总次数
Failed requests:        9007				#失败数
   (Connect: 0, Receive: 0, Length: 9007, Exceptions: 0)
Write errors:           0
Non-2xx responses:      9007
Total transferred:      3553959 bytes		#传输的总数据量
HTML transferred:       1777358 bytes		#html文档的总数据量
Requests per second:    8766.40 [#/sec] (mean) #平均每秒的请求数
Time per request:       1.141 [ms] (mean)
Time per request:       0.114 [ms] (mean, across all concurrent requests)
Transfer rate:          3042.52 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       8
Processing:     0    1   0.8      1       9
Waiting:        0    1   0.8      0       8
Total:          0    1   0.8      1       9
WARNING: The median and mean for the waiting time are not within a normal deviation
        These results are probably not that reliable.

Percentage of the requests served within a certain time (ms)
  50%      1
  66%      1
  75%      1
  80%      1
  90%      1
  95%      2
  98%      5
  99%      6
 100%      9 (longest request)

```

截图有两个注意的地方一个是rate那里，应该是900r/s
还有两个限制的zone名字一样了这是不对的，改下zone名称
(粗心了)
