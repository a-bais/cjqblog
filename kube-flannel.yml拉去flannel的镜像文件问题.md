---
title: kube-flannel.yml拉去flannel的镜像文件问题
date: 2021-11-06 10:50:42
tags: k8s
categories: 解决错误问题 k8s
---

<!--more-->

```bash
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
下载下来官方这个文件，
注意下载时访问这个网站可能有问题，在本专栏中有解决这个问题的文章，日期与本博文相近
安装网上的大多数建议将yml文件中的quay,io改成quay-mirror.qiniu.com

然后kubectl apply -f kubr-flannel.yml

可以通过kubectl  get nodes 和kubectl get pod -n kube-system查看效果，判断flannel镜像是否安装完成

安装不成功的话可以切换回quay.io，重新kubectl apply
，注意systemctl damon-reload和systemctl restart kubelet

