---
title: awk -F指定分割符错误,获取主机网卡信息实战
date: 2021-07-31 15:47:14
tags: 
categories: 解决错误问题
---

<!--more-->

awk默认以空格为分割符，包括空格和Tab等
awk -F ""，意思是以一个空格为分割符

获取主机网址信息的技巧:
ifconfig | grep 'broadcast'|awk '{print $2}'
if addr | grep 'brg' | awk '{print $2}'
ifconfig ens33 | awk '{if(NR==2}{print $2}}'
