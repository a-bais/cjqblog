---
title: nf_conntrack_ipv4模块安装(解决找不到的错误问题)
date: 2021-11-04 22:13:46
tags: k8s
categories: 解决错误问题 k8s
---

<!--more-->

安装kubernetes是，主要修改内核参数，
要加入nf_conntrack_ipv4,有的教程跟着做会显示找不到nf_conntrack_ipv4,高版本的内核因为nf_conntrack_ipv4被nf_conntrack替换了，所以装不了，至于低版本内核就俗所谓了，但要注意文件的后缀名，执行那个修改模块的脚本，注意脚本名后缀的是.modules，
如/etc/sysconfig/modules/ipvs.modules
然后再sysctl -p，请测没有错误
