---
title: /etc/selinux/config与/etc/sysconfig/selinux即selinux状态设置
date: 2021-09-28 11:01:13
tags: linux
categories: 解决错误问题 主要#笔记资料
---

<!--more-->

/etc/sysconfig/selinux是指向**原文件/etc/selinux/config**的一个软连接文件，
修改/etc/sysconfig/selinux会破坏连接关系使其变成普通的文件不再是系统认为的selinux的配置文件
**建议修改/etc/selinux/config即可**

设置selinux状态方法有永久和临时两种

**永久：**
修改配置文件/etc/selinux/config的SELINUX=disabled,然后需要重启服务器生效

**临时：**
getenforce可以查看当前selinux的状态
setenforce [0|1]可以临时设置selinux的状态，0关闭，1开启
即setenforce可以在enforcing（开）和permissive（关）间切换selinux的状态
