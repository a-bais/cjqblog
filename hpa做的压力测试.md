---
title: hpa做的压力测试
date: 2021-11-23 19:07:43
tags: kubernetes 容器 docker
categories: k8s
---

<!--more-->

可以使用postman软件进行压测，
这里使用ab命令更好

如：ab -t 60 -c 1000 http://192.168.160.100:30656/usr/share
这里的/usr/share是任意一个文件名，现在的ab命令使用要求有文件路径
![在这里插入图片描述](https://img-blog.csdnimg.cn/3f6f87e25fe54c9488da8695929cf298.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

