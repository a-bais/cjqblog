---
title: 查看docker的默认仓库的镜像的版本标签
date: 2022-01-04 21:28:23
tags: docker 容器 运维
categories: docker学习笔记
---

<!--more-->

![在这里插入图片描述](https://img-blog.csdnimg.cn/ed5f346b81c44df1aeaf11aa7e71a1f4.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
简单明了

如果你用的是别的加速镜像仓库，方法类似

也可以通过命令

```bash
curl https://registry.hub.docker.com/v1/repositories/ubuntu/tags\
| tr -d '[\[\]" ]' | tr '}' '\n'\
| awk -F: -v image='ubuntu' '{if(NR!=NF && $3 != ""){printf("%s:%s\n",image,$3)}}'
```

这样可以过滤出ubuntu的各个版本
仓库和软件名看个人需求更改


