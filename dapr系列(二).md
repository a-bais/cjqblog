---
title: dapr系列(二)
date: 2022-07-17 13:45:44
tags: kubernetes 云原生 dapr 分布式应用程序
categories: go和微服务零散
---

<!--more-->

@[TOC]
# 安装 Dapr CLI 脚手架工具
命令行客户端界面
Dapr CLI 是用于操作各种任务的主要任务。使用它来运行一个与 Dapr sidecar 相关的应用程序，以及查看 sidecar、日志工具运行中的服务、运行 Dapr 仪表板。 Dapr CLI适用于自托管和**kubernetes**环境。

这里先不讲在kubernetes上的相关的操作，因为后续会专门讲dapr+kubernetes
## linux(二进制)
直接下载二进制包，解压到PATH即可使用，你也可以使用源码包编译安装
![在这里插入图片描述](https://img-blog.csdnimg.cn/c0b87129c16240d9a64aeb41ea05c2a2.png)
或者直接运行下面的命令，默认是安装最新版本，你可以自己调整版本

```bash
wget -q https://raw.githubusercontent.com/dapr/cli/master/install/install.sh -O - | /bin/bash
```
## 验证安装
```bash
[root@master1 ~]# dapr

          __
     ____/ /___ _____  _____
    / __  / __ '/ __ \/ ___/
   / /_/ / /_/ / /_/ / /
   \__,_/\__,_/ .___/_/
             /_/

===============================
Distributed Application Runtime

Usage:
  dapr [command]

Available Commands:
  annotate       Add dapr annotations to a Kubernetes configuration. Supported platforms: Kubernetes
  build-info     Print build info of Dapr CLI and runtime
  completion     Generates shell completion scripts
  components     List all Dapr components. Supported platforms: Kubernetes
  configurations List all Dapr configurations. Supported platforms: Kubernetes
  dashboard      Start Dapr dashboard. Supported platforms: Kubernetes and self-hosted
  help           Help about any command
  init           Install Dapr on supported hosting platforms. Supported platforms: Kubernetes and self-hosted
  invoke         Invoke a method on a given Dapr application. Supported platforms: Self-hosted
  list           List all Dapr instances. Supported platforms: Kubernetes and self-hosted
  logs           Get Dapr sidecar logs for an application. Supported platforms: Kubernetes
  mtls           Check if mTLS is enabled. Supported platforms: Kubernetes
  publish        Publish a pub-sub event. Supported platforms: Self-hosted
  run            Run Dapr and (optionally) your application side by side. Supported platforms: Self-hosted
  status         Show the health status of Dapr services. Supported platforms: Kubernetes
  stop           Stop Dapr instances and their associated apps. Supported platforms: Self-hosted
  uninstall      Uninstall Dapr runtime. Supported platforms: Kubernetes and self-hosted
  upgrade        Upgrades or downgrades a Dapr control plane installation in a cluster. Supported platforms: Kubernetes
  version        Print Dapr runtime and Cli version.

Flags:
  -h, --help          help for dapr
      --log-as-json   Log output in JSON format
  -v, --version       version for dapr

Use "dapr [command] --help" for more information about a command.
```
# 在本地环境中初始化 Dapr
Dapr 与您的应用程序一起作为sidecar运行，在自托管模式下，这意味着它是您本地机器上的一个进程。 因此，初始化 Dapr 包括获取 Dapr sidecar 二进制文件并将其安装到本地.

此外，默认初始化过程还创建了一个开发环境，帮助简化 Dapr 的应用开发。 这包括下列步骤：
1.运行一个用于**状态存储**和**消息代理**的Redis容器实例
2.运行一个用于提供可观察性的**Zipkin**容器实例(后续建议使用tragger)
3.创建具有上述组件定义的默认组件文件夹
4.运行用于本地actor支持的Dapr placement服务容器实例


> 如果你是容器方式安装dapr，那么就需要进入容器里面运行命令，这里直接在终端输入即可

安装最新的 Dapr 运行时二进制程序:
```bash
dapr init
```
验证版本：

```bash
[root@master1 .dapr]# dapr --version
CLI version: 1.8.0
Runtime version: 1.8.0
```

```bash
[root@master1 .dapr]# docker ps
CONTAINER ID   IMAGE                                               COMMAND                  CREATED         STATUS                   PORTS
c3f8bbf23492   daprio/dapr:1.8.0                                   "./placement"            4 minutes ago   Up 4 minutes             0.0.0.0:5000ement
7c9503a84e6e   redis                                               "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes             0.0.0.0:6379s
c2c27bfcf3e7   openzipkin/zipkin                                   "start-zipkin"           4 minutes ago   Up 4 minutes (healthy)   9410/tcp, 0.in
```
在 dapr init时，CLI 还创建了一个默认组件文件夹，其中包括几个 YAML 文件，其中包含state store、elevated 和 zipkin。 Dapr sidecar, 将读取这些文件。 告诉它使用Redis容器进行状态管理和消息传递，以及Zipkin容器来收集跟踪。
```bash
[root@master1 .dapr]# pwd
/root/.dapr
[root@master1 .dapr]# ls
bin  components  config.yaml
```
# 使用 Dapr 的编写 API
运行上一步dapr init中的命令后(就是开启dapr运行时)，你的本地环境具有 Dapr sidecar 二进制文件以及状态管理和消息代理（均使用 Redis）的默认组件定义。现在可以通过使用 Dapr CLI 来运行 Dapr sidecar 并测试使用状态来展示 API 来和检索状态，提供 Dapr 的一些功能。

可以运行sidecar并直接调用API（模拟应用程序将做什么）。
## 运行 Dapr sidecar
最有用的 Dapr CLI 命令之一是`dapr run`

下面以运行 Dapr 边车命令，在启动程序 350 上监听 myapp 的空白应用程序：

```bash
[root@master1 .dapr]# dapr run --app-id myapp --dapr-http-port 3500
WARNING: no application command found.
ℹ️  Starting Dapr with id myapp. HTTP Port: 3500. gRPC Port: 40891
ℹ️  Checking if Dapr sidecar is listening on HTTP port 3500
INFO[0002] starting Dapr Runtime -- version 1.8.0 -- commit dc7f86840c85a1eff2e1223456994f554ea31d11  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] log level set to: info                        app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] metrics server started on :37738/             app_id=myapp instance=master1 scope=dapr.metrics type=log ver=1.8.0
INFO[0002] standalone mode configured                    app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] app id: myapp                                 app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] mTLS is disabled. Skipping certificate request and tls validation  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] local service entry announced: myapp -> 192.168.23.232:36553  app_id=myapp instance=master1 scope=dapr.contrib type=log ver=1.8.0
INFO[0002] Initialized name resolution to mdns           app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] loading components                            app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] component loaded. name: pubsub, type: pubsub.redis/v1  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] waiting for all outstanding components to be processed  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] detected actor state store: statestore        app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] component loaded. name: statestore, type: state.redis/v1  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] all outstanding components processed          app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] gRPC proxy enabled                            app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] enabled gRPC tracing middleware               app_id=myapp instance=master1 scope=dapr.runtime.grpc.api type=log ver=1.8.0
INFO[0002] enabled gRPC metrics middleware               app_id=myapp instance=master1 scope=dapr.runtime.grpc.api type=log ver=1.8.0
INFO[0002] API gRPC server is running on port 40891      app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] enabled metrics http middleware               app_id=myapp instance=master1 scope=dapr.runtime.http type=log ver=1.8.0
INFO[0002] enabled tracing http middleware               app_id=myapp instance=master1 scope=dapr.runtime.http type=log ver=1.8.0
INFO[0002] http server is running on port 3500           app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] The request body size parameter is: 4         app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] enabled gRPC tracing middleware               app_id=myapp instance=master1 scope=dapr.runtime.grpc.internal type=log ver=1.8.0
INFO[0002] enabled gRPC metrics middleware               app_id=myapp instance=master1 scope=dapr.runtime.grpc.internal type=log ver=1.8.0
INFO[0002] internal gRPC server is running on port 36553  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
WARN[0002] app channel is not initialized. did you make sure to configure an app-port?  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] actor runtime started. actor idle timeout: 1h0m0s. actor scan interval: 30s  app_id=myapp instance=master1 scope=dapr.runtime.actor type=log ver=1.8.0
WARN[0002] app channel not initialized, make sure -app-port is specified if pubsub subscription is required  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
WARN[0002] failed to read from bindings: app channel not initialized   app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] dapr initialized. Status: Running. Init Elapsed 31.531104999999997ms  app_id=myapp instance=master1 scope=dapr.runtime type=log ver=1.8.0
INFO[0002] placement tables updated, version: 0          app_id=myapp instance=master1 scope=dapr.runtime.actor.internal.placement type=log ver=1.8.0
ℹ️  Checking if Dapr sidecar is listening on GRPC port 40891
ℹ️  Dapr sidecar is up and running.
✅  You're up and running! Dapr logs will appear here.
```
使用这个命令，没有定义**自定义组件文件夹**。因此在使用 Dapr 中创建的默认组件定义(默认有个组件文件夹，比如$HOME/.dapr/components)，告诉 Dapr 使用本地的 Redis Docker 容器作为状态存储和消息代理。
## 保存状态
我们现在将更新对象的状态。 新状态将看起来像这样：

```bash
Copy
[
  {
    "key": "name",
    "value": "Bruce Wayne"
  }
]
```

注意, 状态中包含的对象有一个 key, 其值 name。 您将在下一步中使用该key。

运行下面显示的命令来存储新的状态:

```bash
curl -X POST -H "Content-Type: application/json" -d '[{ "key": "name", "value": "Bruce Wayne"}]' http://localhost:3500/v1.0/state/statestore

```
## 获取状态
现在通过使用状态管理API，用key name 来获取你刚刚存储在状态中的对象。

```bash
用上面运行的同一个Dapr实例运行：

Copy
curl http://localhost:3500/v1.0/state/statestore/name
```
## 查看状态如何在 Redis 中存储
在 Redis 容器中看到并验证Dapr 正在使用它作为状态存储。 运行以下命令来使用Redis CLI：

```bash
docker exec -it dapr_redis redis-cli
```

列出redis keys以查看Dapr如何创建一个键值对(你提供给 dapr run 的app-id 作为key的前缀)：

```bash
keys *
```
```bash
"myapp||name"
```

查看运行时状态值：

```bash
hgetall "myapp||name"
```

```bash
1) "data"
2) "\"Bruce Wayne\""
3) "version"
4) "1"
```

退出redis-cli，使用

```bash
exit
```

## 删除状态
在同一终端窗口中，name从状态存储中删除状态对象。

```bash
curl -v -X DELETE -H "Content-Type: application/json" http://localhost:3500/v1.0/state/statestore/name
```

# dapr快速入门(主要以go为例子)
python,javascript看情况
## 服务调用
使用Dapr 的**服务调用构建块**，您的应用程序可以与其他应用程序可靠且安全地通信。
![在这里插入图片描述](https://img-blog.csdnimg.cn/d3d32fefabed48509598d9d54dce8ac2.png)
Dapr 提供了多种服务调用方法，你可以根据自己的场景进行选择
### 先决条件
对于此示例，你需要：
1.Dapr CLI 和初始化环境。
2.新版本的go。
3.docker
### 设置环境
克隆示例代码。
源码链接是[https://github.com/dapr/quickstarts/tree/master/service_invocation](https://github.com/dapr/quickstarts/tree/master/service_invocation)

```bash
git clone https://github.com/dapr/quickstarts.git
git clone git@github.com:dapr/quickstarts.git
```
### 运行order-processor服务(订单服务)
cd到order-processor目录

```bash
cd service_invocation/go/http/order-processor
```
安装依赖项：

```bash
go build app.go
```

order-processor与 Dapr sidecar 一起运行该服务。

```bash
dapr run --app-port 6001 --app-id order-processor --app-protocol http --dapr-http-port 3501 -- go run app.go
```

每个订单都通过 HTTP POST 请求接收并由 getOrder函数处理。

```bash
func getOrder(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Order received : %s", string(data))
```

```bash
func getOrder(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Order received : %s", string(data))

```
### 运行checkout服务
在新的终端窗口中，从 Quickstart 克隆目录的根目录导航到该checkout目录。

```bash
cd service_invocation/go/http/checkout
```

安装依赖项：

```bash
go build app.go
```

checkout与 Dapr sidecar 一起运行该服务。

```bash
dapr run --app-id checkout --app-protocol http --dapr-http-port 3500 -- go run app.go
```

在checkout服务中，您会注意到无需重写您的应用程序代码即可使用 Dapr 的服务调用。您可以通过简单地添加标头来启用服务调用，该dapr-app-id标头指定目标服务的 ID。


```bash
req.Header.Add("dapr-app-id", "order-processor")

response, err := client.Do(req)
```

### 查看服务调用输出
Dapr 在任何 Dapr 实例上调用应用程序。在代码中，sidecar 编程模型鼓励每个应用程序与其自己的 Dapr 实例通信。Dapr 实例随后发现并相互通信。

checkout服务输出：

```bash
== APP == Order passed:  {"orderId":1}
== APP == Order passed:  {"orderId":2}
== APP == Order passed:  {"orderId":3}
== APP == Order passed:  {"orderId":4}
== APP == Order passed:  {"orderId":5}
== APP == Order passed:  {"orderId":6}
== APP == Order passed:  {"orderId":7}
== APP == Order passed:  {"orderId":8}
== APP == Order passed:  {"orderId":9}
== APP == Order passed:  {"orderId":10}
```
order-processor服务输出：

```bash
== APP == Order received :  {"orderId":1}
== APP == Order received :  {"orderId":2}
== APP == Order received :  {"orderId":3}
== APP == Order received :  {"orderId":4}
== APP == Order received :  {"orderId":5}
== APP == Order received :  {"orderId":6}
== APP == Order received :  {"orderId":7}
== APP == Order received :  {"orderId":8}
== APP == Order received :  {"orderId":9}
== APP == Order received :  {"orderId":10}
```
## 使用 Dapr 的状态管理构建块
Dapr 的**状态管理**构建块。使用 Redis 状态存储来保存、获取和删除状态
![在这里插入图片描述](https://img-blog.csdnimg.cn/017d56473e24453e813c41f650917b84.png)
### 设置环境

```bash
git clone https://github.com/dapr/quickstarts.git
```
### 操作服务状态

```bash
cd state_management/go/sdk/order-processor
```
安装依赖项并构建应用程序：

```bash
go build app.go
```

order-processor与 Dapr sidecar 一起运行该服务。

```bash
dapr run --app-id order-processor --components-path ../../../components -- go run app.go
```
该服务向组件中定义的实例order-processor写入、读取和删除orderId键/值对。一旦服务启动，它就会执行一个循环。statestorestatestore.yaml

```bash
  client, err := dapr.NewClient()

  // Save state into the state store
  _ = client.SaveState(ctx, STATE_STORE_NAME, strconv.Itoa(orderId), []byte(order))
  log.Print("Saving Order: " + string(order))

  // Get state from the state store
  result, _ := client.GetState(ctx, STATE_STORE_NAME, strconv.Itoa(orderId))
  fmt.Println("Getting Order: " + string(result.Value))

  // Delete state from the state store
  _ = client.DeleteState(ctx, STATE_STORE_NAME, strconv.Itoa(orderId))
  log.Print("Deleting Order: " + string(order))
```
### 查看订单处理器输出
请注意，正如上面代码中所指定的，代码将应用程序状态保存在 Dapr 状态存储中，读取它，然后将其删除。

```bash
== APP == dapr client initializing for: 127.0.0.1:53689
== APP == 2022/04/01 09:16:03 Saving Order: {"orderId":1}
== APP == Getting Order: {"orderId":1}
== APP == 2022/04/01 09:16:03 Deleting Order: {"orderId":1}
== APP == 2022/04/01 09:16:03 Saving Order: {"orderId":2}
== APP == Getting Order: {"orderId":2}
== APP == 2022/04/01 09:16:03 Deleting Order: {"orderId":2}
== APP == 2022/04/01 09:16:03 Saving Order: {"orderId":3}
== APP == Getting Order: {"orderId":3}
== APP == 2022/04/01 09:16:03 Deleting Order: {"orderId":3}
== APP == 2022/04/01 09:16:03 Saving Order: {"orderId":4}
== APP == Getting Order: {"orderId":4}
== APP == 2022/04/01 09:16:03 Deleting Order: {"orderId":4}
== APP == 2022/04/01 09:16:03 Saving Order: {"orderId":5}
== APP == Getting Order: {"orderId":5}
== APP == 2022/04/01 09:16:03 Deleting Order: {"orderId":5}
```
statestore.yaml组件文件
当你运行时dapr init，Dapr 会创建一个默认的 Redisstatestore.yaml并在你的本地机器上运行一个 Redis 容器，它位于：~/.dapr/components/statestore.yaml
使用该statestore.yaml组件，你可以轻松换出状态存储，而无需更改代码。

Redisstatestore.yaml文件包含以下内容：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: statestore
spec:
  type: state.redis
  version: v1
  metadata:
  - name: redisHost
    value: localhost:6379
  - name: redisPassword
    value: ""
  - name: actorStateStore
    value: "true"
```
在 YAML 文件中：

metadata/name是你的应用程序如何与组件对话（DAPR_STORE_NAME在代码示例中调用）。
spec/metadata定义到组件使用的 Redis 实例的连接。
## 使用 Dapr 的发布和订阅构建块(pub/sub)
 Dapr 的发布和订阅 (Pub/sub) 构建块。运行发布者微服务和订阅者微服务，演示 Dapr 如何启用发布/订阅模式。
1.使用发布服务，开发人员可以重复发布消息到主题。
2.Pub/sub 组件对这些消息进行排队或代理。使用 Redis，你可以使用 RabbitMQ、Kafka 等。
3.该主题的订阅者从队列中提取消息并处理它们。
![在这里插入图片描述](https://img-blog.csdnimg.cn/925a99cc6eb247fc8775f9932d6953b0.png)
### 设置环境

```bash
git clone https://github.com/dapr/quickstarts.git
```
### 订阅主题
在终端窗口中，从 Quickstarts 克隆目录的根目录导航到该order-processor目录。

```bash
cd pub_sub/go/sdk/order-processor
```
安装依赖项并构建应用程序：

```bash
go build app.go
```

order-processor与 Dapr sidecar 一起运行订阅者服务。

```bash
dapr run --app-port 6001 --app-id order-processor --app-protocol http --dapr-http-port 3501 --components-path ../../../components -- go run app.go
```
在order-processor订阅者中，订阅了名为order_pub_sub （在组件中定义的pubsub.yaml）和 topic的 Redis 实例orders。应用程序代码能够通过 Dapr sidecar 与 Redis 组件实例通信。

```bash
func eventHandler(ctx context.Context, e *common.TopicEvent) (retry bool, err error) {
	fmt.Println("Subscriber received: ", e.Data)
	return false, nil
}
```
### 发布主题
在新的终端窗口中，从 Quickstarts 克隆目录的根目录导航到该checkout目录。

```bash
cd pub_sub/go/sdk/checkout
```
安装依赖项并构建应用程序：

```bash
go build app.go
```

checkout与 Dapr sidecar 一起运行发布者服务。

```bash
dapr run --app-id checkout --app-protocol http --dapr-http-port 3500 --components-path ../../../components -- go run app.go
```
在checkout发布者中，将 orderId 消息发布到名为order_pub_sub （在组件中定义的pubsub.yaml）和 topic的 Redis 实例orders。服务一启动，就会循环发布：

```bash
client, err := dapr.NewClient()

if err := client.PublishEvent(ctx, PUBSUB_NAME, PUBSUB_TOPIC, []byte(order)); err != nil {
    panic(err)
}

fmt.Sprintf("Published data: ", order)
```
### 查看 Pub/sub 输出
请注意，正如上面代码中所指定的，发布者将一条编号的消息推送到 Dapr 边车，而订阅者接收它。

发布者输出：

```bash
== APP == dapr client initializing for: 127.0.0.1:63293
== APP == Published data:  {"orderId":1}
== APP == Published data:  {"orderId":2}
== APP == Published data:  {"orderId":3}
== APP == Published data:  {"orderId":4}
== APP == Published data:  {"orderId":5}
== APP == Published data:  {"orderId":6}
== APP == Published data:  {"orderId":7}
== APP == Published data:  {"orderId":8}
== APP == Published data:  {"orderId":9}
== APP == Published data:  {"orderId":10}
```

订阅者输出：

```bash
== APP == Subscriber received:  {"orderId":1}
== APP == Subscriber received:  {"orderId":2}
== APP == Subscriber received:  {"orderId":3}
== APP == Subscriber received:  {"orderId":4}
== APP == Subscriber received:  {"orderId":5}
== APP == Subscriber received:  {"orderId":6}
== APP == Subscriber received:  {"orderId":7}
== APP == Subscriber received:  {"orderId":8}
== APP == Subscriber received:  {"orderId":9}
== APP == Subscriber received:  {"orderId":10}
```
pubsub.yaml组件文件
当你运行时dapr init，Dapr 会创建一个默认的 Redispubsub.yaml并在你的本地机器上运行一个 Redis 容器，它位于：~/.dapr/components/pubsub.yaml

使用该pubsub.yaml组件，可以轻松更换底层组件，而无需更改应用程序代码。

Redispubsub.yaml文件包含以下内容：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: order_pub_sub
spec:
  type: pubsub.redis
  version: v1
  metadata:
  - name: redisHost
    value: localhost:6379
  - name: redisPassword
    value: ""
scopes:
  - orderprocessing
  - checkout
```
在 YAML 文件中：

1.metadata/name是尼的应用程序与组件对话的方式。
2.spec/metadata定义与组件实例的连接。
3.scopes指定哪个应用程序可以使用该组件。

## 使用 Dapr 的 Binding 构建块
 Dapr 的Bindings 构建块。使用绑定，你可以：
1.使用来自外部系统的事件触发您的应用程序。
2.外部系统的接口。
使用输入Cron绑定安排批处理脚本每 10 秒运行一次。该脚本使用PostgreSQL Dapr 绑定处理 JSON 文件并将数据输出到 SQL 数据库。
![在这里插入图片描述](https://img-blog.csdnimg.cn/ba792ed9188945bbb47299aa7511d86c.png)
### 设置环境
克隆Quickstarts repo 中提供的示例。

```bash
git clone https://github.com/dapr/quickstarts.git
```
### 在本地运行 PostgreSQL Docker 容器
在 Docker 容器中采用本地运行PostgreSQL 实例。


```bash
cd quickstarts/bindings/db
```

设置容器：

```bash
docker compose up
```

验证容器是否在本地运行。

```bash
docker ps
```
### 安排 Cron 作业并写入数据库

```bash
cd quickstarts/bindings/go/sdk/batch
```

安装依赖项：

```bash
go build app.go
```
batch-sdk与 Dapr sidecar 一起运行该服务。

```bash
dapr run --app-id batch-sdk --app-port 6002 --dapr-http-port 3502 --dapr-grpc-port 60002 --components-path ../../../components -- go run app.go 
```
process_batch函数内的代码每 10 秒执行一次（binding-cron.yaml在components目录中定义）。绑定触发器在 Dapr sidecar 的 Flask 应用程序中查找通过 HTTP POST 调用的路由。

```bash
// Triggered by Dapr input binding
r.HandleFunc("/"+cronBindingName, processBatch).Methods("POST")
```
该batch-sdk服务使用binding-postgres.yaml组件中定义的 PostgreSQL 输出绑定将OrderId、Customer和Price记录插入到orders表中。

```bash
func sqlOutput(order Order) (err error) {

	client, err := dapr.NewClient()
	if err != nil {
		return err
	}

	ctx := context.Background()

	sqlCmd := fmt.Sprintf("insert into orders (orderid, customer, price) values (%d, '%s', %s);", order.OrderId, order.Customer, strconv.FormatFloat(order.Price, 'f', 2, 64))
	fmt.Println(sqlCmd)

	// Insert order using Dapr output binding via Dapr SDK
	in := &dapr.InvokeBindingRequest{
		Name:      sqlBindingName,
		Operation: "exec",
		Data:      []byte(""),
		Metadata:  map[string]string{"sql": sqlCmd},
	}
	err = client.InvokeOutputBinding(ctx, in)
	if err != nil {
		return err
	}

	return nil
}
```

### 查看作业的输出
请注意，如上所述，代码使用 、 和 作为有效负载调用OrderId输出Customer绑定Price。

输出绑定的print语句输出：

```bash
== APP == Processing batch..
== APP == insert into orders (orderid, customer, price) values(1, 'John Smith', 100.32)
== APP == insert into orders (orderid, customer, price) values(2, 'Jane Bond', 15.4)
== APP == insert into orders (orderid, customer, price) values(3, 'Tony James', 35.56)
```

在新终端中，验证是否已将相同的数据插入到数据库中。导航到bindings/db目录。

```bash
cd quickstarts/bindings/db
```

运行以下命令以启动交互式 Postgres CLI：


```bash
docker exec -i -t postgres psql --username postgres  -p 5432 -h localhost --no-password
```

在admin=#提示符下，更改为orders表格：

```bash
\c orders;
```

在orders=#提示符下，选择所有行：


```bash
select * from orders;
```

输出应如下所示：

```bash
 orderid |  customer  | price
---------+------------+--------
       1 | John Smith | 100.32
       2 | Jane Bond  |   15.4
       3 | Tony James |  35.56
```

components\binding-cron.yaml组件文件
执行dapr run命令并指定组件路径时，Dapr sidecar：

1.启动 Cron绑定构建块
2.batch每 10 秒调用一次绑定端点 ( )

Cronbinding-cron.yaml文件包含以下内容：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: cron
  namespace: quickstarts
spec:
  type: bindings.cron
  version: v1
  metadata:
  - name: schedule
    value: "@every 10s" # valid cron schedule
```
    
注意：的metadata部分binding-cron.yaml包含一个Cron 表达式，该表达式指定调用绑定的频率。

component\binding-postgres.yaml组件文件
当您执行dapr run命令并指定组件路径时，Dapr sidecar：
1.启动 PostgreSQL绑定构建块
2.binding-postgres.yaml使用文件中指定的设置连接到 PostgreSQL

使用该binding-postgres.yaml组件，可以轻松换出后端数据库绑定，而无需更改代码。

PostgreSQLbinding-postgres.yaml文件包含以下内容：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: sqldb
  namespace: quickstarts
spec:
  type: bindings.postgres
  version: v1
  metadata:
  - name: url # Required
    value: "user=postgres password=docker host=localhost port=5432 dbname=orders pool_min_conns=1 pool_max_conns=10"
```
    
在 YAML 文件中：
1.spec/type指定 PostgreSQL 用于此绑定。
2.spec/metadata定义到组件使用的 PostgreSQL 实例的连接。

## 使用 Dapr 的secret管理构建块
Dapr 提供了一个专用的secret API，允许开发人员从secret存储中检索秘密:
1.运行带有秘密存储组件的微服务。
2.在应用程序代码中使用 Dapr 机密 API 检索机密。
### 设置环境

```bash
git clone https://github.com/dapr/quickstarts.git
```
### 检索秘密 

```bash
cd secrets_management/go/sdk/order-processor
```
安装依赖项：

```bash
go build app.go
```

order-processor与 Dapr sidecar 一起运行该服务。

```bash
dapr run --app-id order-processor --components-path ../../../components/ -- go run app.go
```
order-processor服务

请注意以下order-processor服务如何指向：

1.在组件DAPR_SECRET_STORE中定义。local-secret-store.yaml
2.中定义的秘密secrets.json。

```bash
const DAPR_SECRET_STORE = "localsecretstore"
	const SECRET_NAME = "secret"
  // ...
	secret, err := client.GetSecret(ctx, DAPR_SECRET_STORE, SECRET_NAME, nil)
	if secret != nil {
		fmt.Println("Fetched Secret: ", secret[SECRET_NAME])
	}
```
local-secret-store.yaml文件

DAPR_SECRET_STORElocal-secret-store.yaml在位于secrets_management/components的组件文件中定义：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: localsecretstore
  namespace: default
spec:
  type: secretstores.local.file
  version: v1
  metadata:
  - name: secretsFile
    value: secrets.json
  - name: nestedSeparator
    value: ":"
```
在 YAML 文件中：

1.metadata/name是你的应用程序引用组件的方式（DAPR_SECRET_NAME在代码示例中调用）。
2.spec/metadata定义与组件使用的秘密的连接。

secrets.json文件

SECRET_NAME在secrets.json文件中定义，位于secrets_management/python/sdk/order-processor：

```bash
{
    "secret": "YourPasskeyHere"
}
```
### 查看订单处理器输出
正如上面的应用程序代码中所指定的，该order-processor服务通过 Dapr 秘密存储检索秘密并将其显示在控制台中。

订单处理器输出：

```bash
== APP == Fetched Secret:  YourPasskeyHere
```

# dapr教程
## 定义一个组件
创建**组件定义文件**以与secret building block进行交互
在构建应用程序时，你很可能会创建自己的组件文件定义，具体取决于你想要使用的**构建块**和**特定组件**。

这里创建一个组件定义文件以与secret building block API进行交互：

1.创建本地 JSON 机密存储。
2.使用组件定义文件向 Dapr 注册secret存储。
3.使用 Dapr HTTP API 获取密钥。

### 创建 JSON secret存储
Dapr 支持多种类型的secret存储，这里创建一个本地 JSON 文件，命名mysecrets.json：

```bash
{
   "my-secret" : "I'm Batman"
}
```
### 创建secret存储 Dapr 组件
创建一个名为my-components保存新组件文件的新目录：

```bash
mkdir my-components
```
切换到这个目录：

```bash
cd my-components
```
创建一个localSecretStore.yaml包含以下内容的新文件：
```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: my-secret-store
  namespace: default
spec:
  type: secretstores.local.file
  version: v1
  metadata:
  - name: secretsFile
    value: <PATH TO SECRETS FILE>/mysecrets.json
  - name: nestedSeparator
    value: ":"
```
`type: secretstores.local.file`告诉 Dapr 使用本地文件组件作为secret存储。
metadata字段提供使用此组件所需的组件特定信息。
在这种情况下，secret存储 JSON 路径是相对于你调用dapr run时所处的路径。

### 运行 Dapr sidecar
启动一个 Dapr sidecar，它将在端口 3500 上侦听一个名为 的空白应用程序myapp：
```bash
dapr run --app-id myapp --dapr-http-port 3500 --components-path ./my-components
```

```bash
如果出现错误消息，说明app-id已在使用中，您可能需要停止任何当前正在运行的 Dapr sidecars。
dapr run在运行下一个命令之前停止 sidecar ：
按 Ctrl+C 或 Command+C。
dapr stop在终端中运行命令。
```
### 获取秘密
在单独的终端中，运行：

```bash
curl http://localhost:3500/v1.0/secrets/my-secret-store/my-secret

{"my-secret":"I'm Batman"}
```
## 为 Dapr 配置状态存储和发布/订阅消息代理组件
要启动并运行状态和 Pub/sub 构建块，你需要两个组件：
1.用于持久性和恢复的状态存储组件。
2.作为用于异步样式消息传递的发布/订阅消息代理组件。
### 创建 Redis 存储
Dapr 可以使用任何 Redis 实例：
1.在本地开发机器上容器化
2.托管的云服务。
#### 自托管环境
作为初始化过程的一部分，Dapr CLI 会在自托管环境中自动安装 Redis。
#### kubernetes
安装redis之前，先安装Helm v3
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install redis bitnami/redis
```
对于 Dapr 的 **Pub/sub** 功能，至少需要 Redis 版本 5。对于**状态存储**，您可以使用较低版本。
运行kubectl get pods以查看现在在集群中运行的 Redis 容器：

```bash
kubectl get pod
```
对于 Kubernetes：
1.主机名是redis-master.default.svc.cluster.local:6379
2.secret ,redis是自动创建的。

### 配置 Dapr 组件
Dapr 定义了用于使用组件构建块功能的资源。用上面创建的资源连接到 Dapr 以获取 state 和 pub/sub。
#### 找到你的组件文件
##### 自托管
在自托管模式下，组件文件会在以下位置自动创建：`$HOME/.dapr/components`
##### kubernetes
组件文件随你放，之后能用kubectl就行
#### 创建状态存储组件
创建一个名为 的文件redis-state.yaml，然后粘贴以下内容：
##### 自托管
```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: statestore
  namespace: default
spec:
  type: state.redis
  version: v1
  metadata:
  - name: redisHost
    value: localhost:6379
  - name: redisPassword
    secretKeyRef:
      name: redis
      key: redis-password
  # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 
```
##### kubernetes

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: statestore
  namespace: default
spec:
  type: state.redis
  version: v1
  metadata:
  - name: redisHost
    value: <REPLACE WITH HOSTNAME FROM ABOVE - for Redis on Kubernetes it is redis-master.default.svc.cluster.local:6379>
  - name: redisPassword
    secretKeyRef:
      name: redis
      key: redis-password
  # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 

```
示例使用你之前在设置集群时创建的 Kubernetes 密钥。
#### 创建发布/订阅消息代理组件
创建一个名为 的文件redis-pubsub.yaml，然后粘贴以下内容：
##### 自托管

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: pubsub
  namespace: default
spec:
  type: pubsub.redis
  version: v1
  metadata:
  - name: redisHost
    value: localhost:6379
  - name: redisPassword
    secretKeyRef:
      name: redis
      key: redis-password
 # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 
```
##### kubernetes

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: pubsub
  namespace: default
spec:
  type: pubsub.redis
  version: v1
  metadata:
  - name: redisHost
    value: <REPLACE WITH HOSTNAME FROM ABOVE - for Redis on Kubernetes it is redis-master.default.svc.cluster.local:6379>
  - name: redisPassword
    secretKeyRef:
      name: redis
      key: redis-password
 # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 

```
示例使用您之前在设置集群时创建的 Kubernetes 密钥。

##### 硬编码密码（不推荐）
仅出于开发目的，可以跳过创建 Kubernetes secret并将密码直接放入 Dapr 组件文件：

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: statestore
  namespace: default
spec:
  type: state.redis
  version: v1
  metadata:
  - name: redisHost
    value: <HOST>
  - name: redisPassword
    value: <PASSWORD>
  # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 
```

```bash
apiVersion: dapr.io/v1alpha1
kind: Component
metadata:
  name: pubsub
  namespace: default
spec:
  type: pubsub.redis
  version: v1
  metadata:
  - name: redisHost
    value: <HOST>
  - name: redisPassword
    value: <PASSWORD>
  # uncomment below for connecting to redis cache instances over TLS (ex - Azure Redis Cache)
  # - name: enableTLS
  #   value: true 
```
生产环境千万别这样做
### 	应用配置
自托管
Kubernetes

当你运行时`dapr init`，Daprpubsub.yaml在你的本地机器上创建一个默认的 redis。通过打开你的组件目录进行验证：
~/.dapr/components/pubsub.yaml

对于新的组件文件：
1.components在你的应用文件夹中创建一个包含 YAML 文件的新目录。
2.dapr run使用标志提供命令的路径`--components-path`

如果你在slim 模式下初始化 Dapr （没有 Docker），你需要手动创建默认目录，或者总是使用--components-path


