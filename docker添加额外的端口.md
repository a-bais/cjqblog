---
title: docker添加额外的端口
date: 2021-08-29 21:14:11
tags: nginx docker
categories: docker学习笔记
---

<!--more-->

查看容器的对应宿主机的端口映射
docker container inspect know_nginx
![在这里插入图片描述](https://img-blog.csdnimg.cn/f3ec148e85784d0e95c280f03f160845.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
docker ps -a:
![在这里插入图片描述](https://img-blog.csdnimg.cn/ca195d750d3c4929856c08331172b55d.png)

给已经存在的容器添加端口(也可以同时制定添加ip,不过很少制定ip就是了)

先关闭docker systemctl stop docker

从上边的命令截图中留意到要修改的容器的id

进入/var/lib/docker/containers/:![在这里插入图片描述](https://img-blog.csdnimg.cn/677da7a234f64b219fa324574c5a9f3e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

进入制定目录:
![在这里插入图片描述](https://img-blog.csdnimg.cn/aa14f1ca3d8547aeb8d45f60e8fcadbc.png)

进入hostconfig.json:
在PortBindings字典里加上，如图
,"5000/tcp":[{"HostIP":"","HostPort":"80"}]
![在这里插入图片描述](https://img-blog.csdnimg.cn/fe6499679e5c4984a563054335ff51bd.png)

进入config.v2.json:
修改Exposedports,加上"5000/tcp":{}
![在这里插入图片描述](https://img-blog.csdnimg.cn/45a70359598b4cebadaebaadd2388484.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
重启docker systemctl restart docker
用docker ps -a和docker inspect know_nginx

附:
一定要先关了容器再配置
docker的start只有第一次有用例如docker run是(docker run = docker create+docker start)

