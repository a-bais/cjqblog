---
title: nginx记录
date: 2021-08-08 22:13:03
tags: 
categories: 重点实操记录 Nginx学习笔记 nginx细节配置记录
---

<!--more-->

**以下在代码段中的中文都是注释，并非真的文件内容**

这里的nginx用的是yum来安装
/etc/nginx/nginx.conf:
```bash
user  nginx;
  2 worker_processes  auto;
  3 
  4 #error_log  /var/log/nginx/error.log notice;   默认的错误日志配置
  5 pid        /var/run/nginx.pid;
  6 
  7 
  8 events {
  9     worker_connections  1024;
 10 }
 11 
 12 
 13 http {
 14     include       /etc/nginx/mime.types;
 15     default_type  application/octet-stream;
 16 
 17     log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
 18                       '$status $body_bytes_sent "$http_referer" '
 19                       '"$http_user_agent" "$http_x_forwarded_for"';
 20 
 21     log_format know_log '$remote_addr - $remote_user [$time_local] $http_host  "$request" $request_time '
 22                         '$status $body_bytes_sent "$http_referer" '
 23                         '"$http_user_agent" "$http_x_forwarded_for"';   自定义的日志格式know_log
 24 
 25     #access_log  /var/log/nginx/access.log  main;   默认的访问日志
 26 
 27     sendfile        on;
 28     #tcp_nopush     on;
 29 
 30     keepalive_timeout  65;
 31 
 32     #gzip  on;
 33     include /etc/nginx/conf.d/*.conf;
 34 
 35 }
~                                 
```

/etc/nginx/conf.d/know.conf:
```bash
limit_conn_zone $binary_remote_addr zone=know_conn:10m;   连接数限制的zone规则
  2 limit_req_zone $binary_remote_addr zone=know_req:10m rate=1r/s;   请求限制的规则
  3 server{
  4   listen 80;
  5   server_name know.com;
  6   access_log /nginx_log/know_access.log know_log; 调用自定义日志
  7   error_log /nginx_log/know_error.log notice;自定义日志
  8 
  9   location / {
 10     root /web/know;
 11     index index.html;
 12     limit_conn know_conn 1;   调用连接数限制
 13     limit_req zone=know_req burst=3 nodelay;   调用请求数限制
 14   }
 15   location = /download {
 16     root /web/know;
 17     autoindex on;  开启索引
 18     autoindex_exact_size on;   确切大小，bytes
 19     autoindex_localtime on;   使用服务器的文件时间，off则是GMT时间
 20     charset utf-8,gbk;   解决默认的中文目录乱码问题
 21     auth_basic "Please enter username and password";   用户认证开启
 22     auth_basic_user_file /etc/nginx/.auth_passwd.conf;   用户认证的文件
 23 
 24   }
 25   location /nginx_status{
 26   stub_status;   开启状态监控
 27   access_log off;   必须的，不把这页的访问写入日志
 28   allow 192.168.160.138;   默认先允许就拒绝所有
 29   deny all;
 30   }
 31 }

```


/etc/nginx/conf.d/shangmei.com:
```bash
server {
  2         listen 80;
  3         server_name shangmei.com;
  4         access_log /nginx_log/shangmei_access.log know_log;
  5         error_log /nginx_log/shangmei_error.log warn;
  6 
  7         location / {
  8                   root /web/shangmei;
  9                   index index.html;
 10       }
 11 }
~                                                                                                                                                                                     
~             
```

/etc/nginx/conf.d/linux.com:

```bash

```server {
  2         listen 80;
  3         server_name linux.com;
  4         access_log /nginx_log/linux_access.log know_log;
  5         error_log /nginx_log/linux_error.log warn;
  6 
  7         location / {
  8                   root /web/linux;
  9                   index index.html;
 10       }
 11 }


/etc/nignx/conf.d/default.conf:
![在这里插入图片描述](https://img-blog.csdnimg.cn/717bf303f94e4dce95259284aca04553.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

下载wordpress源码:
![在这里插入图片描述](https://img-blog.csdnimg.cn/ea33d6e9ebf4470a98afa8587011b072.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)


