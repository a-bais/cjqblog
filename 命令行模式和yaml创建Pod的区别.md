---
title: 命令行模式和yaml创建Pod的区别
date: 2021-11-13 08:25:58
tags: kubernetes nginx docker
categories: k8s
---

<!--more-->

命令行创建pod，要通过创建控制起来实现pod的创建
如：
kubectl run nginx --image=nginx:1.17.1 -n dev --port=80
注意这里的nginx是控制器的名称

而yaml不同，yaml可以直接创建个pod，即再kind中设置为pod
![在这里插入图片描述](https://img-blog.csdnimg.cn/a19fbdc386934ac5ae0fa830e70cb526.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

