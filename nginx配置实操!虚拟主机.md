---
title: nginx配置实操:虚拟主机
date: 2021-09-25 20:24:08
tags: nginx
categories: nginx配置实操（yum方式安装）
---

<!--more-->

虚拟主机一般是用来实现一个服务器实现多个站点
实现虚拟主机的方式有:
1.基于不同ip实现（少用）
2.基于同ip不同端口实现
3.基于同ip同端口不同域名实现

**基于ip:**

情况一般是该机器有多张网卡，或者说一张网卡有多个网络接口
**无非就是多个ip地址**

```bash
给一张网卡添加多个网络接口来实现多个ip
下边的操作对象是ens33

ifconfig ens33:1 192.168.100.2
ifconfig ens33:2 192.168.100.3

接下来设置配置文件即可
vim /etc/nginx/conf.d/xxx.conf
将其中的listen设置为如下
listen 192.168.100.2:80
修改其他站点配置文件，设置listen采用不同的ip，便可以实现多站点了

访问的时候浏览器输入不同的ip即可访问同一机器的不同站点，可以不输入:80，毕竟80是默认的

值得注意的是这些ip不是公网的ip，所以需要在/etc/hosts添加下dns解析

然后浏览器刚测试时可能会发现去的站点不对，这是因为之前使用浏览器留下的缓存，因为url解析先是看浏览器缓存再到本地hosts文件再到本地dns服务器再到互联网dns服务器,这时候可以restart或reload服务，多刷新几次1就可以了
```

**基于端口**
![在这里插入图片描述](https://img-blog.csdnimg.cn/76897932385140b1900e8fb0779563bb.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
也有可能遇到浏览器缓存问题，跟上边一样，多刷新几次
访问时输入ip:port来访问不同的站点

**基于域名**

vim /etc/nginx/conf.d/xxx.conf
不同的站点**server_name这里写不同的域名即可**
值得注意的是要在/etc/hosts下对这些域名进行dns解析记录
访问时输入域名来访问不同的站点

附:修改配置文件一般配置不会直接生效，需要restart或reload服务
