---
title: docker commit
date: 2021-08-30 20:12:24
tags: docker
categories: docker学习笔记
---

<!--more-->

通过一个容器生成镜像，容器依然存在。
**docker container commit 容器名或id 镜像名:版本号**
版本号可以不指定，默认latest
