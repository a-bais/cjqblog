---
title: git学习与常用精华总结
date: 2021-09-07 22:43:21
tags: git
categories: 主要#笔记资料 git
---

<!--more-->

**版本控制系统系统**

集中在一个系统中记录每一次文件的变化，即版本记录

------

**git分布式版本控制系统**

会将远程代码仓库完整得镜像下来，可进行离线本地版本控制。
提交不依赖于网络，带有网络时在于远程仓库进行版本同步。![在这里插入图片描述](https://img-blog.csdnimg.cn/0c78632d559548dda1b213034b1c669c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**git安装**



> 配置yum源，使用阿里源，epel.repo和CentOs-Base.repo
> rm /etc/yum.repos.d/*
> wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
> wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
> 或者curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
> ...
> 关闭防火墙和selinux
> systemctl stop firewalld
> systemctl disable firewalld
> sed -ri '/^SELINUX=/c SELINUX=disabled/' /etc/sysconfig/selinux
> sed -ri '/^SELINUX=/c SELINUX=disabled/' /etc/selinux/config
> 这里的c是行匹配后修改
> ...
> 安装git
> yum install -y git
> git --version
> ...
> 设置git
> git config --global  user.name "cjq"
> git config --global user.email "cjq@mail.com"
> git config --global coloer.ui true
> git config --global color.status auto
> git config --global color.diff auto  比对
> git config --global color.branch auto  分支
> git config --global color.interactive auto 交互



```bash[user]
cat ~/.gitconfig      查看global配置
	name = cjq
	email = cjq@mail.com
[coloer]
	ui = true
[color]
	diff = auto
	status = auto
	branch = auto
	interactive = auto


```
git config -l 也可以列出配置，不过不够清晰

.git/config           当前版本库特定的配置文件， --file，Git的默认选项，具有最高优先级。
~/.gitconfig         当前用户的配置文件，--global。
/etc/gitconfig      系统级别的配置文件， --system 选项，优先级最低。

另外使用初次--file时可能会报错，解决方法是先mkdir ~/.git和建立.git/config文件

**git提交目录文件到本地仓库**

首先创建一个专用的目录即git版本库，这个目录下的所有文件都由git管理，文件的修改删除等都都可以跟踪，可以用来进行历史还原
![在这里插入图片描述](https://img-blog.csdnimg.cn/97e128d8a9ac4711becfaa7b5332502a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**将文件上传到暂存区 git add <filename>**
![在这里插入图片描述](https://img-blog.csdnimg.cn/f43406425d4b4c85a0ec38827e305aeb.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e6c2a11c16a749c8a0d74713b18b690c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**将暂存区的文件提交到本地仓库 git commit**

![在这里插入图片描述](https://img-blog.csdnimg.cn/4f82225322764207bff63fad57ea3da8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
git status看暂存区和工作区

附:git commit -m ""   :提交暂存区的文件
   git commit -am "" ：提交跟踪过的文件
-m:描述   -a:在commit时，能省略git add,但这样要求文件已经被追踪过了，即修改和删除文件有效，如果是新文件还是要先git add,即非untracked状态才行

git rm file = rm file +git add file
用于删除暂存区或分支上的文件，同时工作区页不在需要这个文件
git add file是提交删除的步骤到git仓库，同步到仓库

git rm --cached file
用于删除暂存区或分支的文件，不删除本地文件，下次的commit行为会修改git仓库
有些文件不行提交到git仓库但又被检测到有所变化，untracked file,也可以使用这命令停止跟踪这些文件

**暂存区，工作区，本地文件比对**

git diff file 工作区与暂存区文件内容比对
修改file,git diff file可以看见内容变化，将文件提交到暂存区在比对，不会有任何变动呈现
![在这里插入图片描述](https://img-blog.csdnimg.cn/f7d1d4f3aaf74f5c9eb620f939a5ca08.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
即git diff file用于文件修改后查看文件的变化

**暂存区与本地仓库文件内容比对git diff --cached file**

修改文件后，可以用这命令来查看暂存区与本地仓库的文件差异，提交到本地仓库后再比对会发现无差异
![在这里插入图片描述](https://img-blog.csdnimg.cn/014f5b58286b458f9619257853894339.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**git commit详解**

commit提交，相当于对文件进行每一次的快照
git log可以查看commit每次内容
![在这里插入图片描述](https://img-blog.csdnimg.cn/a54c4eeca8da40939eb6b56e617484e7.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**git回退操作**

![在这里插入图片描述](https://img-blog.csdnimg.cn/335783fb533b4333ae2ffe9e1b5c6dbe.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**工作区回退操作**
![在这里插入图片描述](https://img-blog.csdnimg.cn/e8abe5c585f34546ad7a4016f16be7e2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**暂存区回退到本地文件**
有时候修改文件并且提交到暂存区才发现代码有问题，先从暂存区回退到工作区，早回退到本地文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/0cf51c52e41b4e6b8ab1997854c1d42a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**已经上传到本地仓库后，从仓库回退**
![在这里插入图片描述](https://img-blog.csdnimg.cn/d8c12a06e86b4baba20098da64ad6bf9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d62345af7f1e4f849e649fe1b46c8155.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2083d481aaa84a3782907b182050f0e5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
附:git reflog可以查看左右分支的所有操作记录，即commit和reset记录，包括已删除了的commit记录，git log不能查看已删除的commit记录

git reset 默认git reset --soft,回退到某个版本，值回退commit信息不回恢复到index file一级，可以再次提交试用commit即可
git reset --hard,彻底回退到某个版本，本地的文件也会改变为指定的回退的版本内容

**git分支及其操作的详解**

多开发组开发项目，项目建立时会有个主干分支master,从主干上拉去代码下来会形成一个分支，分支可有无数个。开发人员在自己的分支上修改代码，检查无误后，先将master上的代码拉去下来与分支测试，在将分支代码与master合并。

**创建分支**
git branch 分支名   #创建分支
git branch -d 分支名   #删除分支
 
注意：操作在git目录下进行
![在这里插入图片描述](https://img-blog.csdnimg.cn/698d8a6e611b4d95b81e8fbced8f8f7e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
Deleted branch test (was aa2b925).
已删除的分支test（曾为 aa2b925）,这段信息可以留意下

**切换分支**
![在这里插入图片描述](https://img-blog.csdnimg.cn/d10e574be30d43cc94ca8c00eaa35a8a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**在test分支写代码，或者修改已经有的代码添加新的功能，并且提交到本地仓库**
![在这里插入图片描述](https://img-blog.csdnimg.cn/d14ebad3be524acbbd604ccdc11a61f2.png)
**分支代码合并**
git merge master，意为在test即当前分支合并master分支代码
![在这里插入图片描述](https://img-blog.csdnimg.cn/68cb8b3a3d7248388eb4003da6a28eee.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**合并分支时出现冲突及解决**
同一份代码文件同样位置的内容被不同的开发同时处理，提交时就会出现合并冲突
![在这里插入图片描述](https://img-blog.csdnimg.cn/3adffd8d877d444b9cd365d29a5eeea9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2ae831edd55448b9a3f13b88a533e5ae.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/85f737d9536240acb278ec758496f11a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**git标签**

tag
commit id 不好记，tag可以打一个标签，打一个易记忆的名字

git tag -a "标签名" -m "描述信息"   #增加一个标签，默认给最新的代码添加标签
git tag -d 标签名   #删除标签
git show 标签名   #查看标签的属性

git tag -a "标签名" commit id -m "描述信息"   #指定某个commit打标签
![在这里插入图片描述](https://img-blog.csdnimg.cn/ae42340f9e6d45f3bb81af237f1552d6.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/cbc7da361b74474caff0b2198979e22d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
查看commit id可以用git log

**远程仓库码云gitee**

有网络时将本地仓库推送到远程仓库，一个远程仓库对应一个本地仓库，代码可以所有人下载
前往码云https://gitee.com/
登录自己的账号
![在这里插入图片描述](https://img-blog.csdnimg.cn/843705ff0d7f4d13b7f92d539b7763da.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/81300876f0f64a08b2a0aa7e9c6a23b9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e0993e7af74147589a52aab4af8e0ec5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d016cf8502e846af8d901152fccaaaf2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/deb18e80bbdc49b3b713549db4ffa46a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**创建一个远程仓库**
![在这里插入图片描述](https://img-blog.csdnimg.cn/ff1c164ad9ee499aadac432fbbf8f1fb.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/12d7636bc53b4d2e91096cb0df83f410.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**实操远程仓库**

先将仓库所有代码克隆一份到本地，交给开发使用，修改代码后提交给远程仓库(第一次获取仓库代码时要使用克隆)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b7f18a8474ca4ca1bb384cc0fa958370.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/ba062bdcb21e4ece91bdebeec391771d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
附：git push <远程主机名>  <本地分支名>:<远程分支名>
远程分支名可以没有，默认同名
git push origin :master == git push origin --delete master
省略本地分支名即删除意思
git push origin 意思将当前分支推送给origin主机
git push 如果当前分支只有一个追踪分支，主机名也可以省略了
