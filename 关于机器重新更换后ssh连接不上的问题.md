---
title: 关于机器重新更换后ssh连接不上的问题
date: 2021-10-27 17:24:03
tags: ssh 服务器 linux
categories: 解决错误问题
---

<!--more-->

做个备忘:
原本两台机器已经ssh过了，注意ssh过了在/root/.ssh/下有个known.hosts的文件，里边记录了相应的主机和密钥，
这是我将一台机器重新装，ip不变，此时旧的机器ssh这一台机器是不成功的，根据提示将/root/.ssh/known.hosts删除，重新ssh即可

附:scp使用到ssh
