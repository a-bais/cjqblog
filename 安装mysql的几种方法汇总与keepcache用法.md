---
title: 安装mysql的几种方法汇总与keepcache用法
date: 2021-08-09 18:04:31
tags: mysql 数据库
categories: 主要#笔记资料 数据库mysql mariadb redis 常用软件的安装
---

<!--more-->

**方法一：**
[https://blog.csdn.net/weixin_45843419/article/details/118410378](https://blog.csdn.net/weixin_45843419/article/details/118410378)
**方法二:**
比第一个更方便
先去**repo.mysql.com**，直接进入yum目录，找到要安装的版本的mysql对应的yum源，复制链接地址下载，wget下载下来，yum repolist刷新yum源后，就可以yum install 了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/8e1c388b16b4411d9bab02f28fc678cd.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

-----------------------

接下来说说keepcache：
/etc/yum.conf文件，设置keepcache=1,就是保持缓存，默认是0，yum或者rpm安装软件包后就会自动将安装包删除
yum install mysql-community-server --downloadonly --downloaddir=/root/soft,将文件下载下来还未安装。
![在这里插入图片描述](https://img-blog.csdnimg.cn/ae05b3bf6f874607a0e306ffe6c5624a.png)

然后可以用yum localinstall 或者rpm -ivh加包名来安装就行。

yum localinstall，yum相关，会自动解决依赖问题
如果没有依赖，同时网速不太好，建议用rpm -ivh
yum remove也会自动删除依赖

实际测试现象:即使keepcache=0，也不见得就一定会安装后删除安装包，目前看来此设置无关紧要。


附:已经yum安装了mysql,并且使用过了一段时间，yum remove再一次yum安装后，原本数据库内容还在，一般yum remove，不会删的太干净，数据库内容还在，例如systemctl设置给mysql的enabled还在。
第一次安装**mysql5.7**,它默认配置了密码，需要从/var/log/mysqld.log过滤出密码，grep "temporary password"/。
mysql登录默认-h localhost,localhost对应于/etc/hosts里边的localhost，-h接ip也行，主要要在/etc/hosts下有配置。
