---
title: 初始化集群时的错误[ERROR ImagePull]: failed to pull image k8s.gcr.io/coredns/coredns:v1.8.4: output: Error re
date: 2021-10-31 00:39:49
tags: docker 容器 运维
categories: docker学习笔记 k8s
---

<!--more-->

这里错误是因为没有下载到这个k8s.gcr.io/coredns/coredns:v1.8.4镜像，同时它去官网又下载不了，这里有个坑，按照别的文章帮你把标签一般会是**k8s.gcr.io/coredns:v1.8.4**这样，而新版本的要求是k8s.gcr.io/coredns/coredns:v1.8.4，所以解决办法是docker tag改下标签即可，
![在这里插入图片描述](https://img-blog.csdnimg.cn/5a49b046f6fb4506bcc6837c01244375.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

