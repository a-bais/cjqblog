---
title: docker start -i启动容器时直接进入交互式
date: 2021-08-29 09:58:23
tags: docker
categories: docker学习笔记
---

<!--more-->

docker start -i启动容器时直接进入交互式

docker start -i 容器id或名称
不像exec和docker run可以-it然后指定个/bin/bash,它只有-i.
建议用exec
