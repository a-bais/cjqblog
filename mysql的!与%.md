---
title: mysql的*与%
date: 2021-09-29 13:48:24
tags: mysql
categories: 数据库mysql mariadb redis
---

<!--more-->

*通常用于全选某个或某些东西，如：
select * from tb1 ;
grant all on db1.* to username@ip;

%则作为通配符，如：
grant all on db1.* to username@'192.168.160.%';
greant all on db1.* to username@'%';
