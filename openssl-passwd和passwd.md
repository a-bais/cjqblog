---
title: openssl-passwd和passwd
date: 2021-07-31 21:18:05
tags: 
categories: 解决错误问题
---

<!--more-->

passwd：
echo '123456' | passwd --stdin name
openssl-passwd:
echo '123456' | openssl-passwd -1 -stdin
