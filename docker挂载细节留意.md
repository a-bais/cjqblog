---
title: docker挂载细节留意
date: 2021-08-29 17:46:58
tags: docker linux
categories: docker学习笔记
---

<!--more-->

-v src:dest可以实现挂载，该目录和文件同时由容器和宿主机拥有，均可修改
**加粗样式**
/root/dirtest下有test普通文件**加粗样式**
docker run -d -P -v /root/dirtest:/usr/share nginx:latest
将整个目录挂载下来，即/usr/share目录成为进入/root/dirtest目录的入口，/usr/share下的文件与/root/dirtest看到的底下的文件一样
这里与挂载的覆盖相关，建议使用新的挂载点，如:
docker run -d -P -v /root/dirtest:/usr/dirtest nginx:latest
