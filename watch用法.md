---
title: watch用法
date: 2021-08-06 10:20:09
tags: 
categories: 解决错误问题
---

<!--more-->

动态观察/var/log/messages
watch -n "tail /var/log/messages"

同样的效果实现
tailf /var/log/messages
tail -f /var/log/messages
