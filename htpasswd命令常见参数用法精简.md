---
title: htpasswd命令常见参数用法精简
date: 2021-08-25 22:23:04
tags: linux centos
categories: 主要#笔记资料
---

<!--more-->

```bash
-c创建一个加密文件
-b直接在命令行输入用户名和密码而不是在提示中输入
-n不更新加密文件，将加密后的用户名和密码作为标准输出输出在屏幕上
-p不对密码加密，即明文密码
-D删除指定用户
-m采用MD5算法对密码加密(默认)
-d采用CRYPT加密
-s采用SHA算法加密
-B强制对密码使用bcrypt加密(非常安全)
```

**没有先指定加密文件的情况可以这样写
htpasswd -Bbn aaa aaa >> registry_htpasswd**

> 创建,passwd,默认MD5算法对密码加密
> htpasswd -bc .passwd aaa aaa
> .............................
> 添加一个用户和密码  
> htpasswd -b .passwd bbb bbb
> .............
> 不更新密码文件
> htpasswd -nb ccc ccc
> ..................
> 删除用户和密码
> htpasswd -D .passwd aaa
> ........
> 修改密码 
> htpasswd -b .passwd aaa bbb
