---
title: nginx启动失败问题
date: 2021-07-27 16:49:35
tags: 
categories: 解决错误问题 Nginx学习笔记 nginx排错
---

<!--more-->

> [root@www ~]# systemctl start nginx
Job for nginx.service failed because the control process exited with error code. See "systemctl status nginx.service" and "journalctl -xe" for details.

刚安装好nginx后启动会唱遇到这问题，一般是软件httpd冲突，解决方法简单

```bash
systemctl stop httpd
systemctl start nginx
```

