---
title: nginx配置实操:常用模块之目录索引
date: 2021-09-18 09:33:07
tags: nginx
categories: nginx配置实操（yum方式安装）
---

<!--more-->

目录索引的配置区域有http（所有站点生效），server（单个站点生效），location（单个页面，最常用）
```bash
vim /etc/nginx/conf.d/autoindex.conf

server{
        listen 80;
        server_name a1.com;
        location / {
                root /web/autoindex;
                index index.html;
        }
        location /download{
                root /web/autoindex;
                autoindex on;			#开启目录索引
                autoindex_exact_size on;			#显示文件的确切大小，单位byte
                autoindex_localtime on;			#显示服务器的文件时间，off则是GMT时间
                charset utf-8,gbk;			 #解决目录的中文乱码问题
               								#使用了autoindex就不用了在创建index了
        }
}

```

```bash
vim /etc/hosts          #做好本机的dns解析，便于测试
mkdir -p /web/autoindex
cd /web
vim index.html
cd /autoindex

mkdir d1
touch t1
cd d1
touch t2
```

细节记录:

1
![在这里插入图片描述](https://img-blog.csdnimg.cn/29a47223b1474d90aab551f90f36f1c4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

2
![在这里插入图片描述](https://img-blog.csdnimg.cn/e6963cebafdd43f88e7b3e880c57896d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

3
listen和server_name一般设置在server{}中
root和index一般设置在location{}中
