---
title: sed的i与r的使用注意
date: 2021-10-26 20:47:41
tags: sed
categories: 解决错误问题
---

<!--more-->

```bash
sed -ir '/ swap / s/^\(.*\)$/#\1/' /etc/fstab 或者 sed -r '/ swap / s/^(.*)$/#\1/' /etc/fstab > /test && cat /test > /etc/fstab
```

```bash
从头到尾匹配
r,支持扩展元字符，所以^(.*)$
不支持扩展正则的话，那就^\(.*)$
```


