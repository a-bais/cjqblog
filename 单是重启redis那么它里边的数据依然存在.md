---
title: 单是重启redis那么它里边的数据依然存在
date: 2021-09-16 10:31:56
tags: redis 数据库
categories: 数据库mysql mariadb redis
---

<!--more-->

redis-server /data/redis_cluster/redis_6379/conf/redis_6379.conf
可以重启redis,但用redis-cli进入之后你会发现redis中的key都还存在

正确清除redis的key除了flushdb外，**还可以先shutdown，之后再重启redis**
