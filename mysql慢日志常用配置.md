---
title: mysql慢日志常用配置
date: 2021-10-06 09:57:05
tags: mysql
categories: 数据库mysql mariadb redis
---

<!--more-->

> set global slow_query_log=1;
> set global long_query_time=10;
> set global slow_query_log_file ~/hostname-slow.log
> set global log_queries_not_using_indexes=1;
> set global log_throttle_queries_not_using_indexes=0
> 
> ---
> 测试时
> set session long_query_time=1;
> set session log_throttle_queries_not_using_indexes=0

![在这里插入图片描述](https://img-blog.csdnimg.cn/c360f60fa7d74fc5b21bb94c77a2c6ea.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/cfc1fee4d8924137aa5aa87775d1ec37.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

