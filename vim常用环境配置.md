---
title: vim常用环境配置
date: 2021-07-31 07:45:50
tags: vim
categories: vim常用配置与问题解决
---

<!--more-->

vim的环境配置一般在/etc/vimrc下，不建议修改这个文件，修改**vim ~/.vimrc**
![在这里插入图片描述](https://img-blog.csdnimg.cn/c6362a3eb061417597c3a3dc733016ba.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

