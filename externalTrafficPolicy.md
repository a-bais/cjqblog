---
title: externalTrafficPolicy
date: 2022-03-07 21:23:46
tags: kubelet kubernetes 容器
categories: k8s
---

<!--more-->

获取实际客户端地址，可以在yaml中设置externalTrafficPolicy(nginx日志有关)
但是观察流量发现，这时候按节点来进行负载而不是pod，负载均衡效果不好，所以可以对pod中/etc/nginx/nginx.conf中使用$http_x_forwarded_for来获取实际客户端地址

附:pod是创建好了再调度，pod是逻辑概念，实际上是在调度后的节点上下载镜像创建容器
