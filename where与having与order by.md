---
title: where与having与order by
date: 2021-09-29 10:17:51
tags: html
categories: 数据库mysql mariadb redis
---

<!--more-->

![在这里插入图片描述](https://img-blog.csdnimg.cn/095ded080ded4304b6394ab61091d32c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
where的字段来源是表中已有的
having操作对象可以是聚合函数也可以是别名，但不接受来自表中的字段名，**除非该字段是分组字段**

补充:order by 支持表中字段和别名和聚合函数

设置别名时最好不要与表中的字段同名
**分组查询时，select的对象最好是聚合函数和组名，，其中聚合函数的对象是分组筛选出来的行的字段**
