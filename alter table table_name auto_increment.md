---
title: alter table table_name auto_increment
date: 2021-10-02 21:58:32
tags: mysql
categories: 数据库mysql mariadb redis
---

<!--more-->

对列只能设置auto_increment，而不能设置自增的初始值

对表可以进行重新设置自增初始值操作
alter table table_name auto_increment=1;

特殊情况比如主键约束或者unique字段不可重复的，所有有时候设置初始值虽然不会报错但是无效，这时可以选择设置成别的自增值
