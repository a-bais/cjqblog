---
title: ip a只有mac地址没有ip地址
date: 2021-08-17 09:04:28
tags: 1024程序员节
categories: 解决错误问题
---

<!--more-->

问题原因有几种，常见的问题情况是网络开关没打开，做法将设置中的网络开关打开。(配置文件的ONBOOT=yes，这里也要打开)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d9687ecf9a30404db4b4887457a63427.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
当修改/etc/sysconfig/network-scripts/ifcfg-ens33
使用静态地址，同时设置了ip，网络掩码，网关和dns,
值得注意有两点dns设置是要写dns1或dns2
还有就是这几点配置错了，比如字母少了，重启网络服务systemctl restart network可能是不会报错，但是ens33就是没有ip,reboot后会自动分配个ip，说明了配置文件配错了，默认会使用动态分配ip的方式

总结就是，一开启网络开关，最好装机的时候就打开，二是配置文件一定要写正确了

如：
![在这里插入图片描述](https://img-blog.csdnimg.cn/bcc14f311a58488b97752324644816ab.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
