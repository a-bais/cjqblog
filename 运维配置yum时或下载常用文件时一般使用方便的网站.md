---
title: 运维配置yum时或下载常用文件时一般使用方便的网站
date: 2021-08-26 11:20:14
tags: docker 运维 centos
categories: 零散
---

<!--more-->

**http://mirror.aliyun.com/repo**
epel-7.repo
Centos-7.repo

```bash
wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/epel-7.repo
wget -O /etc/yum.repos.d/Centos-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/2a2a13313bb1496d9de8a0db2ffef691.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
建议使用yum时统一源，方便依赖，大多数情况下我用的是国内的阿里源

————————————————————————
**https://mirrors.tuna.tsinghua.edu.cn/help/docker-ce/**![在这里插入图片描述](https://img-blog.csdnimg.cn/e2b7c7c95c004e0f85a567429ab49eb8.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_Q1NETiBA6Zi_55m977yM,size_45,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6de3222647c04febb6e33c90afedc703.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_Q1NETiBA6Zi_55m977yM,size_56,color_FFFFFF,t_70,g_se,x_16)
-------------
中国科技大学
http://mirrors.ustc.edu.cn/
![在这里插入图片描述](https://img-blog.csdnimg.cn/44cbd7b666f641d1bacd19c8d50d562c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
------------------
华为云
mirrors.huaweicloud.com
repo.huaweicloud.com
（下载redis常用）
![在这里插入图片描述](https://img-blog.csdnimg.cn/e10c5808fd7948d28c5dd4a5294822c5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
