---
title: yum安装nginx时指定使用官方源
date: 2021-07-27 22:06:34
tags: 
categories: 解决错误问题 Nginx学习笔记 nginx排错
---

<!--more-->

一开始我的机器上有epel和nginx的官方nginx仓库，用yum安装是使用了参数--enablerepo=nginx-stable,下载完成后yuminfo nginx发现来自的源还是epel,epel较新同时会功能不够齐全，这边也不知道为什么指定yum源无效，所以就先用管理工具暂停了epel源，重新下载安装，成功
