---
title: prometheus安装与开启并设置开机自启
date: 2022-01-02 00:00:27
tags: kubernetes promethes
categories: prometheus
---

<!--more-->

1.下载对应版本的promethues压缩包（二进制包）
复制链接地址用wget下载下来
![在这里插入图片描述](https://img-blog.csdnimg.cn/fa37a255783b438e8dfa4a5b48ecdf29.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)


2.tar -x -f解包，会得到一个对应的目录，将该目录移动或拷贝到/opt/prometheus下，/opt/下没有这个目录可以自己建立
![在这里插入图片描述](https://img-blog.csdnimg.cn/1958edc0151644848877636e9da3c1d2.png)


3.启动，prometheus的启动需要yaml配置文件，该目录下，执行命令  nohup ./prometheus --config.file=prometheus.yml >/dev/null 2>&1 &

4.这是常规配置：
cat prometheus.yml | grep -v "^#" | grep -v "#\(.*\)" | grep -v "^$"
![在这里插入图片描述](https://img-blog.csdnimg.cn/97561f538f2a4df69ef5196048964072.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
localhost可以改成你的主机详细的静态ip

5。设置开机自启
将sudo nohup  /opt/prometheus/prometheus-2.32.1.linux-amd64/prometheus --config.file=/opt/prometheus/prometheus-2.32.1.linux-amd64/prometheus.yml >/dev/null 2>&1 &加入到/etc/rc.d/rc.local文件中，同时添加执行的权限chmod +x /etc/rc.d/rc.local

注意不要加bash之类的这个执行文件不是shell脚本

