---
title: wordpress,webtatic,wecenter简介
date: 2021-10-01 10:18:45
tags: php mysql centos
categories: 零散
---

<!--more-->

**wordpress:**
wordpress是基于php和mysql开发的个人博客系统，同时逐渐演化内容管理系统(CMS),用户可以在支持php和mysql的服务器上搭建wordpress网站。
搭建wordpress需要下载网站源码压缩包
相关网站:
http://wordpress.org
http://cn.wordpress.org

**webtatic:**
我一般使用webtatic的yum源来安装php，为 CentOS/RHEL 管理员提供最新的稳定的 web 开发/托管软件的次要版本，这些在 CentOS/RHEL 发行版次要版本中没有提供。
相关网站:
http://mirror.webtatic.com
http://webtatic.com

**wecenter:**
WeCenter 是一款建立知识社区的开源程序。
以可检索、归类、整理的问答形式，
帮助企业、学校、政府机构等组织搭建自己的知识库，
用户可主动查询获取产品和服务的说明内容，
通过问答互动，加强组织和用户之间的社交粘性。

简单理解：问答系统，类似知乎
![在这里插入图片描述](https://img-blog.csdnimg.cn/44cef6113d0b442885806500e9dfd77c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
用火狐好点，其他浏览器下载一般会有压缩包不完整的情况

相关网站:wecenter.com
