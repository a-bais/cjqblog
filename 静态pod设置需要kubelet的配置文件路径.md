---
title: 静态pod设置需要kubelet的配置文件路径
date: 2022-03-01 10:47:27
tags: kubelet kubernetes 容器
categories: k8s
---

<!--more-->

rpm -ql kubelet

建议用systenctl status kubelet(部署情况多样，这个看准确点)
![在这里插入图片描述](https://img-blog.csdnimg.cn/1bb548eb1e2948f8b0289d617f06e152.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
进入配置文件查看：
![在这里插入图片描述](https://img-blog.csdnimg.cn/4750142ee8d54c89a4c2ddd391d26b0a.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
配置文件部署静态pod，默认kubeadm方式配置文件路径/etc/kubernetes/manifests
