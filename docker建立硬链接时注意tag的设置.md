---
title: docker建立硬链接时注意tag的设置
date: 2021-08-27 12:38:17
tags: docker
categories: docker学习笔记
---

<!--more-->

docker image tag httpd:latest httpd_test,这样设置，用docker images查看会发现新的链接httpd_test的tag默认是**latest**,这个latest是默认的，与旧镜像的tag无关，比如是对httpd:2.4建立个硬链接，不指定版本的号的话，会默认latest

指定tag:
docker image tag httpd:latest httpd_test:tag
