---
title: python获取图片url与海量下载图片
date: 2021-05-12 23:27:22
tags: 
categories: 主要#笔记资料
---

<!--more-->

**python获取图片url与海量下载图片**
爬取静态的和爬取动态的。
有些页面打开你不断下拉它不断的加载出图片便是动态的。
静态爬取则是针对一页一页翻页的。
爬虫是一种去网页上爬取数据的程序
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512171440327.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021051210475985.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
这便是动态的，几乎加载不完

接下来改成静态的，翻页的
将index改成flip
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512105053889.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512105833153.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512125610982.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
inspect element检查元素
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512134736329.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512135229697.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512135152978.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512135434570.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512171640302.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
复制src然后去拿cbj-url:
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512171957540.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512172453817.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
拿到objurl后可以赋值给代码中的变量url，即可拿到这一张照片
（一般objurl是原图大图）
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512200954977.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512203212847.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
拿到的图片的url的后缀不一定都是.jpg
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512212836650.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512212925749.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
对于没有后缀名的强制更改它的格式加上一个后缀:

wb:    以二进制格式打开一个文件只用于写入。如果该文件已存在则将其覆盖。如果该文件不存在，创建新文件。
wb+:以二进制格式打开一个文件用于读写。如果该文件已存在则将其覆盖。如果该文件不存在，创建新文件。
f.close() 关闭文件
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512232034675.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
下面为完整的代码:
**************************************************
> #!/usr/bin/env python3
import requests
import re
#import ssl
#ssl._create_default_https_context = ssl._create_unverified_context
s = requests.Session()
s.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36' 
#1确定url
url = "https://image.baidu.com/search/flip?tn=baiduimage&ipn=r&ct=201326592&cl=2&lm=-1&st=-1&fm=result&fr=&sf=1&fmq=1620796550603_R&pv=&ic=&nc=1&z=&hd=&latest=&copyright=&se=1&showtab=0&fb=0&width=&height=&face=0&istype=2&ie=utf-8&hs=2&sid=&word=%E6%B0%B4%E6%9E%9C"
#"%E6%B0%B4%E6%9E%9C"

#2请求
res = s.get(url).text
#text默认utf-8编码，指定编码:ret为get1拿到的对象即ret=requests.get(url),ret.content.decode("utf-8")
#print(res)

#3删选数据
image_urls = re.findall('"objURL":"(.*?)"',res)
#findall找出所有
#print(image_urls)
for image_url in image_urls:
    #print(image_url)
    image_name = image_url.split('/')[-1]
    image_name=image_name.split("&")[0]
    #print(image_name)

    #.search()方法用于在整个字符串中搜索第一个匹配的值，匹配成功返回一个March对象，否则返回None,注意操作对象是字符串
    image_end = re.search("(.jpg|.png|.jpeg|.gif)$",image_name)#表示从末尾开始
    if image_end == None:
        image_name = image_name + ".jpg"
    image = requests.get(image_url).content
    #print(image_name)
   
    #4保存
    #路径，.为相对路径写法,wb允许文件写入二进制数据,%s字符串类型，%d%f
   
    with open('/root/python_code/day05/ima/%s'%image_name,'wb') as file: 
        file.write(image)
        file.close()
*************************************************************
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210512232646497.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)


