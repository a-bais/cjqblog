---
title: nginx做负载均衡时注意几点
date: 2021-08-18 12:50:07
tags: nginx
categories: 解决错误问题 重点实操记录 Nginx学习笔记
---

<!--more-->

1.upstream虚拟机池最好与均衡服务器间有了dns解析，如/etc/hosts,同一个网段，内网私网
2.虚拟机要关闭防火墙和selinux
3.web后端的nginx服务打开
4.ip_hash,同一个客户端ip访问，nginx反向代理会将请求一直转发到同同一个后端web
5.对于后端的web提供的自己独有的站点，负载调度均衡就不起作用了
6.ip_hash和weight不可以同时使用,url_hash也一样(url_hash需要安装nginx的hash包)
7.least_conn相当于lvs的wlc，考虑最少连接数的同时考虑权值，也就是**连接数/权值小**的优先连接
8.四层负载均衡时用的是本机ip，如ssh root@192.168.160138 -p7777
9.只能存在一个生效的四层负载均衡的配置文件
10.upstream中可以有多个主机，即四层负载均衡后进行七层负载均衡，如图:
![在这里插入图片描述](https://img-blog.csdnimg.cn/3c732d89141d46359a089c1c525738fb.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
上图的配置下，即使七层负载均衡中设置了ip_hash,还是会请求到不同的服务器，因为四层负载优先。weight的设置正常生效。
测试:curl 192.168.160.138:80
访问了后端的web服务器没有错，但是如果后端的服务器提供多个站点，则会有问题，时间发现总是联想一个固定的站点，原因与解决方案就不深究了。
11.upstream中配置多个server,需要对该目标主机的端口如22的http服务进行了七层负载的配置，否则会有错误
12.四层的配置文件，一个upstream没有特殊需求写一个server就可以
***********************
负载均衡后端状态:
down
backup
max_fails
fail_timeout
max_conn

负载均衡的调度算法:
轮询
加权轮询（常用，写法是跟在server行后）
ip_hash（常用）
urk_hash
least_conn

**13.注意/etc/hosts的配置，如使用curl时，要对负载均衡配置文件中的server_name进行域名解析设置，否则curl server_name时会出错**
14.upstream中不设置端口应该默认80，最好设置ip:port形式

