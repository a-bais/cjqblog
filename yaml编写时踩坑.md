---
title: yaml编写时踩坑
date: 2021-10-30 23:47:09
tags: yaml
categories: 解决错误问题 docker学习笔记 k8s
---

<!--more-->

记录一下今天编写yaml猜的坑，高初始化kubernetes集群是用到kubeadm-config,yaml，编写时用了tab键(**四个空格**)，报错了而且不是包tab键的错误，而是其他不想管行的错误，所以问题没那么直接定位到，价差文件后发现tab的问题，
yaml缩进很严格，两个空格为标准，而一个tab键默认缩进四个空格，所以错误
如果经常需要编写这种yaml文件，**可以在~/vimrc添加一行set tabstop=2修改tab一次缩进两个空格**，方便编写yaml文件
