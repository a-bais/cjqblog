---
title: micro、M3O微服务系列(一)
date: 2022-07-15 17:57:14
tags: 微服务 云原生 go micro M3O
categories: Go go和微服务零散
---

<!--more-->

**本系列以go，micro3为主**
@[TOC]
# 软件开发的预测
回望以前的软件开发的问题预计它的未来。改变正在进行中。我们正越来越多地走向一个由**技术驱动**的世界，这是每项业务的核心。在这个时代保持竞争优势变得越来越困难。当组织试图利用低效的平台、流程和结构进行扩展时，其执行能力可能会停滞不前。十年前的老科技公司经历了这些规模化的阵痛，并且大多数都使用相同的方法来克服这些挑战。
微服务是一种创造竞争优势的方式。
# 什么是微服务
微服务是一种软件架构模式，用于将大型单体应用程序分解为较小的可管理独立服务，这些服务通过**语言无关协议**(protocol buffers、grpc、micro)进行通信，每个服务都专注于做好一件事。(但注意不是让你将应用无限制的拆分，并不是什么都拆的越小越好)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b21e085316d34252aec9c975eea293fd.png)
微服务的概念并不新鲜，这是对面向服务架构的重新构想，但采用了一种更全面地与 unix 进程和管道保持一致的方法。

> 微服务架构的理念：
> 
> 这些服务是小型的——细粒度的作为一个单一的商业目的，类似于“做一件事，把它做好”的 unix 哲学
> 组织文化应该包含部署和测试的自动化。这减轻了管理和运营的负担 
> 文化和设计原则应该包含失败和错误，类似于反脆弱系统。

# 为什么使用微服务
随着组织扩展技术和人数，管理单一代码库变得更加困难。过去有的企业试图用一个单一的系统来扩展产品功能等，但由于系通过与庞大，这种改变带来的失败已经是家常便饭。
每个团队负责由许多微服务组成的业务功能，这些微服务可以独立于其他团队进行部署。**微服务系统可以实现更快的开发周期、更高的生产力和卓越的可扩展系统。**

> 1.更容易扩展开发——团队围绕不同的业务需求进行组织并管理他们自己的服务。
  2.更容易理解- 微服务要小得多，通常为 1000 LOC 或更少。
  3.更容易频繁地部署新版本的服务——服务可以独立部署、扩展和管理。
  4.改进的容错和隔离- 关注点分离可最大限度地减少一个服务中的问题对另一个服务的影响。
  5.提高执行速度——团队通过独立开发、部署和管理微服务更快地满足业务需求。
  6.可重用服务和快速原型设计——微服务中根深蒂固的 Unix 理念允许您重用现有服务并更快地在其上构建全新的功能。


# 什么是micro
Micro 是一个**微服务生态系统**(并不是一个简单的框架，里面内置的用go来开发微服务的框架罢了)，专注于提供产品、服务和解决方案，以实现现代软件驱动型企业的创新。计划成为任何与微服务相关的**事实上的资源**(就是开发者平台，以及提供大量便宜的现成的完善的API接口)，并将寻求使公司能够将这项技术用于自己的业务。从早期原型设计一直到大规模生产部署。

Go Micro 是构建 Go 微服务开发框架的早期尝试。此后，我们将重点完全转移到了 Micro，这是一个云原生开发平台，包括服务器、命令行和服务框架（以前的 go-micro）。Go Micro 已移至asim/go-micro。

Micro 与运行时无关。你可以在任何你喜欢的地方运行它。在裸机、AWS、谷歌云上。选择喜欢的容器编排系统（如 Mesos 或 Kubernetes）上运行。

软件行业正在发生根本性的转变。现有工具和开发实践在这个新时代无法扩展。没有为开发人员提供从单一代码库转向更高效设计模式的工具。大多数公司不可避免地会通过单体设计达到收益递减点，并且必须进行大规模的研发再造工作。Netfix、Twitter、Gilt 和 Hailo 都是这方面的主要例子。所有人最终都建立了自己的微服务平台。
micro提供基本的构建块，让任何人都更容易采用微服务。

micro 默认使用 gRPC

micro发展迅速，经历了多个版本，主讲新版本，前面的概念只是会快速带过下旧版本内容。

Micro，一个开源微服务工具包。**Micro 提供了构建和管理微服务的核心需求**。它由一组库和工具组成，主要面向编程语言**Go**的开发，但希望使用Sidecar通过 HTTP 解决其他语言。

Go Micro是一个可插拔的 RPC 框架，用于在 Go 中构建微服务。它提供了创建、发现和与服务通信所需的基本功能。任何好的微服务架构的核心都是从解决服务发现、同步和异步通信开始的。(新版本已经将go-micro和plugins等全整合到一起了)

Micro 使团队能够扩展微服务开发，同时抽象出分布式系统和云原生基础架构的复杂性。它提供了一个可插拔且与运行时无关的架构，具有合理的零依赖默认值。

Go Micro 确实处于微服务开发的核心，但随着服务的编写，接下来的问题转移到了；我如何查询这些，我如何与他们互动，我如何通过传统方式为他们服务，鉴于 go-micro 使用了一个基于 rpc/protobuf 的协议，该协议既可插拔又与运行时无关，需要某种方式来解决这个问题，这种方式对 go-micro 本身来说是正确的。这导致了微服务工具包**micro**的创建。Micro 提供 api 网关、Web 仪表板、cli、slack bot、服务代理等等。

go-micro将是微服务开发的包罗万象的独立框架。官方已经通过将所有库迁移到 go-micro 来开始整合过程，提供更简单的默认入门体验，同时还添加更多的日志记录、跟踪、指标、身份验证等功能

**Micro将成为微服务开发的运行时，是构建微服务的最简单方法，并逐渐成为基于 Go 的云端微服务开发的事实标准。**


微型工具包通过 http api、浏览器、松弛命令和命令行界面充当交互点。这些是我们查询和构建应用程序的常用方式，对我们来说，提供真正实现这一点的运行时非常重要。然而，它仍然真正专注于沟通高于一切。
# 理清微服务的关键字
微服务和它常见的关键概念挺多了，很多人还是区分不出来什么是什么，这里就全面讲下这些内容，注意有部署的概念在新版本中已经被融合了

**Micro**是一个微服务工具包。它的构建是为了在其功能和界面上固执己见，同时提供强大的可插拔架构，允许交换底层依赖项。
**Micro** 专注于解决构建微服务的基本要求，并希望通过对其设计采取深思熟虑和衡量的方法来做到这一点。

**Go Micro**是一个可插拔的 RPC 框架，用于在 Go 中编写微服务。它为服务发现、客户端负载平衡、编码、同步和异步通信提供库。
**Micro API**是一个 API 网关，提供 HTTP 服务并将请求路由到适当的微服务。它充当单个入口点，既可以用作反向代理，也可以将 HTTP 请求转换为 RPC。
**Micro Web**是微型 Web 应用程序的 Web 仪表板和反向代理。我们认为 Web 应用程序应该构建为微服务，因此应被视为微服务世界中的一等公民。它的行为很像 API 反向代理，但也包括对 Web 套接字的支持。
**Micro Sidecar**将 go-micro 的所有功能作为 HTTP 服务提供。虽然我们喜欢 Go 并相信它是构建微服务的好语言，但您可能还想使用其他语言，因此 Sidecar 提供了一种将您的其他应用程序集成到 Micro 世界的方法。
**Micro CLI**是一个直接的命令行界面，用于与您的微服务进行交互。它还允许您利用 Sidecar 作为代理，您可能不想直接连接到服务注册表。

**RPC、REST、原型**
为什么是 RPC，为什么不是 REST？ RPC 是更适合服务间通信的选择。或者更具体地说，使用 protobuf 编码和使用 protobuf IDL 定义的 API 的 RPC。这种组合允许创建强定义的 API 接口和在线上有效的消息编码格式。RPC 是一种简单明了的通信协议。
Google 是 protobuf 的创建者，内部使用 RPC，最近开源了 gRPC，一个 RPC 框架。Hailo 也是 RPC/Protobuf 的坚定倡导者，并从中受益匪浅，有趣的是，在跨团队开发方面比在系统性能方面受益更多。优步选择了自己的道路，并继续为 RPC 开发一个称为TChannel的框架协议。
个人认为，未来的 API 将使用 RPC 构建，因为它们定义明确的结构化格式、使用高效编码协议（如 protobuf）的倾向以及提供强定义 API 和高性能通信的组合。

**HTTP 到 RPC、API**
但实际上，我们距离网络上的 RPC 还有很长的路要走。虽然它在数据中心内部非常完美，但为面向公众的流量提供服务，例如网站和移动 API，则完全是另一回事。面对现实，还需要一段时间才能摆脱 HTTP。这就是为什么 micro 包含一个 API 网关来服务和翻译 HTTP 请求的原因之一。
API 网关是一种用于微服务架构的模式。它充当外部世界的单一入口点，并根据请求路由到适当的服务。这允许 HTTP API 本身由不同的微服务组成。
这是一个强大的架构模式。对 API 的某个部分进行一次更改就可能导致整个单体应用崩溃的日子已经一去不复返了。

# 微服务必然涉及的服务发现
很多人说到服务发现，往往会直接向到consul集群，但实际上它再应用领域不太跟得上云的时代，且不说kubernetes上很多普通的资源比如service自带了服务发现的属性，更重要的是我们以后会使用服务网格比如istio这些更高端的产品。
kubernetes 已经真正成为容器编排和**服务基础平台**领域的一股算力。随着 etcd 成为他们选择的键值存储，一个使用 raft 共识构建的分布式键值存储。它已经发展到满足 kubernetes 的规模要求，并且已经以其他开源项目很少有的方式进行了实战测试。
Etcd 也是一个非常标准的二进制数据的 Get/Put/Delete 存储，这意味着我们可以轻松地编码和存储我们的服务元数据而不会出现问题。它对所存储数据的格式没有意见。
官方在之前的版本升级中就弃用了consul，使用etcd。

# 使用go和micro构建全球服务网络
微网络是基于go-micro的全球分布式网络，go-micro是一个 Go 微服务框架，使开发人员能够快速构建服务，而无需处理分布式系统的复杂性。Go Micro 提供了强烈固执的接口，这些接口是可插拔的，但也带有健全的默认值。这允许 Go Micro 服务构建一次并在任何地方部署，零代码更改。
微网络利用了五个核心原语：**注册、传输、代理、客户端和服务器**(类似grpc的调用，只是规模更大)。我们的默认实现可以在go-micro框架的每个包中找到。社区维护的插件位于go-plugins存储库中。(新版本将go-micro和go-plugins融合)

微“网络”是一个超载的术语，既指服务发现并相互通信的全球网络，又指由对等节点组成的支撑系统，这些对等节点连接到每个节点并建立服务通信的路由。该网络在任何云或机器上抽象出大规模分布式系统通信的低级细节，并允许任何人一起构建服务，而无需考虑它们在哪里运行。这基本上实现了资源的大规模共享，更重要的是微服务。

有四个基本概念使微网络成为可能。
> 隧道- 点对点隧道 
> 代理- 透明的 rpc 代理 
> 路由器- 路由聚合和广告 
> 网络——建立在以上三者之上的多云网络

这些组件中的每一个都与任何其他Go Micro组件一样 - 可插拔，具有开箱即用的默认实现来开始。在我们的案例中，微网络重要的是**默认设置**在全球范围内大规模运行。
(其实更多时候我们是将服务运行在自己规定的局域网中，很少用到全球网那么大)
## 隧道
从高层次来看，微网络是一个跨越互联网的覆盖网络。所有微网络节点之间保持安全隧道连接，以实现网络中运行的服务之间的安全通信。Go Micro 使用 QUIC 协议和自定义会话管理提供默认隧道实现。
选择 QUIC 是因为它提供了一些出色的特性，尤其是在处理高延迟网络时，这是处理大型分布式网络中运行服务时的重要特性。QUIC 在 UDP 上运行，但通过添加一些基于连接的语义，它支持可靠的数据包传递。QUIC 还支持多个流而没有线头阻塞，并且它被设计为与本机加密一起使用。最后，QUIC 运行在用户空间，而不是传统系统的内核空间，因此它也可以提供性能和额外的安全性。
微隧道使用quic-go，这是在微网络开始时可以找到的最完整的 QUIC 的 Go 实现。 quic-go 是一项正在进行的工作，它偶尔会中断，但还是很乐意支付早期采用者的成本，因为我们相信 QUIC 将在未来成为事实上的**标准互联网通信协议**，支持大规模网络，例如**微网**。
隧道接口源码：

```bash
// Tunnel creates a gre tunnel on top of the go-micro/transport.
// It establishes multiple streams using the Micro-Tunnel-Channel header
// and Micro-Tunnel-Session header. The tunnel id is a hash of
// the address being requested.
type Tunnel interface {
	// Address the tunnel is listening on
	Address() string
	// Connect connects the tunnel
	Connect() error
	// Close closes the tunnel
	Close() error
	// Links returns all the links the tunnel is connected to
	Links() []Link
	// Dial to a tunnel channel
	Dial(channel string, opts ...DialOption) (Session, error)
	// Accept connections on a channel
	Listen(channel string, opts ...ListenOption) (Listener, error)
}
```

在 Go Micro 中，维护与分布式系统开发一致的通用接口，同时介入较低层以解决一些细节问题。

通道很像地址，提供了一种在隧道上分割不同消息流的方法。侦听器在给定的频道上收听，并在客户端拨入该频道时返回唯一的会话。该会话用于在同一隧道通道上的对等方之间进行通信。Go Micro 隧道也提供了不同的通信语义。您可以选择使用单播或多播。

## 路由器
微路由器是微网络的重要组成部分。它提供网络的路由平面。如果没有路由器，将不知道在哪里发送消息。它基于本地**服务注册中心**（Go Micro 的一个组件）构建路由表。路由表维护本地网络上可用服务的路由。通过隧道，它还能够处理来自任何其他数据中心或网络的消息，默认情况下启用全局路由。
默认路由表实现使用简单的 Go in memory 映射，但与 Go Micro 中的所有东西一样，路由器和路由表都是可插拔的。

源码：

```bash
// Router is an interface for a routing control plane
type Router interface {
	// The routing table
	Table() Table
	// Advertise advertises routes to the network
	Advertise() (<-chan *Advert, error)
	// Process processes incoming adverts
	Process(*Advert) error
	// Solicit advertises the whole routing table to the network
	Solicit() error
	// Lookup queries routes in the routing table
	Lookup(...QueryOption) ([]Route, error)
	// Watch returns a watcher which tracks updates to the routing table
	Watch(opts ...WatchOption) (Watcher, error)
}
```

当路由器启动时，它会自动为其本地注​​册表创建一个观察者。每当创建、更新或删除服务时，微注册表都会发出事件。路由器处理这些事件，然后相应地将操作应用于其路由表。路由器本身通告路由表事件，您可以将其视为注册表的缩减版本，仅与请求路由有关，因为注册表提供更多功能丰富的信息，例如 api 端点。
这些路由作为事件传播到本地和全球网络上的其他路由器，并由每个路由器应用到它们自己的路由表。从而维护全球网络路由平面。


```bash
// Route is a network route
type Route struct {
	// Service is destination service name
	Service string
	// Address is service node address
	Address string
	// Gateway is route gateway
	Gateway string
	// Network is the network name
	Network string
	// Router is router id
	Router string
	// Link is networks link
	Link string
	// Metric is the route cost
	Metric int64
}
```

在这里主要关心的是首先按服务名称进行路由，如果必须通过某个远程端点或不同的网络，则查找其本地地址或网关。想知道要使用哪种类型的链接，例如是通过隧道、Cloudflare Argo 隧道还是其他一些网络实现进行路由。然后最重要的是指标，即路由到该节点的成本。有事会有很多路由，希望采用成本最优的路由以确保最低延迟。这并不总是意味着您的请求被发送到本地网络！想象一下在本地网络上运行的服务过载的情况。无论服务在哪里运行，我们都将始终选择成本最低的路线。
## 代理人
我们已经讨论了隧道——消息如何从点到点传递，以及路由——详细说明了如何找到服务所在的位置，但问题是服务实际上如何利用它？为此，我们真的需要一个代理。

在构建微网络时，我们构建的东西对我们来说很重要，它是微原生的并且能够理解我们的路由协议。构建另一个基于 VPN 或 IP 的网络解决方案不是我们的目标。相反，我们希望促进服务之间的通信。

当服务需要与网络中的其他服务通信时，它使用微代理。

代理是基于 Go MicroClient和Server接口构建的原生 RPC 代理实现。它封装了我们服务的核心通信方式，并提供了基于服务名称和端点的请求转发机制。此外，它还能够充当异步通信的消息交换，因为 Go Micro 支持请求/响应和发布/订阅通信。这是 Go Micro 原生的，是请求路由的强大构建块。

接口本身很简单，封装了代理的复杂性。

```bash
// Proxy can be used as a proxy server for go-micro services
type Proxy interface {
	// ProcessMessage handles inbound messages
	ProcessMessage(context.Context, server.Message) error
	// ServeRequest handles inbound requests
	ServeRequest(context.Context, server.Request, server.Response) error
}
```

代理接收 RPC 请求并将它们路由到端点。它向路由器询问服务的位置（根据需要进行缓存），并根据Link路由表中的字段决定是在本地发送请求还是通过全球网络的隧道发送请求。该Link字段的值是“local"（对于本地服务）或者“network"该服务只能通过网络访问。

与其他一切一样，代理是我们独立构建的东西，它可以在一个数据中心的服务之间工作，但在与隧道和路由器一起使用时也可以跨多个服务工作。

终于到达了阻力位。网络接口。

## 网络
网络节点是将所有核心组件联系在一起的魔法。使能够建立真正的全球服务网络。在创建网络接口时，使其符合我们现有的假设和对 Go Micro 和分布式系统开发的理解非常重要。我们真的很想拥抱框架的现有接口，并设计一些关于服务对称的东西。

我们得到的是与micro.Service接口本身非常相似的东西

```bash
// Network is a micro network
type Network interface {
	// Node is network node
	Node
	// Name of the network
	Name() string
	// Connect starts the resolver and tunnel server
	Connect() error
	// Close stops the tunnel and resolving
	Close() error
	// Client is micro client
	Client() client.Client
	// Server is micro server
	Server() server.Server
}

// Node is a network node
type Node interface {
	// Id is node id
	Id() string
	// Address is node bind address
	Address() string
	// Peers returns node peers
	Peers() []Node
	// Network is the network node is in
	Network() Network
}
```

aNetwork具有名称、客户端和服务器，与 a 非常相似Service，因此它提供了类似的通信方法。这意味着我们可以重用大量现有的代码库，但它也走得更远。A直接在接口中Network包含 a 的概念Node，它具有对等点并且可能属于同一网络或其他网络。这意味着网络是点对点的，而服务主要集中在客户端/服务器上。开发人员每天都专注于构建服务，但这些服务在构建为全球通信时需要跨由相同对等点组成的网络运行。

我们的网络有能力充当为其他人路由的对等点，但也可以自己提供某种服务。在这种情况下，它主要是路由相关信息。
网络有一个要与之交谈的对等节点列表。在默认实现的情况下，对等列表来自具有相同名称（网络本身的名称）的其他网络节点的注册表。当一个节点启动时，它通过建立它的隧道、解析节点然后连接到它们来“连接”到网络。一旦他们通过两个多播会话连接了节点，一个用于对等公告，另一个用于路由公告。随着这些传播，网络开始汇聚相同的路由信息​​，构建一个完整的网格，允许将服务从任何节点路由到另一个节点。
节点维护keepalive，定期通告完整的路由表并在事件发生时刷新它们。我们的核心网络节点使用多个解析器来相互查找，包括 DNS 和本地注册表。对于加入我们网络的对等方，我们已将它们配置为使用 http 解析器，该解析器通过 Cloudflare 任播 DNS 进行地理引导，并将全局负载平衡到最本地的区域。他们从那里提取节点列表并连接到具有最低指标的节点。然后他们重复上述相同的歌舞，以继续网络的增长并参与服务路由。
每个节点根据其接收到的对等消息维护自己的网络图。对等消息包含最多 3 跳的每个对等点的图表，这使每个节点都能够构建网络的本地视图。对等点忽略任何超过 3 跳半径的东西。这是为了避免潜在的性能问题。
我们提到了一些关于对等点和路由通告的内容。那么网络节点实际交换的是什么消息呢？首先，网络嵌入路由器接口，通过该接口将其本地路由通告给其他网络节点。然后这些路由在整个网络中传播，就像互联网一样。节点本身从其对等方接收路由通告，并将通告的更改应用于其路由自己的路由表。消息类型是“请求”请求路由和“广告”广播更新。
网络节点在启动时发送“连接”消息，在退出时发送“关闭”消息。在他们的一生中，他们会定期广播“对等”消息，以便其他人可以发现它们，并且他们都可以构建网络拓扑。
当网络创建并融合后，服务就能够通过它发送消息。当网络上的服务需要与网络上的其他服务通信时，它会向网络节点发送请求。微网络节点嵌入了微代理，因此能够通过网络或本地转发请求，如果它根据在路由表中查找路由后检索到的指标认为更合适的话。
这作为一个整体形成了我们的微服务网络。


## 用法

```bash
# enable go modules
export GO111MODULE=on

# download micro
go get github.com/micro/micro@master

# connect to the network
micro --peer

已连接到网络。开始探索那里有什么。

# List the services in the network
micro network services

# See which nodes you're connected to
micro network connections

# List all the nodes in your network graph
micro network nodes

# See what the metrics look like to different service routes
micro network routes
```
开发人员使用Go Micro框架编写他们的 Go 代码，一旦他们准备好，他们就可以直接从他们的笔记本电脑或微网络节点运行的任何地方在网络上提供他们的服务(就类似于自己写的服务可以使用grpc等进行远程调用，只是这里范围更大，是个全球范围的微服务网络)
编写的简单服务的示例go-micro：

```bash
package main

import (
	"context"
	"log"
	"time"

	hello "github.com/micro/examples/greeter/srv/proto/hello"
	"github.com/micro/go-micro"
)

type Say struct{}

func (s *Say) Hello(ctx context.Context, req *hello.Request, rsp *hello.Response) error {
	log.Print("Received Say.Hello request")
	rsp.Msg = "Hello " + req.Name
	return nil
}

func main() {
	service := micro.NewService(
		micro.Name("helloworld"),
	)

	// optionally setup command line usage
	service.Init()

	// Register Handlers
	hello.RegisterSayHandler(service.Server(), new(Say))

	// Run server
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
```

启动服务后，它会自动向服务注册中心注册，并且网络上的每个人都可以立即访问以进行消费和协作。所有这些对开发人员来说都是完全透明的。无需处理低级分布式系统垃圾！


```bash
# enable proxying through the network
export MICRO_PROXY=go.micro.network

# call a service
micro call go.micro.srv.greeter Say.Hello '{"name": "John"}'
```
或者你也可以将服务的代码clone下来，运行在你的局域网，然后类似grpc去调用即可，只不过这底层的调用被micro抽象

# M3O
又名micro3
微服务开发平台，云原生开发平台，是一个 API 驱动开发的平台。
Micro 已经从一个 Go 框架转变为一个独立的服务器和托管平台
一种在云端及其他地方构建分布式系统的简单得多的方法，无需管理基础设施。M3O（源自单词M[icr]o）是多年分布式系统开发经验的结晶。
所有专注于软件开发的尝试都被云计算的新时代所阻挠

Micro 最初是一个用于微服务开发的 Go 框架。以开发人员为中心解决分布式系统开发问题。它采取了以尽可能最小的方式解决这个问题的方法。但现在，我们发现自己在不断发展并继续成为运行时和平台。

M3O 是一个微服务开发平台。一个完全托管的云平台，开发人员无需接触基础设施，让他们重新专注于产品和服务开发。每家成功的科技公司最保守的秘密现在正向全世界开放，作为一个定价简单的托管平台。

目标是专注于以下方面：
1.开发人员生产力 - 使开发人员能够专注于构建服务
2.协作和重用 - 该平台是为团队和他们之间的服务共享而构建的
3.一切即服务——平台上的所有应用程序都构建为微服务
4.发展速度——我们正在建立一个环境，让您能够以极快的速度前进


Micro 是作为微服务开发的工具包，结合了 api 网关、Web 仪表板和 cli，以与使用 Go RPC 框架构建的服务进行交互

M3O 提供 3 个内置环境；本地、开发和平台。

1.Local - Micro 是否在您的本地机器上运行
2.Dev - 是云中的免费开发环境
3平台- 是付费的安全、可扩展且受支持的生产环境

像这样与环境交互。

```bash
# view the environments
micro env

# set the environment
micro env set dev

# add a new environment
micro env add foobar proxy.foo.com:443
```

## local环境
本地环境就是这样，本地笔记本电脑。它是开发开始的地方，通常这需要您运行各种疯狂的基础设施。Micro 专注于提供可插入抽象作为 gRPC 服务，因此服务只需将 gRPC 直接与 Micro 对话，micro对用户隐藏细节。在本地，这意味着我们正在使用尽力而为的东西，例如 mdns、文件存储等。

从本地开始变得非常简单。只需运行一个命令。

```bash
micro server
这将启动您需要的所有服务，并让您构建一个在任何运行 Micro as a Service 的云环境中看起来都相同的服务。

#使用时将环境设置为本地服务器。

micro env set local

#curl localhost:8080 with your namespace
curl -H "Micro-Namespace: $NAMESPACE" "http://localhost:8080/helloworld?name=Alice"


#获取你的命名空间
micro user namespace

这在本地可能是空白的，你需要了解命名空间隔离的工作原理。
```

## dev开发环境
“开发”环境是提供 Micro 3.0 即服务的免费云托管环境。那里有一些很棒的开源工具，开发环境使每个人都能够使用与在云中进行本地开发相同的工具，在几分钟内启动并运行。

将 env 设置为“dev”，就可以像本地一样使用它。

如果使用的是开发环境，可以在m3o.dev上查找更多详细信息

## platform平台环境
“平台”环境是一个安全、可扩展且受支持的生产环境，可以在其中运行面向客户的服务和产品。这是一个付费层，其资源限制是开发者资源限制的 2 倍，开始包括 slack 和电子邮件支持以及 SLA。您可以将其视为您在任何工作场所所熟悉的生产平台。

我们在 Local、Dev 和 Platform 方面的目标是调用我们都知道并期望作为真正产品的工作流程。这些是完全独立的环境，它们的管理方式与 M3O 完全相同。

## 多租户和命名空间
随着像 kubernetes 这样的系统的出现以及对云的推动，可以看到确实需要转向共享资源使用。云并不便宜，我们也不强制运行单独的 kubernetes 集群(最好还是使用kubernetes)。micro使用与 kubernetes 相同的逻辑来构建**多租户**，称为命名空间。

micro在本地映射了相同的体验，因此可以为本地开发人员获得基本形式的命名空间，但大多数情况下，在生产中使用 kubernetes 命名空间以及用于身份验证、存储、配置、事件流的大量自定义编写隔离机制等，因此 Micro 3.0 可用于托管多个租户。

每个租户都有一个命名空间。该命名空间在每个子系统中都有自己独立的一组用户和资源。当您以用户或服务的身份发出任何请求时，将传递一个 **JWT 令牌**，以便底层系统可以路由到适当的资源。

注册开发环境后，将为您设置命名空间。你可以使用命令获取它(登录账号)

```bash
micro user namespace
```

当您使用任何类型的 CLI 命令时，你的**命名空间和身份验证令牌**会自动注入到请求中，包括刷新这些令牌。在 Micro 上运行的任何服务都会发生同样的情况。

此外，每个命名空间都有自己的自定义域，因此foobar命名空间变成foobar.m3o.dev ，比如 helloworld 服务，路由到foobar.m3o.dev/helloworld

## 运行源
Micro 是基于对现有工具的不满而构建的。作者想在一个命令中“运行源代码”。使用 Heroku，我们可以做到这一点，但它确实离我们太远了。早在 2010 年，Heroku 就专注于单片 Rails 开发。作者认为 Heroku 拿走了太多，而 AWS 回馈了太多。我们需要介于两者之间的东西。一个不需要我们依赖虚拟机或容器，但另一方面并不限制我们进行单体开发。

Micro 可以从本地目录或托管在 github、gitlab 或 bitbucket 上的 repo 获取源代码。在一个命令中，它将从相关位置上传或拉取，将其打包为容器并运行它，只需一个命令即可运行的源代码。不再需要处理管道，不再攻击容器和容器注册表。

## 发展模式
在新的 PaaS 热潮中，真正缺乏的一件事是开发模型。
Micro一直专注于分布式系统开发或微服务的实践。将大型单体应用程序分解成较小的独立服务的想法，这些服务可以很好地完成一件事。
服务的概念，它包含一个客户端和服务器，用于处理请求和对其他服务进行查询。**micro专注于围绕 API 定义的 protobuf 进行标准化，并使用 gRPC 进行网络分层。**

## 多语言客户端
micro开发的每个平台使用单一语言，例如 Web、移动等。云也不例外。micro支持 Go for Cloud，但认为需要一个生态系统来消费 Go 服务，并可能扩展到无法使用 python、java、ruby、rust 或 javascript 的地方。因为 Micro 的接口是 **gRPC**，所以我们通过代码生成 gRPC 客户端并允许任何语言利用 Micro 服务器。(底层就是protocol buffers使用grpc方式生成服务器的骨架和客户端的存根，服务器和客户端分别是用这两者)

使用 gRPC，跨语言变得简单，有一个内置的服务框架，您可以使用 Go 来非常优雅地编写代码，但是 gRPC 允许我们减少表面区域的范围并提供可以支持的强类型客户端不同的开发模型，一种可能有更大的空间推动微服务以一种框架无法实现的方式被广泛采用。

micro还包括 grpc-web 生成的客户端，使前端能够快速轻松地使用类型化的 javascript 客户端来利用与后端相同的开发。已经看到 grpc-web 在各个公司内部慢慢获得采用，并认为这也可能很快扩展到公共领域。

查看生成的客户端的micro/client/sdk目录。这些将在不久的将来发布给各自的包管理器。

## 构建 API First 服务
Micro 旨在使微服务开发更容易并提高后端开发人员的生产力，除了能够使用 gRPC 使用这些服务之外，我们认为世界仍然真正关心基于 HTTP/JSON 的 API，因此 Micro 包括一个 API 网关，它可以转换 http /json 自动发送到 grpc 请求。这意味着每个人都可以在云中构建 API 优先服务，而无需做任何事情。

这是一个简单的例子。

假设您使用以下原型在后端编写 helloworld

```bash
syntax = "proto3";

package helloworld;

service Helloworld {
	rpc Message(Request) returns (Response) {}
}

message Request {
	string name = 1;
}

message Response {
	string msg = 1;
}
```

然后将其公开为 M3O 平台上的“helloworld”服务。您将立即能够以 $namespace.m3o.dev/helloworld/message 的形式访问它

我们使用基于路径的解析将 http 请求映射到 gRPC。所以 /[service]/[method] 变成了 [Service.Method]。如果您的微服务名称由于某种原因与 proto 不匹配（您有多个 proto 服务），那么它的工作方式会略有不同，例如您的服务名称是 foobar 然后端点变为/foobar/helloworld/message.

```bash
# Install the micro binary
curl -fsSL https://install.m3o.com/micro | /bin/bash

# Set env to dev for the free environment in the cloud
micro env set dev

# Signup before getting started
micro signup

# Create a new service (follow the instructions and push to Github)
micro new helloworld

# Deploy the service from github
micro run github.com/micro/services/helloworld

# Check the service status
micro status

# Query the logs
micro logs helloworld

# Call the service
micro helloworld --name=Alice

# Get your namespace
NAMESPACE=$(micro user namespace)

# Call service via the public http API
curl "https://$NAMESPACE.m3o.dev/helloworld?name=Alice"
```

go-micro已经弃用了，只开发到go-micro2版本，不在作为官方维护了，只存在于作者的个人github账户下，现在使用micro3

# 简述必然涉及的云原生
云原生基本上是一个描述性术语，用于描述为在云中运行而构建的东西它可能听起来像一个流行词，实际上它只是意味着，**该软件是为在云中运行而构建的**。这与我们以前构建的方式有什么不同？云背后的理念是它的短暂性、可扩展性和一切都可以通过 API 访问。(云计算：云上，使用云计算提供的基础的计算存储等能力，云原生:云里，构建在云环境中运行软件应用)

我们对在云中运行的服务的期望是它们大多是无状态的，利用外部服务进行持久化，它们通过名称而不是 IP 地址来识别，它们本身提供了一个可供多个客户端使用的 API，例如 web、移动和 cli 或其他服务。

云原生应用程序是水平可扩展的，并且在域边界内运行，将它们划分为单独的应用程序，这些应用程序通过其 API 在网络上进行通信，而不是作为一个单一的实体。**云服务需要一种完全不同的软件创建方法**


