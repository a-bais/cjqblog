---
title: docker参数选项写法留意
date: 2021-08-30 16:51:10
tags: docker
categories: docker学习笔记
---

<!--more-->

选项参数等设置要按规定写，顺序是有要求的，一般这些不能写在镜像后。
例如--name nginx_second写在镜像后边，虽然不报错但是该容器名设置是不生效的
