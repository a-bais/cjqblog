---
title: 解决LNMP拆分出mysql服务器后连接不上mysql的问题
date: 2021-08-11 23:34:28
tags: 
categories: 解决错误问题 重点实操记录 Nginx学习笔记
---

<!--more-->

今天拆分出mysql遇到这个问题，问题原因就是原来的web服务器连接不上远程的数据库，所以整个LNMP架构出来的网站缺少数据运行不了。

首先，目标mysql服务器上装了mysql而且监听端口3306打开了，mysql服务器得监听得到web服务器的连接请求

```bashroot@localhost ~]# netstat -tunlp | grep 3306
tcp6       0      0 :::3306                 :::*                    LISTEN      68472/mysqld

```
如果这里是127.0.0.1:3303，它只监听自己，要修改:
![在这里插入图片描述](https://img-blog.csdnimg.cn/ddaf7b31455d493fbcba1da125028f71.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
对于端口3306web服务器可以是不开启的即mysql它服务可以关闭，端口时用来监听和区分服务请求，所以主要是mysql服务器开启3306端口

在mysql服务器上进入mysql，为web服务器创建个账号，这里需要web的用户名(root)和主机ip192.168.160.138:
grant all privileges on *.* 'root'@'192.169.160.138' identified by 'passwdweb' with grant option
注意这里的passwdweb是web服务器登录mysql服务器的数据库的密码，
在web服务器下这样使用:
mysql -uroot -h192.168.160.140 -p(这里的root和ip和密码都是mysql服务器的mysql服务的，并不是web的)
(另外测试发现这条命令在web关闭mysql服务是也可以用)

上面验证可以有web连接到mysql服务器的mysql服务了，接下来就是web的网站指定远程数据库了:
![在这里插入图片描述](https://img-blog.csdnimg.cn/cd0daa7fdeb641b6b2d8969f97f82734.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

附:
yum info mysql-community-server
rpm -ql mysql-community-server
systemctl status mysqld
mysql_secure_installtion也可以用来给自己mysql账号修改密码
