---
title: awk细节留意
date: 2021-08-04 14:45:42
tags: 
categories: 重点实操记录
---

<!--more-->

记：

> NR：awk已经读取的记录数目，一记录为一行，就是指读取第几行
> NF：读取的记录的域的个数，就是改行有几列
> 

```bash
print中NF和NR不需要$变量符，如果是$NF表示该行最后一列，$NR表示第NR列，就是把NR和NF变成数字先
$0表示输出该行
```

1.
awk 'BEGIN{FS=":";OFS="---"}{print $1,$2,$3}' /etc/passwd
使用print时，变量间要有,隔开，否则这里的输出用分隔符不起作用
FS，输入分隔符，用于对文件切割
OFS。输出分隔符，用于输出到屏幕时进行切割

2![在这里插入图片描述](https://img-blog.csdnimg.cn/4f6c4714c8164e8da0a7ca1a9503a61b.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/5756290f891744fdae4ff1b6ba3ee9a3.png)

3.
判断式，不是if判断，直接写就好，不加{}

```bash
[root@web ~]# awk -F ":" '{$3<3}{print $1 "\t" ">" $2}' passwd.txt 
root	>x
bin	>x
daemon	>x
adm	>x
lp	>x
sync	>x
shutdown	>x
halt	>x
mail	>x
operator	>x
[root@web ~]# awk -F ":" '$3<3{print $1 "\t" ">" $2}' passwd.txt 
root	>x
bin	>x
daemon	>x

```
4.
OFS情况下，正常输出就要求print中个部分用逗号隔开
![在这里插入图片描述](https://img-blog.csdnimg.cn/31258abef5244ebf8f5180ac194147b8.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
5.
以空格或者制表符为分隔符进行切割，要注意NF与NR实际结果与自己直观看文件认为的数目往往不同，比如一个较大的空格是多个空格或制表符加在一块的
![在这里插入图片描述](https://img-blog.csdnimg.cn/2c8c78ef19684ab88d8e753b0bb8ed49.png)
6.
默认RS="",ORS="\n",
RS会影响每次处理的行，可能不是像文件那样的行，因为行此时会被切割

7.
printf
![在这里插入图片描述](https://img-blog.csdnimg.cn/15f29422b52a4243a7e58f2b8b83e1a6.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
8.
![在这里插入图片描述](https://img-blog.csdnimg.cn/75a01e9846414096a18d314b17f745c6.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)


