---
title: docke命令中的container，及操作容器的常用命令
date: 2021-08-29 10:27:21
tags: docker
categories: docker学习笔记
---

<!--more-->

dcoker命令的container，操作对象是容器，container往往可以省略

常见的以容器为对象的命令总结:
run
ps
rm
start|stop|kill|restart
rename
cp
exec
logs
