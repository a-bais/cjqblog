---
title: ansible解决group模块删除组错误问题和user使用create_home=no后的返回信息细节留意
date: 2021-08-01 18:43:25
tags: 
categories: 解决错误问题 ansible学习笔记
---

<!--more-->



使用了create_home=no之后创建用户不会创建它的家目录，虽然去对应主机查询后的确没有这个目录，但是ansible返回的信息有，比如/home/aaa,即使你使用了create_home=no



user和group使用在删除用户时，基本上都是**用户名+uid+state，组名+gid+state**，但组的删除会有些特殊情况，不如有程序在使用这个组，用户删除一般不会有这些情况，
解决方案如下
关闭该进程
ansible web -i inventory -m shell -a "ps aux | awk '/^组名/{print $2}' | xargs kill -9"
