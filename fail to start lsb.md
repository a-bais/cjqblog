---
title: fail to start lsb
date: 2021-10-28 22:07:00
tags: network
categories: 解决错误问题
---

<!--more-->

配置文件检查过没有错误，systemcl restart netowrk就是报错，journalctl一看是fail to start lsb,
解决方法：

```bash
systemctl stop NetworkManager
systemctl disable NetworkManager
systemctl restart network
```

