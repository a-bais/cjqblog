---
title: kubernetes的DevOps业务(七)：Jenkins，GitLab，Harbor，Tekton，GitOps
date: 2022-06-21 09:24:49
tags: kubernetes devops gitops argocd
categories: 总复习:云原生运维开发知识点
---

<!--more-->

@[TOC]
# gitops的一些相关的概念
## GitOps 是什么？
你可能听说过 DevOps，或者 AIOps、ChatOps 等，那么 GitOps 又是什么呢？
GitOps 是 Weaveworks 提出的一种持续交付方式，它的核心思想是将应用系统的**声明性基础架构和应用程序**存放在 Git 版本库中。将 **Git** 作为交付流水线的核心，每个开发人员都可以提交拉取请求（Pull Request）并使用 Git 来加速和简化 Kubernetes 的应用程序部署和运维任务。通过使用像 Git 这样的简单工具，开发人员可以更高效地将注意力集中在创建新功能而不是运维相关任务上（例如，应用系统安装、配置、迁移等）。
## 主要优点
通过 GitOps，当使用 Git 提交基础架构代码更改时，自动化的交付流水线会将这些更改应用到应用程序的实际基础架构上。但是 GitOps 的想法远不止于此，它还会使用工具将整个应用程序的实际生产状态与基础架构源代码进行比较，然后它会告诉集群哪些基础架构源代码与实际环境不匹配。
通过应用 GitOps 最佳实践，应用系统的基础架构和应用程序代码都有“真实来源”——其实是将基础架构和应用程序代码都存放在 gitlab、或者 github 等版本控制系统上，这使开发团队可以提高开发和部署速度并提高应用系统可靠性。
将 GitOps 理论方法应用在持续交付流水线上，有诸多优势：

1.安全的云原生 CI/CD 流水线模型
2.更快的部署时间和恢复时间
3.稳定且可重现的回滚（例如，根据 Git 恢复/回滚/fork）
4.与监控和可视化工具相结合，对已经部署的应用进行全方位的监控

## 应用场景
作为 CI/CD 流水线的方案，由于没有单一工具可以完成流水线中所需的所有工作，因此可以自由地为流水线的不同部分选择最佳工具。可以从开源生态系统中选择一组工具，也可以从封闭源中选择一组工具，或者根据使用情况，甚至可以将它们组合在一起，其实，创建流水线最困难的部分是将所有部件粘合在一起。
不管如何选择构造自己的交付流水线，将基于 Git（或者其他版本控制工具）的 GitOps 最佳实践应用在交付流水线中都是一个不二选择，这将使构建持续交付流水线，以及后续的推广变得更加容易，这不仅从技术角度而且从文化角度来看都是如此。
当然，GitOps 也不是万能的，它也有相应的应用场景。
### 不可变基础设施

> 应用都需要运行在多台机器上，它们被组织成不同的环境，例如开发环境、测试环境和生产环境等等。需要将相同的应用部署到不同的机器上。通常需要系统管理员确保所有的机器都处于相同的状态。接着所有的修改、补丁、升级需要在所有的机器中进行。随着时间的推移，很难再确保所有的机器处于相同的状态，同时越来越容易出错。这就是传统的可变架构中经常出现的问题。这时我们有了不可变架构，它将整个机器环境打包成一个单一的不可变单元，而不是传统方式仅仅打包应用。这个单元包含了之前所说的整个环境栈和应用所有的修改、补丁和升级，这就解决了前面的问题。 —— 摘自InfoQ的《关于不可变架构以及为什么需要不可变架构》作者 百占辉


**不可变基础设施**这一概念不是刚刚才提出来的，它也不是必须需要容器技术。然而，通过容器，它变得更易于理解，更加实用，并引起了业内广泛注意。不可变基础设施让我们以全新的方式理解和面对应用系统，尤其是使以微服务为代表的分布式系统在部署、运维等方面变得不那么复杂，而有很好的可控性。

那么，如何比较方便地在实际的生产过程中应用不可变基础设施，这给业界也提出了另外一个问题，GitOps 是在具体 Kubernetes 的应用实践中出现的，GitOps 需要依托于不可变基础架构才能发挥其作用。在一定程度上说，不可变基础架构为 GitOps 的出现创造了必要的条件，反过来 GitOps 应用 Kubernetes 的容器编排能力，能够迅速的使用镜像搭建出应用系统所需的组件。
### 声明性容器编排
Kubernetes 作为一个云原生的工具，可以把它的声明性看作是代码，声明意味着配置由一组事实状态而不是一组指令组成，例如，“有十个redis服务器”，而不是“启动十个redis服务器，告诉我它是否有效”。

借助 Kubernetes 的声明性特点，应用系统的整个配置文件集可以在 Git 库中进行版本控制。通过使用 Git 库，应用程序更容易部署到 Kubernetes 中，以及进行版本回滚。更重要的是，当灾难发生时，集群的基础架构可以从 Git 库中可靠且快速地恢复。

Kubernetes 等云原生工具的声明性体现在可以对实例、容器、网络、存储、CPU 等配置通过一组代码方便的表达出来，Kubernetes 等云原生工具可以利用这些配置代码运行出来一套基于容器的应用系统，比如下面的 YMAL 资源清单文件

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
        app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
GitOps 充分利用了不可变基础设施和声明性容器编排，通过 GitOps 可以轻松地管理多个应用部署。为了最大限度地降低部署后的变更风险，无论是有意还是偶然的“配置差异”，GitOps 构建了一个可重复且可靠的部署过程，在整个应用系统宕机或者损坏情况下，为快速且完全恢复提供了所需条件。
## 基本原则
以下是几条在云原生环境中，GitOps 的一些基本原则：

任何能够被描述的内容都必须存储在 Git 库中：通过使用 Git 作为存储声明性基础架构和应用程序代码的存储仓库，可以方便地监控集群，以及检查比较实际环境的状态与代码库上的状态是否一致。所以，我们的目标是描述系统相关的所有内容：策略，代码，配置，甚至监控事件和版本控制等，并且将这些内容全部存储在版本库中，在通过版本库中的内容构建系统的基础架构或者应用程序的时候，如果没有成功，则可以迅速的回滚，并且重新来过。

不应直接使用 kubectl 命令：一般不提倡在命令行中直接使用 kubectl 命令操作执行部署基础架构或应用程序到集群中。还有一些开发者使用 CI 工具驱动应用程序的部署，但如果这样做，可能会给生产环境带来潜在不可预测的风险。

调用 Kubernetes 的 API 接口或者控制器应该遵循 Operator 模式：集群的状态和 Git 库中的配置文件等要保持一致，并且查看分析它们之间的状态差异。

Git 是 GitOps 形成的最基础的内容，就像第一条原则任何能够被描述的内容都必须存储在 Git 库中描述的那样：通过使用 Git 作为存储声明性基础架构和应用程序代码的存储仓库，可以方便地监控集群，以及检查比较实际环境的状态与代码库上的状态是否一致。所以，我们的目标是描述系统相关的所有内容：策略、代码、配置，甚至监控事件和版本控制等，并且将这些内容全部存储在版本库中，在通过版本库中的内容构建系统的基础架构或者应用程序的时候，如果没有成功，则可以迅速的回滚，并且重新来过。
## 将变更部署到集群
目前大多数 CI/CD 工具都使用基于 push 推送的模型，基于推送的流水线意味着代码从 CI 系统开始，通过一系列构建测试等最终生成镜像，最后手动使用 kubectl 将变更推送到 Kubernetes 集群。

如下图所示是一条典型的 push 模式的 CI/CD 流水线，CI 工具负责运行测试、构建镜像、检查 CVE 并将新镜像重新部署至集群当中。
![在这里插入图片描述](https://img-blog.csdnimg.cn/677b9e04a7e3414e94a4ab9b5c77ea83.png)
GitOps 方法的区别在于主要使用的是 pull 流水线模式，Git 仓库是 pull 模式的核心，它存储应用程序和配置文件。开发人员将更新的代码推送到 Git 代码库，CI 工具获取更改并最终构建 成 Docker 镜像。GitOps 检测到有镜像，从存储库中提取新镜像，然后在 Git 配置仓库中更新其 YAML。然后，GitOps 检测到集群状态已过期，则从配置库中提取已更改的清单，并将新镜像部署到集群中。
![在这里插入图片描述](https://img-blog.csdnimg.cn/6d8c4954dd814b628636baea6a25906d.png)
## gitops流水线
![在这里插入图片描述](https://img-blog.csdnimg.cn/84ceccb5c81a4940988f9d996fd389eb.png)
上图中我们可以看到部署的相关操作都是围绕 Git 仓库工作的，在“拉式流水线”中提到过，开发人员将更新的代码推送到 Git 代码仓库，CI 工具获取变更并最终构建成 Docker 镜像，GitOps 的 Config Updater 检测到有镜像更新，从存储库中提取新镜像，然后在 Git 配置仓库中更新其 YAML。然后，GitOps 的 Deploy Operator 会检测到集群中应用已过期，并从配置库中提取已更改的清单，并将新镜像部署到集群中。

使用集群内部的 Deploy Operator，相关凭据不会暴露到集群外部。一旦将 Deploy Operator 安装到集群与 Git 仓库建立连接，线上环境中的任何更改都将通过具有完全回滚的 Git pull 请求以及 Git 提供的方便审计日志完成。
### 状态同步
由于没有单一工具可以完成流水线中所需的所有工作，可以从开源生态系统中选择一组工具，也可以从封闭源中选择一组工具，或者根据使用情况，甚至可以将它们组合在一起，其实，创建流水线最困难的部分是将所有部件粘合在一起。要实现 GitOps，必须要开发出新的组件，用于粘合这些工具，实现拉式交付流水线。

部署和发布自动化是应用落实 GitOps，实施交付流水线工作的基础。当开发人员通过 Git 更新配置文件的时候，GitOps 流水线要自动根据最新的配置文件状态更新线上环境，而且 GitOps 还要能够实时比对 Git 仓库中配置文件最新的状态与线上环境最新的状态保持一致。
![在这里插入图片描述](https://img-blog.csdnimg.cn/e4d9d8dc32274333b75cd9fa851ed406.png)
上面提到了两个名词：Config Updater 和 Deploy Operator，根据 GitOps 的实践，Config Updater 和 Deploy Operator 是需要进行设计开发的，它们是实现 GitOps 流水线的关键组件。GitOps 赋予了它们神奇的魔法，它们既是自动化容器升级和发布到线上环境的工具，可能也要负责服务、部署、网络策略甚至路由规则等任务。因此，Config Updater 和 Deploy Operator 是映射代码、服务和运行集群之间所有关系的“粘合剂”。

当然，你可以根据具体的设计，赋予各种其他的功能，但是自动同步是一定需要的，确保如果对存储库进行任何变更，这些更改将自动部署到线上环境中。
### 只部署容器和配置
GitOps 建议不直接将应用程序部署到线上环境中，而是将应用程序和相关配置打包成镜像，并存储到镜像库中，最后，通过镜像的方式生成容器，并部署到线上环境中。

容器为什么如此重要？在 GitOps 模型中，我们使用不可变基础架构模式。一旦代码在 Git 中提交，GitOps 就不希望任何其他内容发生变化，这样可以最大限度地降低系统潜在不确定性、不一致性风险。例如，需要将相同的应用部署到不同的机器上，通常需要系统管理员确保所有的机器都处于相同的状态。接着所有的修改、补丁、升级需要在所有的机器中进行。随着时间的推移，很难再确保所有的机器处于相同的状态，同时越来越容易出错。然而，容器是比较完美地解决了这个问题，当然，使用虚拟机是可以的，显然使用容器更加方便。

## GitOps 的可观察性
在 GitOps 中，使用 Git 库来存储应用系统的配置文集和应用程序，它确保开发人员将所有对于应用系统的配置和程序的新增、修改等都通过 Git 库进行版本控制，使 Git 成为配置和程序的唯一真实来源。而 GitOps 的可观察性则是确保线上环境的真实状态与 Git 库中的保持一致性。
### 状态监测
在 GitOps 中，我们使用 Git 作为系统所需状态的真实来源。例如，如果应用系统宕机，GitOps 可以回滚到之前的正确状态，而可观察性是系统实际运行状态的真实来源，系统开发人员或运维人员可以监控系统的状态。这是一张显示流程的图片。
![在这里插入图片描述](https://img-blog.csdnimg.cn/416fb56bada8464389cfea8d09979cf3.png)
可观察性可被视为 Kubernetes 持续交付周期的主要驱动因素之一，因为它描述了在任何给定时间系统的实际运行状态。观察运行系统以便理解和控制它。新功能和修复程序被推送到 Git 并触发部署流水线，当准备好发布时，可以实时查看正在运行的集群。此时，开发人员可以根据反馈将镜像部署并发布到生产集群。

在这里 GitOps 引入一个新的工具：Diffs，用来监控对比系统状态。即：

1.验证当前线上系统的状态是否和 Git 库中描述的状态一致，例如，我上一次发布是否符合期望？
2.提醒开发人员不一致状态，以及相应的明细信息。
前面我们反复提到过在 Git 库中存储的实际上是“声明性基础设施”，例如 Kubernetes 的 YAML 资源清单文件，用以构建应用系统所需的各种组件、域名、网络等配置信息。Diffs 需要读取 Git 库中配置信息，同时，通过 API 等读取集群的相应信息，并进行比对。

例如，我们需要的 Kubernetes 状态可能是“有4个redis服务”，Diffs 定期检查集群，并在数量从4变化时发出警报。一般而言，Diffs 将 YAML 文件转换为运行状态查询。

GitOps 是面向发布的操作模型，交付速度取决于团队在此周期中绕过各个阶段的速度
![在这里插入图片描述](https://img-blog.csdnimg.cn/68fbd1d452754639a89e72c8c5274964.png)
### 合规性和安全性
开发人员或者运维人员通过 Git 操作系统配置和应用程序的新建和更新等。通过 Git 客户端 git commit/git merge 的所有操作都会在 Git 库记录下来，审计员可以查看 Git，看看谁做了任何更改，何时以及为何以及如何影响正在运行的系统应用。当然，可以根据自身的需求定制不同的交付合规性。相较于直接进入服务器操作或者通过 Kubectl 操作集群，Git 记录了每一个操作步骤，这些可以为合规性和审计提供完整的操作日志。

### 角色和权限控制
几乎所有的 Git 库都提供角色和权限控制，与开发和运维无关的人员没有权限操作 Git 库。而不是直接把服务器或者集群的操作权限暴露出去，这样特别容易引起安全泄露。
## 工具
上面我们提到在实现 GitOps 中有两个重要的概念：Config Updater 和 Deploy Operator，这在 Kubernetes 中通常也是使用 Operator 来实现的，目前比较热门的用于实现 GitOps 的工具有 Flux、ArgoCD、Jenkins X
![在这里插入图片描述](https://img-blog.csdnimg.cn/03cc458bdfd4474d9daa30cd2a5b5262.png)
# Argo CD
Argo CD 是一个为 Kubernetes 而生的，遵循声明式 GitOps 理念的持续部署工具。Argo CD 可在 Git 存储库更改时自动同步和部署应用程序。

Argo CD 遵循 GitOps 模式，使用 Git 仓库作为定义所需应用程序状态的真实来源，Argo CD 支持多种 Kubernetes 清单：

1.kustomize
2.helm charts
3.ksonnet applications
4.jsonnet files
6.Plain directory of YAML/json manifests
7.Any custom config management tool configured as a config management plugin
Argo CD 可在指定的目标环境中自动部署所需的应用程序状态，应用程序部署可以在 Git 提交时跟踪对分支、标签的更新，或固定到清单的指定版本。
![在这里插入图片描述](https://img-blog.csdnimg.cn/3a97df44a156429db9bd2c41bfd11d22.png)
Argo CD 是通过一个 Kubernetes 控制器来实现的，它持续 watch 正在运行的应用程序并将当前的实时状态与所需的目标状态（ Git 存储库中指定的）进行比较。已经部署的应用程序的实际状态与目标状态有差异，则被认为是 OutOfSync 状态，Argo CD 会报告显示这些差异，同时提供工具来自动或手动将状态同步到期望的目标状态。在 Git 仓库中对期望目标状态所做的任何修改都可以自动应用反馈到指定的目标环境中去。

下面简单介绍下 Argo CD 中的几个主要组件：

**API 服务**：API 服务是一个 gRPC/REST 服务，它暴露了 Web UI、CLI 和 CI/CD 系统使用的接口，主要有以下几个功能：

1.应用程序管理和状态报告
2.执行应用程序操作（例如同步、回滚、用户定义的操作）
3.存储仓库和集群凭据管理（存储为 K8S Secrets 对象）
4.认证和授权给外部身份提供者
5.RBAC
6.Git webhook 事件的侦听器/转发器

**仓库服务**：存储仓库服务是一个内部服务，负责维护保存应用程序清单 Git 仓库的本地缓存。当提供以下输入时，它负责生成并返回 Kubernetes 清单：

1.存储 URL
2.revision 版本（commit、tag、branch）
3.应用路径
4.模板配置：参数、ksonnet 环境、helm values.yaml 等

**应用控制器**：应用控制器是一个 Kubernetes 控制器，它持续 watch 正在运行的应用程序并将当前的实时状态与所期望的目标状态（ repo 中指定的）进行比较。它检测应用程序的 OutOfSync 状态，并采取一些措施来同步状态，它负责调用任何用户定义的生命周期事件的钩子（PreSync、Sync、PostSync）。

## 功能
自动部署应用程序到指定的目标环境
支持多种配置管理/模板工具（Kustomize、Helm、Ksonnet、Jsonnet、plain-YAML）
能够管理和部署到多个集群
SSO 集成（OIDC、OAuth2、LDAP、SAML 2.0、GitHub、GitLab、Microsoft、LinkedIn）
用于授权的多租户和 RBAC 策略
回滚/随时回滚到 Git 存储库中提交的任何应用配置
应用资源的健康状况分析
自动配置检测和可视化
自动或手动将应用程序同步到所需状态
提供应用程序活动实时视图的 Web UI
用于自动化和 CI 集成的 CLI
Webhook 集成（GitHub、BitBucket、GitLab）
用于自动化的 AccessTokens
PreSync、Sync、PostSync Hooks，以支持复杂的应用程序部署（例如蓝/绿和金丝雀发布）
应用程序事件和 API 调用的审计
Prometheus 监控指标
用于覆盖 Git 中的 ksonnet/helm 参数

## 核心概念
Application：应用，一组由资源清单定义的 Kubernetes 资源，这是一个 CRD 资源对象
Application source type：用来构建应用的工具
Target state：目标状态，指应用程序所需的期望状态，由 Git 存储库中的文件表示
Live state：实时状态，指应用程序实时的状态，比如部署了哪些 Pods 等真实状态
Sync status：同步状态表示实时状态是否与目标状态一致，部署的应用是否与 Git 所描述的一样？
Sync：同步指将应用程序迁移到其目标状态的过程，比如通过对 Kubernetes 集群应用变更
Sync operation status：同步操作状态指的是同步是否成功
Refresh：刷新是指将 Git 中的最新代码与实时状态进行比较，弄清楚有什么不同
Health：应用程序的健康状况，它是否正常运行？能否为请求提供服务？
Tool：工具指从文件目录创建清单的工具，例如 Kustomize 或 Ksonnet 等
Configuration management tool：配置管理工具
Configuration management plugin：配置管理插件

## 安装
当然前提是需要有一个 kubectl 可访问的 Kubernetes 的集群，直接使用下面的命令即可，这里我们安装最新的稳定版

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.0.4/manifests/install.yaml
如果你要用在生产环境，则可以使用下面的命令部署一个 HA 高可用的版本：
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.0.4/manifests/ha/install.yaml
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/d1407ecf6f4f42ffaa18d6143ecc27c5.png)
这将创建一个新的命名空间 argocd，Argo CD 的服务和应用资源都将部署到该命名空间。

```bash
[root@master1 devops-argocd]# kubectl get pod -n argocd
NAME                                  READY   STATUS    RESTARTS   AGE
argocd-application-controller-0       1/1     Running   0          163m
argocd-dex-server-76ff776f97-fp7j6    1/1     Running   0          163m
argocd-redis-747b678f89-ldj2q         1/1     Running   0          163m
argocd-repo-server-6fc4456c89-fbzgl   1/1     Running   0          163m
argocd-server-5cc96b75b4-pxkhv        1/1     Running   0          82m
```

> 如果你对 UI、SSO、多集群管理这些特性不感兴趣，只想把应用变更同步到集群中，那么你可以使用 --disable-auth 标志来禁用认证，可以通过命令 kubectl patch deploy argocd-server -n argocd -p '[{"op": "add", "path": "/spec/template/spec/containers/0/command/-", "value": "--disable-auth"}]' --type json 来实现。

然后我们可以在本地安装 CLI 工具方便操作 Argo CD，我们可以在[https://github.com/argoproj/argo-cd/releases/tag/v2.3.4](https://github.com/argoproj/argo-cd/releases/tag/v2.3.4)仓库发布页面查看最新版本的 Argo CD 或运行以下命令来获取版本
![在这里插入图片描述](https://img-blog.csdnimg.cn/a5cdffb7fe4f4bf0be786591a023d112.png)解压下来，最好给文件改个名比如argocd，记得给文件添加执行权限，然后移动到/usr/local/bin下(path的内容是目录，寻找命令的时候不会到path对应的子目录下去找)

```bash
[root@master1 devops-argocd]# argocd version
argocd: v2.3.4+ac8b7df
  BuildDate: 2022-05-18T13:06:25Z
  GitCommit: ac8b7df9467ffcc0920b826c62c4b603a7bfed24
  GitTreeState: clean
  GoVersion: go1.17.9
  Compiler: gc
  Platform: linux/amd64
argocd-server: v2.0.4+0842d44
  BuildDate: 2021-06-23T01:27:53Z
  GitCommit: 0842d448107eb1397b251e63ec4d4bc1b4efdd6e
  GitTreeState: clean
  GoVersion: go1.16
  Compiler: gc
  Platform: linux/amd64
  Ksonnet Version: v0.13.1
  Kustomize Version: v3.9.4 2021-02-09T19:22:10Z
  Helm Version: v3.5.1+g32c2223
  Kubectl Version: v0.20.4
  Jsonnet Version: v0.17.0
```
Argo CD 会运行一个 gRPC 服务（由 CLI 使用）和 HTTP/HTTPS 服务（由 UI 使用），这两种协议都由 argocd-server 服务在以下端口进行暴露：
1.443 - gRPC/HTTPS
2.80 - HTTP（重定向到 HTTPS）
我们可以通过配置 Ingress 的方式来对外暴露服务，这里我们仍然使用 Traefik 的 IngressRoute 进行配置

由于 Traefik 它可以在同一端口处理 TCP 和 HTTP 连接，所以我们不需要定义多个 IngressRoute 来暴露 HTTP 和 gRPC 服务，然后应在禁用 TLS 的情况下运行 API 服务，编辑 argocd-server Deployment 以将 --insecure 标志添加到 argocd-server 命令中

```bash
spec:
  template:
    spec:
      containers:
      - name: argocd-server
        command:
        - argocd-server
        - --staticassets
        - /shared/app
        - --repo-server
        - argocd-repo-server:8081
        - --insecure  # 需要禁用 tls，否则会 `redirected you too many times`
```
然后创建如下所的 IngressRoute 资源对象即可，我们创建了一个 redirect-https 的中间件，可以让 http 服务强制跳转到 https 服务去

```bash
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: redirect-https
  namespace: argocd
spec:
  redirectScheme:
    scheme: https
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: argocd-server-http
  namespace: argocd
spec:
  entryPoints:
    - web
  routes:
    - kind: Rule
      match: Host(`argocd.k8s.local`)
      priority: 10
      middlewares:
        - name: redirect-https
      services:
        - name: argocd-server
          port: 80
    - kind: Rule
      match: Host(`argocd.k8s.local`) && Headers(`Content-Type`, `application/grpc`)
      priority: 11
      middlewares:
        - name: redirect-https
      services:
        - name: argocd-server
          port: 80
          scheme: h2c
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: argocd-server
  namespace: argocd
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`argocd.k8s.local`)
      priority: 10
      services:
        - name: argocd-server
          port: 80
    - kind: Rule
      match: Host(`argocd.k8s.local`) && Headers(`Content-Type`, `application/grpc`)
      priority: 11
      services:
        - name: argocd-server
          port: 80
          scheme: h2c
  tls:
    certResolver: default
    options: {}
```
tle那里可能会报错，给它注释掉即可

创建完成后，我们就可以通过 argocd.k8s.local 来访问 Argo CD 服务了，不过需要注意我们这里配置的证书是自签名的，所以在第一次访问的时候会提示不安全，强制跳转即可
![在这里插入图片描述](https://img-blog.csdnimg.cn/3386c224c316416a87d8000756579d67.png)
默认情况下 admin 帐号的初始密码是自动生成的，会以明文的形式存储在 Argo CD 安装的命名空间中名为 password 的 Secret 对象下的 argocd-initial-admin-secret 字段下，我们可以用下面的命令来获取

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo
```
使用用户名 admin 和上面输出的密码即可登录 Dashboard，同样我们也可以通过 ArgoCD CLI 命令行工具进行登录

```bash
[root@master1 devops-argocd]# argocd login argocd.k8s.local
WARNING: server certificate had error: x509: certificate is valid for f5a45b095717e83a82f4c613b1002d75.2dc1a8dee58271e21d2e07f279f63878.traefik.default, not argocd.k8s.local. Proceed insecurely (y/n)? y
Username: admin
Password:
'admin:login' logged in successfully
Context 'argocd.k8s.local' updated

CLI 登录成功后，可以使用如下所示命令更改密码
[root@master1 devops-argocd]# argocd account update-password
*** Enter password of currently logged in user (admin):
*** Enter new password for user admin:
*** Confirm new password for user admin:
Password updated
Context 'argocd.k8s.local' updated
```
## 配置集群
由于 Argo CD 支持部署应用到多集群，所以如果你要将应用部署到外部集群的时候，需要先将外部集群的认证信息注册到 Argo CD 中，如果是在内部部署（运行 Argo CD 的同一个集群，默认不需要配置），应该使用 https://kubernetes.default.svc 作为应用的 K8S APIServer 地址。

首先列出当前 kubeconfig 中的所有集群上下文

```bash
[root@master1 devops-argocd]# kubectl config get-contexts -o name
kubernetes-admin@kubernetes
```
从列表中选择一个上下文名称并将其提供给 argocd cluster add CONTEXTNAME，比如对于 docker-desktop上下文，运行

```bash
argocd cluster add kubernetes-admin@kubernetes
```
上述命令会将 ServiceAccount (argocd-manager) 安装到该 kubectl 上下文的 kube-system 命名空间中，并将 ServiceAccount 绑定到管理员级别的 ClusterRole，Argo CD 使用此 ServiceAccount 令牌来执行任务管理（部署/监控）。
argocd-manager-role 可以修改 Role 的规则，使其仅对有限的一组命名空间、组、种类具有 create、update、patch、delete 等权限，但是对于 Argo CD 需要 get，list，watch 的权限在 ClusterRole 范围内。

## 创建应用
Git 仓库 [https://github.com/argoproj/argocd-example-apps.git](https://github.com/argoproj/argocd-example-apps.git) 是一个包含留言簿应用程序的示例库，我们可以用该应用来演示 Argo CD 的工作原理。
### 通过 CLI 创建应用
我们可以通过 argocd app create xxx 命令来创建一个应用

```bash
[root@master1 devops-argocd]# argocd app create --help
```

```bash
argocd app create guestbook --repo https://github.com/argoproj/argocd-example-apps.git --path guestbook --dest-server https://kubernetes.default.svc --dest-namespace default
application 'guestbook' created
```
很可能因为网络问题运行失败，运行多几次就行了，或者解析一下github.com
![在这里插入图片描述](https://img-blog.csdnimg.cn/ab32d532a7b84c6ca90ff8cc0c2a6474.png)

### 通过 UI 创建应用
我这里将应用删除了重新创建
除了可以通过 CLI 工具来创建应用，我们也可以通过 UI 界面来创建，定位到 argocd.k8s.local 页面，登录后，点击 +New App 新建应用按钮，如下图
将应用命名为 guestbook，使用 default project，并将同步策略设置为 Manual
![在这里插入图片描述](https://img-blog.csdnimg.cn/2b92b2f9bbc445a2a70f2640732f2458.png)
然后在下面配置 Repository URL 为 https://github.com/argoproj/argocd-example-apps.git，由于某些原因(科学上网问题)我们这里使用迁移到 Gitee 上面的仓库地址 [https://gitee.com/cnych/argocd-example-apps](https://gitee.com/cnych/argocd-example-apps)，将 Revision 设置为 HEAD，并将路径设置为 guestbook
![在这里插入图片描述](https://img-blog.csdnimg.cn/478099500f874e9b96a5889a5289a801.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/ead73cc18dbb45e4af8775ebef3acef1.png)
然后下面的 Destination 部分，将 cluster 设置为 in-cluster 和 namespace 为 default
![在这里插入图片描述](https://img-blog.csdnimg.cn/37fcedf6d47f43ab982c294bef6512ae.png)
填写完以上信息后，点击页面上方的 Create 安装，即可创建 guestbook 应用，创建完成后可以看到当前应用的处于 OutOfSync 状态
![在这里插入图片描述](https://img-blog.csdnimg.cn/76554079a0a14b42aea773228cdafe68.png)
## 部署应用
由于上面我们在创建应用的时候使用的同步策略为 Manual，所以应用创建完成后没有自动部署，需要我们手动去部署应用。同样可以通过 CLI 和 UI 界面两种同步方式
### 使用 CLI 同步
应用创建完成后，我们可以通过如下所示命令查看其状态：

```bash
$ argocd app get guestbook
Name:               guestbook
Project:            default
Server:             https://kubernetes.default.svc
Namespace:          default
URL:                https://argocd.k8s.local/applications/guestbook
Repo:               https://gitee.com/cnych/argocd-example-apps
Target:             HEAD
Path:               guestbook
SyncWindow:         Sync Allowed
Sync Policy:        <none>
Sync Status:        OutOfSync from HEAD (53e28ff)
Health Status:      Missing

GROUP  KIND        NAMESPACE  NAME          STATUS     HEALTH   HOOK  MESSAGE
       Service     default    guestbook-ui  OutOfSync  Missing
apps   Deployment  default    guestbook-ui  OutOfSync  Missing
```
应用程序状态为初始 OutOfSync 状态，因为应用程序尚未部署，并且尚未创建任何 Kubernetes 资源。要同步（部署）应用程序，可以执行如下所示命令：
```bash
argocd app sync guestbook
```
此命令从 Git 仓库中检索资源清单并执行 kubectl apply 部署应用，执行上面命令后 guestbook 应用便会运行在集群中了，现在我们就可以查看其资源组件、日志、事件和评估其健康状态了。
### 通过 UI 同步
直接添加 UI 界面上应用的 Sync 按钮即可开始同步：
![在这里插入图片描述](https://img-blog.csdnimg.cn/b6db6125c1884f89882036a0470d2071.png)
同步完成后可以看到我们的资源状态：
![在这里插入图片描述](https://img-blog.csdnimg.cn/44c02f8cc5354dcaa65d71ea23fe832a.png)
还可以有不同的视角进行查看：
![在这里插入图片描述](https://img-blog.csdnimg.cn/8fc1261610624dbf8805084256055e38.png)
也可以通过 kubectl 查看到我们部署的资源：

```bash
➜  ~ kubectl get pods
NAME                                      READY   STATUS      RESTARTS   AGE
guestbook-ui-6c96fb4bdc-nmk9b             1/1     Running     0          2m22s
➜  ~ kubectl get svc
NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
guestbook-ui         ClusterIP   10.96.32.11     <none>        80/TCP     11m
kubernetes           ClusterIP   10.96.0.1       <none>        443/TCP    41d
```
和我们从 Git 仓库中同步 guestbook 目录下面的资源状态也是同步的，证明同步成功了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/e6094d3b6ea64cf48e14d177451bb6c1.png)

