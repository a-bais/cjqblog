---
title: helm的chart包释放出来的release该不该命名
date: 2022-03-08 19:30:35
tags: kubernetes 容器 云原生
categories: k8s
---

<!--more-->

chart包生成release应用时不指定名称就会有个较长随机字符串组成的默认名称，这里有个好处，就是高度的唯一性。
如果自己定义名称，release对应的deploy和pod的标签会自动生成一个release=<rekease_name>，这是如果有服务正好是通过这个标签来绑定pod的，就会有问题了，所以不建议自定义release的名称，一定要定义话考虑命名的唯一性。

附：release对应的deploy名称就是release_name
