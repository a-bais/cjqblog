---
title: dql实操命令汇总
date: 2021-09-29 11:12:47
tags: mysql sql 数据库
categories: 数据库mysql mariadb redis
---

<!--more-->

各个命令的逻辑都是思考设置过的，按顺序看，一一比对可以较系统学习和了解**细节差异**，由于命令过多就不一一注释解析，有问题的可以留言

```bash
mysql> select distinct name from tb2;
+--------+
| name   |
+--------+
| cjq    |
| cjq100 |
+--------+
2 rows in set (0.01 sec)

mysql> select distinct name,grade from tb2;
+--------+-------+
| name   | grade |
+--------+-------+
| cjq    |     3 |
| cjq100 |   100 |
+--------+-------+
2 rows in set (0.00 sec)

mysql> select distinct name grade from tb2;
+--------+
| grade  |
+--------+
| cjq    |
| cjq100 |
+--------+
2 rows in set (0.00 sec)

mysql> select (distinct name),grade from tb2;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'distinct name),grade from tb2' at line 1
mysql> select (distinselect ct name),grade from tb2;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ct name),grade from tb2' at line 1
mysql> select distinct name,grade from tb2;
+--------+-------+
| name   | grade |
+--------+-------+
| cjq    |     3 |
| cjq100 |   100 |
+--------+-------+
2 rows in set (0.00 sec)

mysql> select * from tb2;
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
|  4 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
7 rows in set (0.00 sec)

mysql> insert into tb2(address) values("beijing");
Query OK, 1 row affected (0.00 sec)

mysql> select * from tb2;
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
|  4 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
| 14 | NULL   |  NULL | beijing  |
+----+--------+-------+----------+
8 rows in set (0.00 sec)

mysql> select avg(id) from tb2 where id>=6;
+---------+
| avg(id) |
+---------+
| 10.4286 |
+---------+
1 row in set (0.00 sec)

mysql> select avg(id) as id from tb2 where id!=6;
+---------+
| id      |
+---------+
| 10.1429 |
+---------+
1 row in set (0.00 sec)

mysql> select avg(id) as id2 from tb2 where id!=6;
+---------+
| id2     |
+---------+
| 10.1429 |
+---------+
1 row in set (0.00 sec)

mysql> select avg(id) as id2 from tb2 where id2=6;
ERROR 1054 (42S22): Unknown column 'id2' in 'where clause'
mysql> select avg(id) as id2 from tb2 where id<id2;
ERROR 1054 (42S22): Unknown column 'id2' in 'where clause'
mysql> select avg(id) as id2 from tb2 where id<avg(id);
ERROR 1111 (HY000): Invalid use of group function
mysql> select avg(id) as id2 from tb2 having id<avg(id);
ERROR 1054 (42S22): Unknown column 'id' in 'having clause'
mysql> select avg(id) as id2 from tb2 having avg(id)>10;
Empty set (0.00 sec)

mysql> select avg(id) as id2 from tb2 having avg(id)<10;
+--------+
| id2    |
+--------+
| 9.6250 |
+--------+
1 row in set (0.00 sec)

mysql> select avg(id) as id2 from tb2 having id2<10;
+--------+
| id2    |
+--------+
| 9.6250 |
+--------+
1 row in set (0.00 sec)

mysql> select avg(id) as id2 from tb2 where id2<10;
ERROR 1054 (42S22): Unknown column 'id2' in 'where clause'
mysql> select avg(id) as id2 from tb2 where avg(id)<10;
ERROR 1111 (HY000): Invalid use of group function
mysql> select avg(id) as id2 from tb2 where id<10;
+--------+
| id2    |
+--------+
| 5.6667 |
+--------+
1 row in set (0.00 sec)

mysql> select avg(id) as id2 from tb2 having id<10;
ERROR 1054 (42S22): Unknown column 'id' in 'having clause'
mysql> select * from tb2 name like "cjq%";
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'like "cjq%"' at line 1
mysql> select * from tb2 where name like "cjq%";
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
|  4 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
7 rows in set (0.00 sec)

mysql> select * from tb2 where name like "cjq";
+----+------+-------+----------+
| id | name | grade | address  |
+----+------+-------+----------+
|  4 | cjq  |     3 | jiangmen |
|  6 | cjq  |     3 | jiangmen |
|  7 | cjq  |     3 | jiangmen |
| 10 | cjq  |     3 | jiangmen |
| 11 | cjq  |     3 | jiangmen |
+----+------+-------+----------+
5 rows in set (0.00 sec)

mysql> select * from tb2 where name like "cj_";
+----+------+-------+----------+
| id | name | grade | address  |
+----+------+-------+----------+
|  4 | cjq  |     3 | jiangmen |
|  6 | cjq  |     3 | jiangmen |
|  7 | cjq  |     3 | jiangmen |
| 10 | cjq  |     3 | jiangmen |
| 11 | cjq  |     3 | jiangmen |
+----+------+-------+----------+
5 rows in set (0.00 sec)

mysql> select * form tb2 where grade between 2 and 5;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'form tb2 where grade between 2 and 5' at line 1
mysql> select * from tb2 where grade between 2 and 5;
+----+------+-------+----------+
| id | name | grade | address  |
+----+------+-------+----------+
|  4 | cjq  |     3 | jiangmen |
|  6 | cjq  |     3 | jiangmen |
|  7 | cjq  |     3 | jiangmen |
| 10 | cjq  |     3 | jiangmen |
| 11 | cjq  |     3 | jiangmen |
+----+------+-------+----------+
5 rows in set (0.00 sec)

mysql> select * from tb2 where grade not in(3,4);
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
2 rows in set (0.00 sec)

mysql> select distinct * from tb2 where grade not in(3,4);
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
2 rows in set (0.00 sec)

mysql> select distinct grade from tb2 where grade not in(3,4);
+-------+
| grade |
+-------+
|   100 |
+-------+
1 row in set (0.00 sec)

mysql> select distinct grade from tb2 where grade in(3,4);
+-------+
| grade |
+-------+
|     3 |
+-------+
1 row in set (0.00 sec)

mysql> select distinct grade from tb2 where grade=3 or grade=100;
+-------+
| grade |
+-------+
|     3 |
|   100 |
+-------+
2 rows in set (0.00 sec)

mysql> select distinct grade from tb2 where grade!=3 and grade<>100;
Empty set (0.00 sec)

mysql> select distinct grade from tb2 where grade!=5 and grade<>100;
+-------+
| grade |
+-------+
|     3 |
+-------+
1 row in set (0.00 sec)

mysql> select * from tb2 order by id asc;
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
|  4 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
| 14 | NULL   |  NULL | beijing  |
+----+--------+-------+----------+
8 rows in set (0.00 sec)

mysql> select * from tb2 order by id desc;
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
| 14 | NULL   |  NULL | beijing  |
| 13 | cjq100 |   100 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  4 | cjq    |     3 | jiangmen |
+----+--------+-------+----------+
8 rows in set (0.00 sec)

mysql> select * from tb2 group by address;
ERROR 1055 (42000): Expression #1 of SELECT list is not in GROUP BY clause and contains nonaggregated column 'db1.tb2.id' which is not functionally dependent on columns in GROUP BY clause; this is incompatible with sql_mode=only_full_group_by
mysql> select avg(id) from tb2 group by address;
+---------+
| avg(id) |
+---------+
| 14.0000 |
|  9.0000 |
+---------+
2 rows in set (0.00 sec)

mysql> select avg(id) from tb2 where id>6 group by address;
+---------+
| avg(id) |
+---------+
| 14.0000 |
| 10.6000 |
+---------+
2 rows in set (0.00 sec)

mysql> select avg(id) as avfrom tb2 where id>6 group by address having av>11;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'tb2 where id>6 group by address having av>11' at line 1
mysql> select avg(id) as av from tb2 where id>6 group by address having av>11;
+---------+
| av      |
+---------+
| 14.0000 |
+---------+
1 row in set (0.00 sec)

mysql> select avg(id) as av from tb2 where id>6 group by address having av>11 order by av asc;
+---------+
| av      |
+---------+
| 14.0000 |
+---------+
1 row in set (0.00 sec)

mysql> select avg(id) from tb2 where id>6 group by address having avg(id)>11 order by avg(asc);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'asc)' at line 1
mysql> select avg(id) from tb2 where id>6 group by address having avg(id)>11 order by avg(id) asc;
+---------+
| avg(id) |
+---------+
| 14.0000 |
+---------+
1 row in set (0.00 sec)

mysql> select grade,avg(ifnull(grade,0)) from tb2 group by grade;
+-------+----------------------+
| grade | avg(ifnull(grade,0)) |
+-------+----------------------+
|  NULL |               0.0000 |
|     3 |               3.0000 |
|   100 |             100.0000 |
+-------+----------------------+
3 rows in set (0.00 sec)

mysql> select avg(ifnull(grade,0)) from tb2 group by grade;
+----------------------+
| avg(ifnull(grade,0)) |
+----------------------+
|               0.0000 |
|               3.0000 |
|             100.0000 |
+----------------------+
3 rows in set (0.00 sec)

mysql> select * from tb2 grade<=(select avg(ifnull(grade,0)) from tb2 group by grade);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '<=(select avg(ifnull(grade,0)) from tb2 group by grade)' at line 1
mysql> select * from tb2 where grade<=(select avg(ifnull(grade,0)) from tb2 group by grade);
ERROR 1242 (21000): Subquery returns more than 1 row
mysql> select * from tb2 where grade in (select avg(ifnull(grade,0)) from tb2 group by grade);
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
|  4 | cjq    |     3 | jiangmen |
|  6 | cjq    |     3 | jiangmen |
|  7 | cjq    |     3 | jiangmen |
| 10 | cjq    |     3 | jiangmen |
| 11 | cjq    |     3 | jiangmen |
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
7 rows in set (0.00 sec)

mysql> select * from tb2 where grade in (select avg(ifnull(grade,0)) as av from tb2 group by grade having av>=100 );
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
2 rows in set (0.00 sec)

mysql> 
mysql> select * from tb2 where grade >=(select avg(ifnull(grade,0)) as av from tb2 group by grade having av>=100 );
+----+--------+-------+----------+
| id | name   | grade | address  |
+----+--------+-------+----------+
| 12 | cjq100 |   100 | jiangmen |
| 13 | cjq100 |   100 | jiangmen |
+----+--------+-------+----------+
2 rows in set (0.00 sec)

mysql> select avg(id) from tb2
    -> ;
+---------+
| avg(id) |
+---------+
|  9.6250 |
+---------+
1 row in set (0.00 sec)

mysql> select sum(id) from tb2
    -> ;
+---------+
| sum(id) |
+---------+
|      77 |
+---------+
1 row in set (0.00 sec)

mysql> select count(id) from tb2;
+-----------+
| count(id) |
+-----------+
|         8 |
+-----------+
1 row in set (0.00 sec)

mysql> select max(id) from tb2;
+---------+
| max(id) |
+---------+
|      14 |
+---------+
1 row in set (0.00 sec)

mysql> select min(id) from tb2;
+---------+
| min(id) |
+---------+
|       4 |
+---------+
1 row in set (0.00 sec)


```

