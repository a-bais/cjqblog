---
title: nginx配置实操:nginx location
date: 2021-09-25 21:18:16
tags: nginx php
categories: nginx配置实操（yum方式安装）
---

<!--more-->

location语法优先级:
1.= 精确匹配
2.^~ 以某个字符串开头
3.~ 区分大小写的匹配
4.~* 不区分大小写的匹配
5.!~ 区分大小写的不匹配
6.!~* 不区分大小写的不匹配
7./ 通用匹配，任何请求都会匹配到

```bash
location /{
	default_type text/html;  #网站类型，必须是文本或html
	return 200 "location /";  #如果访问的status是200，则返回location /
}

location =/{
	default_type text/html;
	return 200 "location =/;
}
精确匹配，如http://192.168.160.100/

location ~/{

}

location ~ .php${

}
匹配以.php结尾的使用这个location

location ~* .*\.(jpg|gif|png|js|css){

}
不区分大小写匹配到.jpg或.gif等会用这个location

location ~* .*\.(sql|bak|tgz|tar.gz|git)${
}
不区分大小写匹配以.sql等结尾的使用这个location


.表示任意一个字符
*表示任意个前一个re字符
那么.*表示任意个字符
.是有意义的，所以用\转义符将其编程普通字符
```

