---
title: 记录一个关闭vm进程后重启虚拟机连不上的问题
date: 2021-11-12 21:09:09
tags: 网络 centos linux
categories: 解决错误问题
---

<!--more-->

![在这里插入图片描述](https://img-blog.csdnimg.cn/bb9d09c70a234b6fbaa61e298ff5ed9e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
ping本机没问题
ping同网段的和百度不行
问题就定位在硬件上，网卡

网上查了资料，主要是重启vm后**NAT**和**DHCP**一般不会自动重启，任务管理看一下果然没开启这两个服务
那我就找到这两个服务的可执行文件，但是发现就算用windows页启动不了这两个程序。
好的那换个法子启动，去vm中**编辑虚拟网络**
![在这里插入图片描述](https://img-blog.csdnimg.cn/0e3e64fa6add4928a0995fc2c0ee1d9b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
我这里本身用的是NAT网卡，那我就从它入手，点击更改配置然后确定，发现并没有启动以上那两个服务，
好那就将网卡删了，重新加入NAT网卡(这里留意下网段的配置如192.168.160.100，还有勾上NAT)
ok，网络问题解决
