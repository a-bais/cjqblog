---
title: redis的set类型的命令用于多个key值是的情况
date: 2021-09-15 22:39:50
tags: redis
categories: 数据库mysql mariadb redis
---

<!--more-->

sinter:获取共同的value
如:sinter set1 set2 set3
获取三个集合的共同的value
______________
sunion:获取集合所有值（不重复）
sunion set1 set2 set3
获取三个集合的所有值
______________
sdiff:比较第一个集合与后边集合的差异，即第一个集合特有的
sdiff set1 set2 set3
第一个相对于第二个加第三个两集合的独有的value
即set1-(set2+set3)
______________
smembers:查看集合的值
smembers set1
不可以同时查看多个集合的值
