---
title: shell编程之四剑客awk、sed、find、grep
date: 2021-06-10 10:56:37
tags: shell
categories: 主要#笔记资料
---

<!--more-->

四剑客

**find：**
》》》》》》》》》》》》》》》》

>find [path] [option] [action]
> 
> 

> find / -name "*txt",通配符,在/下找名字以.txt为后缀的文件
 find / -name "*txt“ -type -d ，type指定文件类型这里是目录，type f普通文件,详细的类型man查看
find / -mtime 0 ,0代表当前的时间，意思**从现在开始到24小时前**有被变更过内容的文件都会显示(mtime修改时间，ctime状态改变时间，atime访问时间)
find / -mtime 3 3天前的那24小时内内容变更过的文件
find /etc -newer /etc/passwd
-mtime n:意思n天前的【一天之内】被修改过内容的文件
-mtime +n:列出n天之前(不含n天本身)被修改过内容的文件
-mtime -n:列出n天之内(含n天本身)bei修改过内容的文件
-newer file：file是一个已经存在的文件，找比file新的
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210609110047644.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
(以下的\与;是没有空格的，另外建议用-exec而不是|xargs,-exec支持更广，两者效率差不多)
find / -name "*.txt" -type d -mtime -1 | xargs rm -rf {} \ ;
意思是将find的输出的相关文件删除掉
find / -name "*.txt" -type d -mtime -1 -exec cp -r {} /tmp \ ;(常考题)
ls | xargs rm -rf {}
将find输出的文件递归cp到/tmp下
find / -name "*.txt" -type d -mtime -1 -size +1k
指定文件的大小 k,M,1024k等等写法
find / -name "*.txt" -type d -mtime -1 -size +1k -perm 755
-perm指定权限(掩码默认022)
find / -type f -exec chmod -R 644\ ;
统一递归修改(子目录可以继承)对应类型的文件的权限
find / -name "*.txt" -type d -mtime -1 -size +1k -perm 755 -exec mv {} \ ;
移动的意思

文件种类:
d目录；
l链接文件；
b区块设备文件；
c字符设备文件；
p数据输送文件；
一般文件 -，包括纯文本文件（如用cat读取的，普通的记录着相关内容的文件）和二进制文件（如命令cat，可执行）和数据文件（有些程序运行时会读取浙西特殊格式的文件，用last可以读取，应用cat读取会出现乱码）


**grep:**
》》》》》》》》》》》》》

> ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210609201512199.png)
grep -v "^#" /usr/local/nginx/conf/nginx.conf.default | grep -v "^$"
-v反向匹配，|后的语句作用过滤空行
-a 以文本文件方式搜索
-i 忽略大小写
-l 查询多文件时只输出包含匹配字符的文件名
-c 计算找到的符合行的次数
-n 行号
-v 反向匹配
-h查询多文件时不显示文件名
-s 不显示不存在或无匹配文件的错误信息
-E grep -E "a|b"
.........................
grep "[0-9]" test.txt 匹配0到9任意一个字符
grep --color "[0-9][0-9]" test.txt
egrep是grep的升级版，也是行匹配
egrep --color "[0-9]{1}" test.txt 其中{1}意思匹配一次
{1,3}匹配1到3次，{，3}最多匹配3次，{1，}至少匹配1次
...
(\与.之间没有空格, $前没有空格)
egrep --color "[0-9]{1,3}\ .[0-9]{1,3}\ .[0-9]{1,3}\ .[0-9]{1,3} $" test.txt
优化:egrep --color "([0-9]{1,3}\ .){3}[0-9]{1,3} $" test.txt
\是转义字符

grep正则表达式元字符集: 
^ 锚定行的开始
$ 锚定行的结束
. 匹配一个非换行符的
*匹配零或多个先前字符
.*任意字符
[]匹配一个指定范围内的字符如[Gg]
[^]匹配一个不在指定范围内的字符如[Gg]
/(../)标记匹配字符如'/(love/)',love标记为1
/<锚定单词的开始
/>锚定单词的结束，如‘grep/>’匹配包含以grep结尾的单词的行
/w匹配文字和数字字符，即[A-Za-z0-9]


**awk:**
》》》》》》》》》》》》》》》》

```
cat test.txt --> my name is a
awk '{print $1}' test.txt --> my
awk '{print $NF}' test.txt --> a
awk '{print $(NF-1}' test.txt --> is
awk '{print $1}' /etc/passwd |head -n 5
awk -F: '{print $1}' /etc/passwd |head -n 5
awk -F: '{print $1,$NF}' /etc/passwd |head -n 5
awk -F: '{print $1":"$NF}' /etc/passwd |head -n 5
awk -F: '{print $1" and "$NF}' /etc/passwd |head -n 5

NF:每一行的字段总数
NR:当前处理第几行
FS:目前的分隔符
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210609211102712.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210609211317601.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210609211948696.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210610104042887.png)

**sed:**

```
cat test.txt -->my name is a,my name is a
sed 's/a/good/' test.txt
sed 's/a/good/1' test.txt 替换1次
sed 's/a/good/2' test.txt 替换2次
sed 's/a/good/g' test.txt 全部替换
sed 's#a#good#g' test.txt 全部替换
sed 's#a#///good#g' test.txt 全部替换，#可略/
sed 's/a/\/\/\/good/g' test.txt 转义符\
cat test.txt
sed -i 's/a/\/\/\/good/g' test.txt -i写入
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210610105417245.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
''是强引，如果用到变量则需要用弱引""














