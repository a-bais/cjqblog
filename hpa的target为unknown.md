---
title: hpa的target为unknown
date: 2022-03-03 13:51:01
tags: docker kubernetes 容器
categories: 解决错误问题 k8s
---

<!--more-->

hpa对应的下层RS或Deployment的pod模板下de1container设置要写上requests和limits的配置，
最重要的，控制器不要使用通用标签，着点很容易报错

hpa需要metrics server获取的数据来计算，而这些对象间的匹配底层应该就是通过标签来匹配，不是简单的在hpa配置文件中指定控制器就可以(没有深究,记录个坑)
hpa其实一般，建议使用别的方案

![在这里插入图片描述](https://img-blog.csdnimg.cn/35adeb27c55f44619420b7508f39106e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

