---
title: 关于initcontainer容器执行顺序的一定性带来的克数情况记录
date: 2021-11-15 23:06:08
tags: 容器 服务器 mysql
categories: k8s
---

<!--more-->

假设我要启动一个pod,里边只运行一个容器(pause容器所以基于这里可先忽略)，要运行的容器也就是主容器maincontainer，在这个主容器运行之前会先运行inti容器，

比如获取某些目录下的某些文件，而系统处于安全性管理不愿意让主容器获取这些目录和相关文件的权限，但又要使用到这些文件，基于initcontainer初始化容器初始化正常后就关闭了，所以让initcontainer有权限去调用这些文件，这样既能调用到要用的文件有能提高安全性。

再举个例子，主容器是nginx，运行前要检测nginx服务器能否连接到mysql服务器和php服务器，那mysql服务器来说，让initcontainer先去检测能否ping通mysql服务器，能ping通则继续运行下一个initcontainer,最后到主容器。

这里有这么一个情况，mysql服务器还没搭建，ifconfig本地一张往卡上再定一个网络接口192.168.160.201给initcontainer来ping，能ping通，那么就运行下一个初始化容器，这是重启网络，之前定的临时网络接口就没了，那么这时候容器会因为对应的那个initcontainer出现错误吗？不会，即使现在网络接口没了，但initcontainer已经完成了ping检测，任务已经完成了，到下一个initcontainer,而且他是短暂性的检测工作，而不是一直以来的工作，所以不会有问题

附：网络接口设置时，最好是同个网段内，而且lnmp结构，内部服务器通常也是内网的集群模式。
