---
title: cp递归复制目录
date: 2021-07-28 20:04:18
tags: 
categories: 解决错误问题
---

<!--more-->

cp递归复制目录，-r,常用语复制目录
cp -r 目录 目标路径(目录)
eg:cp -r ./dir/ ~/
注意的是这里如果./dir/*便是复制底下的文件而不是复制目录


> -r:rm,cp,mv,意为递归操作
> -p:mkdir,rmdir,意为连带上父目录

