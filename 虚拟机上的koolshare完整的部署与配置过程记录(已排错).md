---
title: 虚拟机上的koolshare完整的部署与配置过程记录(已排错)
date: 2021-10-24 15:14:39
tags: k8s
categories: k8s
---

<!--more-->

前言：已经将自己在部署koolshare上踩的坑填上了

**开始:**
—————————————————————————————————————————————

**准备ios文件写入软件**
这里使用UltraISO
下载地址：https://www.onlinedown.net/soft/614.htm
![在这里插入图片描述](https://img-blog.csdnimg.cn/f6717072a79c45209781c726b628cfde.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**准备虚拟pe软件**
这里使用：老毛桃软件
下载地址：https://www.laomaotao.net/down/
![在这里插入图片描述](https://img-blog.csdnimg.cn/73be4c1a72b84773a7f93a436fc69253.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**openwrt-koolshare**
这里使用：openwrt-koolshare-mod-v2.31
下载地址：http://firmware.koolshare.cn/LEDE_X64_fw867/%E8%99%9A%E6%8B%9F%E6%9C%BA%E8%BD%AC%E7%9B%98%E6%88%96PE%E4%B8%8B%E5%86%99%E7%9B%98%E4%B8%93%E7%94%A8/
![在这里插入图片描述](https://img-blog.csdnimg.cn/885c0d46384842bd984d931963ed49e9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**IMG写盘软件**
本文档使用：IMG写盘工具
下载地址：http://ttl7.pc6.com/wgd/IMGxiepan.zip
![在这里插入图片描述](https://img-blog.csdnimg.cn/c5124bae9e654b33b60fd211c3c40cd7.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/69d39369f2634a41bd010cc4d4c7f8c1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
**创建虚拟机PE的ios镜像:**
![在这里插入图片描述](https://img-blog.csdnimg.cn/11b682957ce04c25b4c54a57a39490f4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/dfc4f2aa1dc4495ab0ac3da9794cd965.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_16,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/579353dc60f94a64892c3acc7f87092c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**创建koolshare系统文件ios(后续可以直接当做类似光盘一样放进PE虚拟机中,方便):**
![在这里插入图片描述](https://img-blog.csdnimg.cn/934ce3fa4fda495598ba69d9352bcfd9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/878e4d0012304255bb2dbc1039026479.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/dcb69eb3f2a94557bc16a4c8af4d549b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/4dc1baaca5a74924abfec78aa4c96f7c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

![在这里插入图片描述](https://img-blog.csdnimg.cn/e5145202c94e4eeba40a1376f519c187.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/976c965aba1c4d03a03c5ca206de7ee2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/4f0ec424fb6d484181909212c03a9d9b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

**创建虚拟机:**
![在这里插入图片描述](https://img-blog.csdnimg.cn/230ac75ab2d04082bd58294172a1d125.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/5f6a6187dd074a44aeb397502ec47662.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6e687079e53646a0ad03bff96142a768.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/3699fa832d7b4b87b864e716f074845a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/431847ee47b54300bcee7fc0f769c56b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f470ae91a22b44d3a4eef279a41d2a26.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/73857955103949ea8b569bcf18426911.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/efaa4922db4b43438ca54705a6f621bd.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/10c431086a77467787c5381808b34bd2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/83d46cf9a8d94c89ba6511eaa614e6b8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/57e3a840303247d1b728ebf1fd334b0b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/02b9718151b64a86930133437528de76.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/765589f3b9a444ef860bd58438d9d5e0.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/05f854e6082642d5b07fa27bf67596e6.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f51701bff8164154b29c1bea752609a5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/a9228f86cce441fdb8796c815a6f7e97.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/289ef7aa59394989a5ad11ea3fdbf2f8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e75ce011f28a48f98499d5a2416a6a2d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
然后点击PE里面的**此电脑**，点击**DVD驱动器**，
可以看见这两个文件夹
![在这里插入图片描述](https://img-blog.csdnimg.cn/96a2b797906a4f8ebc943a64aff15578.png)
点击**IMGxiepan**,开启**IMG写盘工具.exe**,选择上图的文件夹openwrt-kollshare-mod下的文件
为映像档，
![在这里插入图片描述](https://img-blog.csdnimg.cn/c3374cf1bf0b458c844cd0115fa0f8ef.png)
点击**开始**
等待镜像写入完成然后重启系统，那么koolshare系统就安装完成了
![在这里插入图片描述](https://img-blog.csdnimg.cn/890a9bcc435c472ca40f0241c10e2391.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d2a340d957ac405ca85cc1c21843a125.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
ip a查看网络信息(有时候添加了新网卡需要reboot下才看得到信息)
![在这里插入图片描述](https://img-blog.csdnimg.cn/7402b6759533460c80d2627461a9a547.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/199559d4f3c34c23a55541e505bd5dec.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
一句话，**先主机后nat**
![在这里插入图片描述](https://img-blog.csdnimg.cn/9df06aa551e342e5805961179bfbca6e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/89716816857241699faecce5dcdc10f1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20843a005ce04418be74654dcff65a72.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_17,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/0ed5f2c0e97744f698978c7fbfdebb2d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_19,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/9922acbc6c3548e7bcc10edc7bbb2496.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_19,color_FFFFFF,t_70,g_se,x_16)
依次点击确定
![在这里插入图片描述](https://img-blog.csdnimg.cn/1935a16ac9ac448ab0b279644f305ed1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2fcdf5fc45b44e97863078ed71cc3e9e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2f38c4f8d64e4608bc111ca7c7bf928c.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/9bcddc6eb57d4deea2a3bad6f08c6820.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/ed358d595624459ba771d3822344f775.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/ec47b46149cf4734b4c201b68d4c3eb8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/74a889ee49f1424284ddce0d2214d6a2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
上面的66.1的设置后可能服务会出错，这样解决:
![在这里插入图片描述](https://img-blog.csdnimg.cn/4b0c14aac3f1475995aa7bd3c26e47c5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
然后/etc/init.d/network restart重启网络
ip a查看网络
即可解决

参看与这位博主的文章:
```bash
关于 koolShare 配置两块网卡遇到的坑【搞了一天】

1、两张网卡的连接模式顺序很重要，必须是第一张网卡“仅主机”，第二张网卡“NAT”。

因为：虚拟机默认第一张网卡 （eth-0 lan）连接内网，第二张网卡（eth-1 wan）连接外网

2、在 1 的基础上，koolShare 虚拟机启动后，只需要设置一个IP地址M： 192.168.XX.YY

【X,Y 自由指定，但需要避开 “NAT/VMnet8” 和 “宿主机” 网段】

设置好之后，再打开宿主机 VMnet1 设置。

（1）第一个IP地址：网段固定为 192.168.1, 1之后除了1都可以

（2）第二个IP地址：192.168.XX.ZZ【网段同M，最后一段不同】,

这个地址即为：仅主机模式下之网关

3、使用 192.168.XX.YY 通过浏览器登录 koolShare 后，只需将 LAN 口的“物理设置”之“桥接接口”取消

=================================

4、挂在 koolShare 局域网之内的虚拟机

网关、DNS 均设置为 koolShare 虚拟机 之内网地址M： 192.168.XX.YY
————————————————
版权声明：本文为CSDN博主「NRlovestudy」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/NRlovestudy/article/details/104526481
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/887f8267fa204f2895dd3e640060cb4a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
网上搜索koolss的软件包同时下载下来
![在这里插入图片描述](https://img-blog.csdnimg.cn/edeffd7c136746faa782de371b5af155.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e045a2096e5d4390ac10af891f3c55b1.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
koolss的使用就不说了，似乎也不大重要这里
