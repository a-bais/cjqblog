---
title: redis安装部署
date: 2021-09-13 22:08:26
tags: redis 数据库
categories: 数据库mysql mariadb redis 常用软件的安装
---

<!--more-->

```bash
mkdir /data/redis_cluster

准备自行下载的软件安装包的存放路径和下载redis
mkdir /data/soft;cd /data/soft
wget  https://repo.huaweicloud.com/redis/redis-3.2.9.tar.gz

安装redis
tar xf redis-3.2.9.tar.gz -C /data/redis_cluster
cd /data/redis_cluster
ln -s redis-3.2.9 redis
cd redis
mkdir conf pid logs data
cd /data/redis_cluster/redis-3.2.9/src
make
make install

使用redis官方脚本生成redis的配置文件
[root@localhost redis]# cd utils/
[root@localhost utils]# ls
build-static-symbols.tcl  corrupt_rdb.c   generate-command-help.rb  hyperloglog        lru            redis_init_script      redis-sha1.rb  speed-regression.tcl
cluster_fail_time.tcl     create-cluster  hashtable                 install_server.sh  redis-copy.rb  redis_init_script.tpl  releasetools   whatisdoing.sh
[root@localhost utils]# sh install_server.sh 
Welcome to the redis service installer
This script will help you easily set up a running redis server

Please select the redis port for this instance: [6379] 
Selecting default: 6379
Please select the redis config file name [/etc/redis/6379.conf] 
Selected default - /etc/redis/6379.conf
Please select the redis log file name [/var/log/redis_6379.log] 
Selected default - /var/log/redis_6379.log
Please select the data directory for this instance [/var/lib/redis/6379] 
Selected default - /var/lib/redis/6379
Please select the redis executable path [/usr/local/bin/redis-server] 
Selected config:
Port           : 6379
Config file    : /etc/redis/6379.conf
Log file       : /var/log/redis_6379.log
Data dir       : /var/lib/redis/6379
Executable     : /usr/local/bin/redis-server
Cli Executable : /usr/local/bin/redis-cli
Is this ok? Then press ENTER to go on or Ctrl-C to abort.
Copied /tmp/6379.conf => /etc/init.d/redis_6379
Installing service...
Successfully added to chkconfig!
Successfully added to runlevels 345!
Starting Redis server...
Installation successful!

配置文件
[root@localhost redis-3.2.9]# cat /etc/redis/6379.conf | grep -v "^#" | grep -v "^$"
bind 127.0.0.1                            #绑定本机地址，可以再加上本机ip用空格隔开，如192.168.160.168
protected-mode yes
port 6379                                 #监听端口
tcp-backlog 511
timeout 0
tcp-keepalive 300
daemonize yes       					  #以守护进程模式启动
supervised no
pidfile /data/redis_cluster/redis_6379/pid/redis_6379.pid           #pid文件所在路径
loglevel notice
logfile /data/redis_cluster/redis_6379/logs/redis_6379.log           #log文件所在路径
databases 16                              #数据库的数量
save 900 1
save 300 10
save 60 10000
stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename redis_6379.rdb                 #指定本地持久化文件的文件名，默认dump.rdb
dir /var/lib/redis/6379                   #本地数据库的目录，持久化文件路径
slave-serve-stale-data yes
slave-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5
repl-disable-tcp-nodelay no
slave-priority 100
appendonly no
appendfilename "appendonly.aof"
appendfsync everysec
no-appendfsync-on-rewrite no
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
lua-time-limit 5000
slowlog-log-slower-than 10000
slowlog-max-len 128
latency-monitor-threshold 0
notify-keyspace-events ""
hash-max-ziplist-entries 512
hash-max-ziplist-value 64
list-max-ziplist-size -2
list-compress-depth 0
set-max-intset-entries 512
zset-max-ziplist-entries 128
zset-max-ziplist-value 64
hll-sparse-max-bytes 3000
activerehashing yes
client-output-buffer-limit normal 0 0 0
client-output-buffer-limit slave 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60
hz 10
aof-rewrite-incremental-fsync yes

启动和查看redis
[root@localhost pid]# redis-server /data/redis_cluster/redis_6379//conf/redis_6379.conf                 #另外指定了配置文件
[root@localhost pid]# ps aux | grep redis
root      77842  0.1  0.1 136976  2184 ?        Ssl  21:30   0:02 /usr/local/bin/redis-server 127.0.0.1:6379
root      78838  0.0  0.0 112712   960 pts/0    R+   22:04   0:00 grep --color=auto redis
[root@localhost pid]# systemctl status redis
Unit redis.service could not be found.
[root@localhost pid]# netstat -tunlp | grep redis
tcp        0      0 127.0.0.1:6379          0.0.0.0:*               LISTEN      77842/redis-server

建个文件/data/redis_cluster/redis_6379/conf/redis_6379.conf，将有我注释的那些配置写入(最好将主配置文件较为完整的复制过来),同时将这个文件include到/etc/redis/6379.conf,上边的配置文件某些注释的配置是我已经修改过的。
/data/redis_cluster/redis_6379/logs/redis_6379.log和/data/redis_cluster/redis_6379/pid/redis_6379.pid文件页准备好
```
附:
![在这里插入图片描述](https://img-blog.csdnimg.cn/e10c5808fd7948d28c5dd4a5294822c5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

![在这里插入图片描述](https://img-blog.csdnimg.cn/ed67cbe1165b45378e03e421fea6eb0a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
自行安装的软件后各类可执行命令放在/usr/local/bin


