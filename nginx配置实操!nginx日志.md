---
title: nginx配置实操:nginx日志
date: 2021-09-24 14:14:49
tags: nginx 运维 http
categories: nginx配置实操（yum方式安装）
---

<!--more-->

log_format指令:用来定义日志格式，对error.log和access.log均生效，一般用来设置access.log
配置区域是http，即对所有站点生效

**access_log配置**
调用:access_log path

```bash
vim /etc/nginx/nginx.conf

user  nginx;
  3 worker_processes  auto;
  4 
  5 #error_log  /var/log/nginx/error.log notice;			#注释
  6 pid        /var/run/nginx.pid;
  7 
  8 
  9 events {
 10     worker_connections  1024;
 11 }
 12 
 13 
 14 http {
 15     include       /etc/nginx/mime.types;
 16     default_type  application/octet-stream;
 17 
 18     log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
 19                       '$status $body_bytes_sent "$http_referer" '
 20                       '"$http_user_agent" "$http_x_forwarded_for"';
 21 
 22     log_format a1_log '$remote_addr - $remote_user [time_local] $http_host "request" $request_time'
 23                       '$status $body_bytes_sent "$http_refer"'
 24                       '"$http_user_agent" "$http_x_forwarded_for"';				#自定义一个日志格式
 25 
 26     #access_log  /var/log/nginx/access.log  main;			#注释
 27 
 28     sendfile        on;
 29     #tcp_nopush     on;
 30 
 31     keepalive_timeout  65;
 32 
 33     #gzip  on;
 34     #limit_conn_zone $binary_remote_addr zone=a1_conn:10m;
 35     include /etc/nginx/conf.d/*.conf;
 36 }

```
虽然注释了主配置文件的error_log和access_log，但是如果站点的配置文件不设置好access_log或error_log依然会使用主配置中的这两个默认路径
![在这里插入图片描述](https://img-blog.csdnimg.cn/af73060fc54641ae812bb1e463c57155.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash
创建日志文件的路径

mkdir /nginx_log
nginx -s reload 
systemctl reload nginx
一般对于日志文件路径，创建前边的目录后，服务重启或者说重载就会自动创建，不行就自己手动创建即可
```

附:
**$remote_addr   记录客户端ip
$remote_user	客户端的用户名
$time_local		通用的本地时间
$request			请求的方法请求的文件请求的http协议
$status			请求的状态码
$body_bytes_sent			发送给客户端的资源字节数
$http_refer						记录从哪个页面链接访问过来
$http_host						请求地址也就是浏览器输入的访问地址
$http_user_agent			客户端的浏览器信息
$http_x_forwarded_for	记录真实的客户端ip
$request_time					请求花费的时间
$upstream_status			后台服务器的响应状态码
$upstream_response_time			请求过程中后台服务器的响应时间
$upstream_addr							后台服务器的地址，真正提供服务的主机地址**

$msec								日志写入时间
$byte_sent						发送给客户端的总字节数
$time_iso8601					记录iso8601标准格式下的本地时间
$ssl_protocol								ssl协议版本
$ssl_ciper									交换数据中的算法
