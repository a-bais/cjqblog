---
title: sed细节留意
date: 2021-08-02 22:39:21
tags: 
categories: 解决错误问题 重点实操记录
---

<!--more-->

1.sed -rn 's/t1/t2/gp'
这个p是全局打印，n表示忽略符合执行命令前的行，所以如果去掉n则会显示全部
n一般与p一块用

2.sed是一种在线的、非交互式的编辑器，它一次处理一行内容，处理时，把当前处理的行存储在临时缓冲区中，成为模式空间，接着用sed命令处理缓冲区中的内容，处理完成后，把缓冲区的内容送往屏幕。接着处理下一行，这样不断重复，直到文件末尾，文件内容并没有更改，除非使用重定向存储输出。
使用p命令时，这个命令特殊它先将一行数出屏幕在执行p命令，所以显示的行会是两倍，可以加-n去重

3.sed -r '3p;3d' passwd.txt = sed -r '3{p;d}' passwd.txt
p特殊，先输出了第三行，再由d命令操作了模式空间那行，所以屏幕中会有一行该行数据。
如果换成d;p,因为d在模式空间中删除了,p打印不了该行，所以屏幕不会有该行数据


4.删除第一次包含'cjq'的行到第四行：

> sed '/cjq/,4 d' test

如果在前四行没有匹配成功，就只删除第四行以后匹配'cjq'的行。

删除从第一次匹配'cjq'的行到最后一行：

> sed '/cjq/,$ d' test

删除从第一次匹配'cjq'的行 到匹配'aaa'之间的行：

> sed '/cjq/,/aaa/ d' test
>  
d参数会删除匹配的行，其余命令将不会作用在该行
> 
> 删除空行和注释:
> sed -r '/^$/d test.txt
> sed -r /^#/d test.txt

5.
在行前边加注释符
sed -r 's/(.*)/#\1/'
sed -r 's/^/#/'
![在这里插入图片描述](https://img-blog.csdnimg.cn/cd271a8afd3745bc9480d3dd78db4dbb.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/fec5708ae35e4ea7bc1799df5043640f.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

6.
```bash
[root@web ~]# sed r 's/^[a-z]/&1' datafile 
sed: can't read s/^[a-z]/&1: No such file or directory
aaaa99
god
good
goood
gooood
goooood
[root@web ~]# sed -r 's/[a-z]/&1' datafile 
sed: -e expression #1, char 10: unterminated `s' command
[root@web ~]# sed -r 's/[a-z]/&1/' datafile 
a1aaa99
g1od
g1ood
g1oood
g1ooood
g1oooood
[root@web ~]# 

```
&1，在匹配到的字符创后边添加，**注意往往需要/收回来**

7.
组字符和\1优先调用情况
![在这里插入图片描述](https://img-blog.csdnimg.cn/69935c83efd8406fa23a80167b06f62b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f1cf00a3c32348569b20e4e98201201d.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
7.\1\2\3使用案例

```bash
[root@web ~]# sed -r 's/[0-9]+$/&.5/' datafile 
aaaa99.5
god
good
goood
gooood
goooood
[root@web ~]# sed -r 's/(.)(.)(.*)/\1\1chen\3/' datafile 
aachenaa99
ggchend
ggchenod
ggchenood
ggchenoood
ggchenooood
[root@web ~]# sed -r 's/(.)(.)(.*)/\1\2chen\3/' datafile 
aachenaa99
gochend
gochenod
gochenood
gochenoood
gochenooood
[root@web ~]# sed -r 's/(.)(.)(.*)/\1chen\2\3/' datafile 
achenaaa99
gchenod
gchenood
gchenoood
gchenooood
gchenoooood
[root@web ~]# sed -r 's/(.)(.)(.*)cjq/\1\2chen\3/' datafile 
aaaa99
god
good
goood
gooood
goooood
[root@web ~]# sed -r 's/(.)(.)o(.*)/\1\2chen\3/' datafile 
aaaa99
god
gochend
gochenod
gochenood
gochenoood

```




&的使用
sed -r 's/.*/#&/' filename == sed -r 's/^/#&'/' filename
在&前一位着在前面添加，在后面反之
