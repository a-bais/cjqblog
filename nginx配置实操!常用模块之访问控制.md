---
title: nginx配置实操:常用模块之访问控制
date: 2021-09-23 22:14:03
tags: nginx
categories: nginx配置实操（yum方式安装）
---

<!--more-->

访问控制分两种:
1.基于ip的访问控制
2.基于用户认证的访问控制
常用位置是location

**基于ip的访问控制:**

```bash
 server{
  2   listen 80;
  3   server_name a1.com;
  4   location / {
  5     root /web/autoindex;
  6     index index.html;
  7   }
  8   location /download{
  9     root /web/autoindex;
 10     autoindex on;
 11     autoindex_exact_size on;
 12     autoindex_localtime on;
 13     charset utf-8,gbk;
 14     allow 192.168.160.147;			#允许一个ip访问
 15     allow 192.168.160.2;
 16     allow 192.168.100.0/24;			#允许一个网段访问
 17     deny all;
 18   }
 19   location /nginx_status {
 20     stub_status;
 21     access_log off;
 22     deny 192.168.160.188;			#拒绝一个ip访问
 23     allow all;						#允许所有
 24   }
 25 }

```
多个ip或网段就写多个deny或allow
被拒绝的ip访问时会显示为403 forbidden

**基于用户认证的访问控制:**

先试用htpasswd创建用于http服务认证用的文件
-b 直接在命令行输入用户和密码
-c 创建认证文件，不能对就文件使用-c，会覆盖原来内容（htpasswd -b -c 认证文件 用户名 密码)
-D 删除用户 (htpasswd -D 认证文件 用户名)
修改用户密码 （htpasswd 认证文件 用户名）
![在这里插入图片描述](https://img-blog.csdnimg.cn/2515804abe984446acb4cd5b7b67700f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash server{
  2   listen 80;
  3   server_name a1.com;
  4   location / {
  5     root /web/autoindex;
  6     index index.html;
  7   }
  8   location /download{
  9     root /web/autoindex;
 10     autoindex on;
 11     autoindex_exact_size on;
 12     autoindex_localtime on;
 13     charset utf-8,gbk;
 14     auth_basic "Please input the user name and password"; #这是提示内容，有些浏览器看不到，建议使用火狐
 15     auth_basic_user_file /etc/nginx/.auth_passwd.conf;	  #指定认证文件
 17   location /nginx_status {
 18     stub_status;
 19     access_log off;
 20     deny 192.168.160.188;
 21     allow all;
 22   }
 23 }


```

附:同一个location同时配置ip访问设置和用户认证访问控制会出错，会显示403 forbidden

