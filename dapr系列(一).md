---
title: dapr系列(一)
date: 2022-07-17 00:54:32
tags: 云原生 kubernetes dapr 分布式应用程序
categories: go和微服务零散
---

<!--more-->

@[TOC]
如果你学过istio，那么理解dapr会很快，这个专栏中有专门讲istio的博文
本片主要讲基础概念
# 概述
分布式应用程序运行时(distribute application runtime)
Dapr 是一个可移植的、事件驱动的运行时，它使任何开发人员都可以轻松构建在**云和边缘**上运行的弹性、无状态和有状态的应用程序，并包含语言和开发人员框架的多样性。

**任何语言，任何框架，任何地方**
![在这里插入图片描述](https://img-blog.csdnimg.cn/cced03643b9543c88fb1e4901d6ae421.png)
可以看出它是不限制任何语言的，跨语言，平台无关
业内正在经历一波云采用浪潮。开发人员对 Web + 数据库应用程序架构感到满意，例如经典的 3 层设计，但对本质上是分布式的微服务应用程序架构不满意。成为分布式系统专家很难。**开发人员希望专注于业务逻辑，同时依靠平台为他们的应用程序注入云原生架构的规模、弹性、可维护性、弹性和其他属性**

这就是 Dapr 的用武之地。Dapr 将构建微服务应用程序的最佳实践编入称为构建块的开放、独立 API，使开发者能够使用开发者选择的语言和框架构建可移植的应用程序。每个构建块都是完全独立的，您可以在应用程序中使用其中的一个、部分或全部。

使用 Dapr，开发者可以将现有应用程序逐步迁移到微服务架构，从而采用**云原生模式**，例如横向扩展/缩减、弹性和独立部署。

此外，Dapr 与平台无关，这意味着您可以在本地、任何 Kubernetes 集群、虚拟机或物理机以及与 Dapr 集成的其他托管环境中运行应用程序。开发者能够构建可以在云和边缘运行的微服务应用程序。

# 云和边缘的微服务构建块
![在这里插入图片描述](https://img-blog.csdnimg.cn/6be370033c0c426fa012e8aea753694c.png)
在构建微服务应用程序时有许多考虑因素。在构建微服务应用程序时，开发人员可以以标准方式使用并部署到任何环境Dapr 提供了通用功能的最佳实践。它通过提供分布式系统构建块来做到这一点。

这些构建块 API 中的每一个都是独立的，这意味着您可以在应用程序中使用它们中的一个、部分或全部。可以使用以下构建块：
![在这里插入图片描述](https://img-blog.csdnimg.cn/0757a828369c4cedb14b181b149adceb.png)
# sidecar架构
Dapr 将其 **HTTP 和 gRPC API** 公开为 sidecar 架构，既可以作为容器也可以作为进程，不需要应用程序代码包含任何 Dapr 运行时代码。这使得与 Dapr 的集成很容易与其他运行时集成，并提供应用程序逻辑的分离以提高可支持性。
(sidecar，envoy流行，其实就是讲通信的工作交给它们来完成)
![在这里插入图片描述](https://img-blog.csdnimg.cn/7f38dfecbe744c91a87e38d2fab3c0bf.png)
# 托管环境
Dapr 可以托管在多个环境中，包括自托管在 Windows/Linux/macOS 机器上用于本地开发，以及在 Kubernetes 或生产中的物理或虚拟机集群上。
## 自托管本地开发
在自托管模式下，Dapr 作为一个单独的 sidecar 进程运行(容器或者主机上的普通进程)，你的服务代码可以通过 HTTP 或 gRPC 调用该进程。**每个正在运行的服务都有一个 Dapr 运行时进程**（或 sidecar），它被配置为使用状态存储、发布/订阅、绑定组件和其他构建块。

可以使用Dapr CLI在本地计算机上运行启用 Dapr 的应用程序。下图显示了使用 CLI命令行客户端界面init命令配置Dapr 的本地开发环境。
![在这里插入图片描述](https://img-blog.csdnimg.cn/c36cda4ab97249abb86f560e57f6e0ae.png)
## Kubernetes
Kubernetes 可用于本地开发（例如minikube、k3S）或**生产环境**。在 Kubernetes 等容器托管环境中，Dapr 作为 sidecar 容器运行(本地是进程，容器本质也是进程)，应用程序容器在同一个 pod 中。

Dapr 有**控制平面服务**。`dapr-sidecar-injector`和`dapr-operator`服务提供一流的集成，在与服务容器相同的 pod 中将 Dapr 作为 sidecar 容器启动，并提供集群中配置的 Dapr 组件更新的通知。

该服务是一个**证书颁发机构**，可以在 Dapr sidecar 实例之间启用相互 **TLS** 以实现安全数据加密，并通过`Spiffedapr-sentry`提供身份。

在 Kubernetes 集群中部署和运行支持 Dapr 的应用程序就像在部署方案中添加一些注释一样简单。
![在这里插入图片描述](https://img-blog.csdnimg.cn/c267e15f06a84fceb0e0b204dcba910f.png)
## 物理或虚拟机集群
Dapr 控制平面服务可以以高可用性 (HA) 模式下部署到生产中的物理机或虚拟机集群，例如，如下图所示。这里 `ActorPlacement`和`Sentry`服务在三个不同的 VM 上启动，以提供 HA 控制平面。使用 DNS 为集群中运行的应用程序提供域名解析，Dapr 使用Hashicorp Consul 服务，也以 HA 模式运行。
![在这里插入图片描述](https://img-blog.csdnimg.cn/d95055a259ab482f843d8d4dab208628.png)
# 开发人员语言 SDK 和框架
Dapr 提供了各种 SDK 和框架，让您可以轻松地以您的首选语言开始使用 Dapr 进行开发。(Software Development Kit，软件开发工具包)
## Dapr SDK
为了让不同语言使用 Dapr 更加自然，它还包括特定语言的 SDK，用于：

Dapr SDK
为了让不同语言使用 Dapr 更加自然，它还包括特定语言的 SDK，用于：

```bash
C++
Go
Java
JavaScript
.NET
PHP
Python
Rust
```
这些 SDK 通过类型语言 API 公开 Dapr 构建块的功能，而不是调用 http/gRPC API。这使开发者能够用自己选择的语言编写无状态和有状态函数和参与者的组合。由于这些 SDK 共享 Dapr 运行时，你可以获得跨语言的表现和功能支持
## 开发者框架
Dapr 可以在任何开发者框架中使用。以下是与 Dapr 集成的一些内容：
## web
![在这里插入图片描述](https://img-blog.csdnimg.cn/c6a310b835cf4ba1a61563d91e54b205.png)

# 制造模块(building block)
可通过标准 HTTP 或 gRPC API 访问的模块化最佳实践
**构建块**是可以从你的代码中调用并使用一个或多个 Dapr 组件的 HTTP 或 gRPC API。

构建块解决了构建弹性微服务应用程序和编纂最佳实践和模式方面的常见挑战。**Dapr 由一组构建块组成**，具有添加新构建块的可扩展性。

下图显示了构建块如何公开从你的代码调用的公共 API(应用编程接口)，使用组件来实现构建块的功能。
![在这里插入图片描述](https://img-blog.csdnimg.cn/3e67d4d64b4642c3bbe34cf94cd3e9ab.png)
dapr提供的构建块
![在这里插入图片描述](https://img-blog.csdnimg.cn/45bc9ef1d51346519027d13b22db3282.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/5f3fb64908814cc3b0b4c574862c19fa.png)

# 组件
**被用于构建块和应用程序的模块化功能**
Dapr 使用模块化设计，将功能作为组件来提供。 每个组件都有接口定义。 所有组件都是可插拔的，因此你可以将组件换为另一个具有相同接口的组件。 你可以在 components contrib repo 为组件接口贡献实现并扩展 Dapr 功能。

构建块可以使用任何组件组合。 例如， actors 构建块和 状态管理 构建块都使用 状态组件。 另一个示例是 Pub/Sub 构建块使用 Pub/Sub 组件。

你可以使用 dapr components CLI 命令查看当前托管环境中可用组件的列表。
# 状态存储
状态存储组件是存储键值对的数据存储（数据库、文件、内存），其作为 状态管理 的构建模块之一。
1.状态存储列表
2.状态存储的实现
# 服务发现
服务发现组件与 服务调用 构建块配合使用，与托管环境集成以提供服务到服务发现。 例如， Kubernetes 服务发现组件与 Kubernetes DNS 服务集成，而自托管时使用 mDNS。
1.服务发现名称解析的实现
# 中间件
Dapr 允许将自定义 中间件 插入请求处理管道。 中间件可以在请求路由到用户代码之前，或者在将请求返回给客户端之前，对请求执行额外的操作（例如，认证，加密和消息转换）。 中间件组件与 服务调用 基础结构块一起使用。
1.中间件的实现
# Pub/Sub 代理
发布/订阅 组件是消息分发器，可以作为应用程序之间进行消息发布 & 订阅 构建块。
1.Pub/sub 支持的列表
2.发布/订阅 实现
# 绑定
绑定 构建块使得外部资源可以连接到 Dapr 以触发服务或作从应用触发外部服务。
1.支持的绑定列表
2.绑定实现
# Secret stores（密钥存储）
在 dapr 中，密钥 是指任何你不想给不受欢迎的人（例如：黑客）知道的私有信息。 密钥存储被用来存储可以在服务中被获取以及被使用的密钥
1.支持的密钥存储
2.密钥存储实现

# Configuration (配置)
变更 Dapr sidecars 或全局 Dapr 系统服务的行为
你可以在 Dapr 控制面板中更改 Dapr 全局系统服务配置，这些设置能够改变单个 Dapr 应用程序 sidecar。

配置定义和部署形式为 YAML 文件。 一个应用程序配置示例就像这样：

```bash
apiVersion: dapr.io/v1alpha1
kind: Configuration
metadata:
  name: daprConfig
  namespace: default
spec:
  tracing:
    samplingRate: "1"
    zipkin:
      endpointAddress: "http://localhost:9411/api/v2/spans"
```
这个配置文件配置了遥测录制跟踪。 你可以在自托管模式中通过编辑 .dapr 目录中名为 config.yaml 的配置文件进行加载；或者通过 kubectl/helm 应用到您的 Kubernetes 集群。

# 中间件管道
链式中间件组件的自定义处理管道
Dapr 允许通过链接一系列中间件组件来定义自定义处理管道。 请求在路由到用户代码之前经过所有已定义的中间件组件，然后在返回到客户机之前，按相反顺序经过已定义的中间件，如下图中所示。
![在这里插入图片描述](https://img-blog.csdnimg.cn/7d3a9bb53a294ba99d691e8b7e5361d2.png)
## 自定义处理管道
启动后， Dapr sidecar 会构建中间件处理管道。 默认情况下，管道由 追踪中间件 和 CORS 中间件组成。 其他中间件，由 Dapr configuration 配置，按照定义的顺序添加到管道中。 管道适用于所有 Dapr API 终结点，包括状态，发布/订阅，服务调用，绑定，安全性和其他。

> 注意： Dapr 提供 middleware.http.uppercase 预注册组件，该组件将请求正文中的所有文本更改为大写。你可以使用它来测试/验证自定义管道是否已就绪。

以下配置示例定义了使用 OAuth 2.0 中间件 和大写中间件组件的自定义管道。 在这种情况下，在转发到用户代码之前，所有请求都将通过 OAuth 2.0 协议进行授权，并转换为大写文本。(联想到了webhook)

```bash
apiVersion: dapr.io/v1alpha1
kind: Configuration
metadata:
  name: pipeline
  namespace: default
spec:
  httpPipeline:
    handlers:
    - name: oauth2
      type: middleware.http.oauth2
    - name: uppercase
      type: middleware.http.uppercase
```
# 可观测性
如何通过跟踪、度量、日志和健康状况来监控应用程序
在构建应用程序时，了解系统如何运行是运维的一个重要部分——这包括有能力观测应用程序的内部调用，评估其性能并在发生问题时立即意识到问题。 这对任何系统都是一种挑战，而对于由**多个微服务组成的分布式系统**来说更是如此，其中由多个调用组成的流程，可以在一个微服务中启动，然后在另一个微服务中继续。 可观察性在生产环境中至关重要，但在开发期间也很有用，可以了解瓶颈所在，提高性能并在整个微服务范围内进行基本的调试。

虽然可以从底层基础设施层收集有关应用程序的一些数据( 例如内存消耗， CPU 使用率) ，但其他有意义的信息必须从 “应用程序感知 " 层收集，比如用于显示一系列调用如何跨微服务执行的信息。 这通常意味着开发人员必须添加一些代码来用于此目的的应用程序。 通常，检测代码只是将收集的数据 ( 例如跟踪（traces）和度量（metrics）） 发送到外部监视工具或服务，由这些工具或服务来帮助存储，可视化和分析所有这些信息。

这是开发者的额外负担：必须维护此代码，即使它不属于应用程序核心逻辑的一部分，甚至有时还需要了解监视工具 API，使用额外的 SDK 等操作。 此检测需求可能会增加应用程序的可移植性难度，比如在不同地方部署时，该应用程序可能需要不同的检测代码。 例如，不同的云提供者提供不同的监控解决方案，而在本地部署中可能要求一个本地解决方案。
## 通过 Dapr 进行观测
在利用 Dapr 构建块来执行服务到服务调用和 pub/sub 消息传递构建应用程序时， Dapr 拥有相对于 **distributed tracing** 的优势，因为此服务间通信全部流经 Dapr sidecar，sidecar 处于这样独特的位置，可以消除应用程序级别检测的负担。
### 分布式跟踪
Dapr 可以配置发送跟踪数据，并且由于 Dapr 使用广泛采用的协议（如 **Zipkin** 协议）进行跟踪，因此可以轻松地集成多个监控后端。
![在这里插入图片描述](https://img-blog.csdnimg.cn/5df888ec2b0842fdbd1be8b644d30f64.png)
### OpenTelemetry 采集器
Dapr 还可以通过配置来使用 `OpenTelemetry Collector`(开放遥测采集) ，它会提供更多与外部监控工具的兼容性。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d32b73fce5c5494397907974df511d82.png)
### 跟踪上下文
Dapr 使用 W3C 跟踪 规范来跟踪上下文，并可以生成和传播上下文头本身或传播用户提供的上下文头。

## Dapr sidecar 和系统服务的可观察性
至于系统的其他部分，你希望能够观察 Dapr 本身，并收集 Dapr sidecar 沿每个微服务以及你环境中的 Dapr 相关服务（如部署在 Dapr 启用的 Kubernetes 集群中的控制面板服务）发出的指标和日志。
![在这里插入图片描述](https://img-blog.csdnimg.cn/57006286e50d49149efaf31fc40f350c.png)
## 日志
Dapr 生成 日志，以提供 sidecar 操作的可见性，并帮助用户识别问题并执行调试。 日志事件包含由 Dapr 系统服务生成的警告，错误，信息和调试消息。 Dapr 还可以通过配置将日志发送到收集器，例如 Fluentd 和 Azure Monitor ，这样就可以轻松搜索，分析和提供洞察。

## 度量
指标（Metrics）是在一段时间内收集和存储的一系列度量值和计数。 Dapr 指标 提供监控功能，以了解 Dapr sidecar 和系统服务的行为。 例如，Dapr sidecar 和用户应用之间的服务指标可以展示调用延迟、流量故障、请求的错误率等。 Dapr 的系统服务度量 则可以显示 sidecar 注入失败，系统服务的运行状况 ( 包括 CPU 使用率，actor 位置数量等) 。

## 健康状态
Dapr sidecar 暴露了 健康检查的 HTTP 终结点。 通过此终结点，可以探测 Dapr 进程或 sidecar，以确定它的准备度和活跃度，并采取相应的行动。

# 安全
上述几个领域是通过对传输中的数据进行加密解决的。 Dapr 用于加密传输中数据的安全机制之一是 相互认证（mutual authentication）TLS 或简写为 mTLS。 mTLS 为应用程序内的网络流量提供了一些关键功能：
1.双向身份验证 - 客户端向服务器证明其身份，反之亦然
2.建立双向认证后，所有进行中通信都走加密通道

在几乎所有场景中，相互 TLS 都很有用，尤其是对于受法规约束的系统，例如 HIPAA 和 PCI。

Dapr 支持 mTLS 和本文涉及的应用程序中的所有功能，在生产系统中几乎不需要额外的代码或复杂配置。
## Sidecar与应用程序之间的通信
Dapr sidecar通过 localhost 运行在应用程序附近，建议在与应用程序相同的网络边界下运行。 尽管如今许多云原生系统将 Pod 级别（例如 Kubernetes 上）视为可信任的安全边界，但 Dapr 还是可以为用户提供使用令牌的 API 级别身份验证。 此功能保证即使在 localhost 上，也只有经过身份验证的调用方才能调用 Dapr。
## Sidecar之间的通信
Dapr 包括一个"默认开启"，自动相互 TLS，为 Dapr sidecar之间的流量提供传输加密。 为此，Dapr 利用名为 **Sentry** 的系统服务，该服务充当证书颁发机构 （Certificate Authority/CA），并为来自 Dapr sidecar的工作负载 （app） 签署证书请求。

Dapr 还管理工作负载证书轮换，并且这样做时应用程序不会停机。

除非用户提供了现有的根证书，否则，作为 CA 服务的 Sentry 会自动创建并持有自签名根证书，有效期为一年。

更换根证书（Kubernetes 模式下的 secret 和自托管模式的文件系统）时，Sentry 会提取它们并重新构建信任链，而无需重新启动，而 Sentry 的停机时间为零。(也就是自动签证)

当新的 Dapr sidecar 初始化时，它首先检查 mTLS 是否启用。 如果是，则生成 ECDSA 私钥和证书签名请求，然后通过 gRPC 接口发送到 Sentry。 Dapr sidecar 和 Sentry 之间的通信使用信任链证书进行身份验证，该证书由 Dapr Sidecar Injector 系统服务注入到每个 Dapr 实例中。

在 Kubernetes 集群中，保存根证书的密钥的范围是 Dapr 组件部署所在的命名空间，并且只有 Dapr 系统 pod才能访问。

在 Kubernetes 上部署时，Dapr 还支持强标识，它依赖于Pod 的 Service Account 令牌，而这个令牌会作为证书签名请求 （CSR） 的一部分发送到 Sentry。

默认情况下，工作负荷证书的有效期为 24 小时，时钟偏差设置为 15 分钟。

编辑与 Dapr 一起部署的默认配置中的 spec.mtls.enabled 字段，可以关闭/开启相互TLS。 这既可用于 Kubernetes 模式，也可以用于自托管模式。
### 自托管中的 mTLS
下图显示了 Sentry 系统服务如何根据运维人员提供或由 Sentry 服务生成的根证书/颁发者证书（这些证书存储在文件中）为应用程序颁发证书。
![在这里插入图片描述](https://img-blog.csdnimg.cn/ca9bd6925f8444408f68df8b59107e91.png)
### kubernetes 中的 mTLS
下图显示了 Sentry 系统服务如何根据运维人员提供的，或者由 Sentry 服务生成（存储为 Kubernetes sucret ）的根证书/颁发者证书为应用程序颁发证书。
![在这里插入图片描述](https://img-blog.csdnimg.cn/75f901037e94435ab4137c4f0a4627a5.png)
## Sidecar与系统服务之间的通信
除了 Dapr Sidecar 之间的自动 mTLS 之外，Dapr 还提供 Dapr sidecar 和 Dapr 系统服务之间的强制性 mTLS，这些系统服务包括 Sentry 服务（证书颁发机构）、 Placement 服务（Actor安置）和 Kubernetes Operator。

启用 mTLS 时， Sentry 将根证书和颁发者证书写入 Kubernetes secret，该密钥的作用域限定为部署控制平面的名称空间。 在自托管模式下，Sentry 将证书写入可配置的文件系统路径下。

在 Kubernetes 中，当 Dapr 系统服务启动时，它们会自动装载包含根证书和颁发证书的 secret，并使用这些secret 来加固 Dapr sidecar 使用的 gRPC 服务器。

在自托管模式下，每个系统服务都可以装载文件系统路径以获取证书。

当 Dapr sidecar 初始化时，它使用挂载的叶证书和颁发者私钥对系统 pod 进行身份验证。 这些作为环境变量挂载在 sidecar 容器上。

### Kubernetes 中系统服务的 mTLS
下图显示了 Dapr Sidecar 与 Dapr Sentry（证书颁发机构）、Placement（Actor 安置）和 Kubernetes Operator 系统服务之间的安全通信
![在这里插入图片描述](https://img-blog.csdnimg.cn/c0c2ece7ba114c74926981707b1d2210.png)
# 组件命名空间的作用域和密钥
Dapr 组件是受限于命名空间的。 这意味着 Dapr runtime sidecar 的实例只能访问部署到同一命名空间的组件。 
Dapr 组件使用 Dapr 的内置密钥管理功能来管理密钥。 
此外，Dapr 通过允许用户指定哪些应用程序可以使用给定组件，为组件提供应用程序级的范围限制。

# 网络安全
你可以采用常见的网络安全技术，如网络安全组 （NSG）、非军事区 （DMZ） 和防火墙，以便为您的网络资源提供层层保护。
例如，除非配置为与外部绑定目标通讯，否则 Dapr sidecar 不会打开到 Internet 的连接。 而大多数绑定实现仅使用出站连接。 你可以设计防火墙规则，只允许通过指定的端口进行出站连接。

# 绑定安全性
具有绑定目标的身份验证由绑定的配置文件配置。 通常，应配置所需的最低访问权限。 例如，如果仅从绑定目标读取，则应配置绑定以使用具有只读访问权限的帐户。

# 状态存储安全
Dapr 不会转换来自应用程序的状态数据。 这意味着 Dapr 不会尝试加密/解密状态数据。 但是，您的应用程序可以采用您选择的加密/解密方法，而且状态数据对 Dapr 保持不透明。
Dapr 不存储任何数据。
Dapr 使用配置的身份验证方法来与底层状态存储进行身份验证。 许多状态存储实现都使用官方客户端库，这些客户端库通常使用安全通信通道和服务器通讯。

# 管理安全
在 Kubernetes 上部署时，你可以使用 Kubernetes RBAC 控制对管理活动的访问。

在 Azure Kubernetes Service （AKS） 上部署时，可以使用 Azure Active Directory （AD） 服务主体 控制对管理活动和资源管理的访问。

# 威胁模型
威胁建模是一个过程，通过该过程可以识别、枚举潜在威胁（如结构漏洞或缺乏适当的安全措施），并确定缓解措施的优先级。 Dapr 威胁模型如下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/8c3e0d87e2fa4c2683adef2a61f3898b.png)
# Dapr文件中通用术语和缩略语的定义
![在这里插入图片描述](https://img-blog.csdnimg.cn/a9ac9f7faf8642a1afbc97000b8d64c4.png)
# 解答一些冲突混淆的概念
1.Dapr 与 Istio 、Linkerd 或 OSM 等服务网格相比如何？
**Dapr 不是一个服务网格**。 服务网格侧重于细粒度网络控制，但 Dapr **专注于帮助开发人员构建分布式应用程序**。 Dapr 和服务网格都使用 sidecar 模式，并随应用程序一起运行，它们确实具有一些重叠的功能，但也提供独特的优势。 

2.如果我想使用特定的编程语言或框架，Dapr是否有任何语言的SDK？
为了使不同语言使用 Dapr 更加自然，它包括 特定语言的 SDK 用于 Go、Java、JavaScript、.NET、Python、PHP、Rust 和C++。
这些 SDK 通过类型化的语言 API 而不是通过调用 API 来使用 Dapr 构建块中的功能，例如，保存状态，发布事件或创建Actor。 这使您能够以自己选择的语言编写无状态和有状态功能和 actors 的组合。 由于这些 SDK 共享 Dapr 运行时，因此您可以获得跨语言 actor 和功能支持。


