---
title: k8s集群配置了软路由，其他的master和node节点上不了的问题
date: 2021-10-26 15:47:25
tags: k8s
categories: k8s
---

<!--more-->

这里我的软路由采用国内开源的koolshare实现，刚接触有个问题就是master跟node都ping不上百度，
回想起master和node网卡模式设置的都是仅主机模式，于是问题就定位到了koolshare软路由上，发现koolshare机器我关了，**开启koolshare即可**

附:虚拟机的仅主机模式上网有问题，一般上网会设置nat模式(koolshare使用了仅主机和nat两种),非网络方向学生所以能大概定位到问题出在那，但不清楚其中的底层原理
