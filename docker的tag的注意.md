---
title: docker的tag的注意
date: 2022-01-05 17:26:30
tags: docker 容器 运维
categories: docker学习笔记
---

<!--more-->

一：
docker tag
如果心得镜像你不指定版本会是默认的latest

二：
docker tag
官方解释是硬链接

往往会联想到文件系统的硬链接，也就是node和block的指定关系，按照文件系统的硬链接的特性是原链接删除后硬链接正常使用，但是实操发现，这在容器中是不一样的

比如有两个镜像，一个原镜像，一个是硬链接生成的镜像，都可以正常使用

当docker rmi删除原镜像后，硬链接的镜像无法正常使用，就连删除都要用docker rmi -f，与常规的硬链接理解不同。

所以最好将tag理解成打标签。
