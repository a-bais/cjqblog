---
title: 记一个由于改docker引起的kubernetes的错误
date: 2022-01-03 22:45:48
tags: docker kubernetes 容器
categories: 解决错误问题
---

<!--more-->

刚开始装docker时，根目录是默认的，/var/lib/docker，主机用的东西越来越多就在/etc/docker/daemon.json下用graph指定新的目录来存放docker数据，这是之前用的好好的kubernetes直接报错了，问题大概定位于其中的使用中的容器，将docker跟目录更改回来错误解除

猜测：将原本的跟目录数据移到新目录下或许也能解决问题

对于一个使用中的结构，底下的部署，在未详细理解之前不要轻易改动
