---
title: nginx安装（yum方式）
date: 2021-09-27 06:51:36
tags: nginx 运维 linux
categories: 常用软件的安装
---

<!--more-->

nignx也可以直接使用epel源安装（epel安装的功能一般不够完善）
下图是nginx的官方源:
![在这里插入图片描述](https://img-blog.csdnimg.cn/d7997fc750ab4c968e10bc80e134d28c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/a65e382ff3664a019ba0a953e2d27d70.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/3cbf152dae874495bdf2e53589f80617.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6184c15b4c864399a61e56c1c99baa72.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
复制黏贴到/etc/yum.repos.d/nginx.repo下即可yum下载安装nginx了
