---
title: yum remove后配置文件及配置等还在
date: 2021-08-06 10:47:24
tags: 
categories: 解决错误问题
---

<!--more-->

yum安装好了的nginx,又yumremove之后，会在/etc/nginx/下留有conf.d/和default.d/和nginx.conf.rpmsave(这里边应该保留了原有配置)，其中最值得注意的是conf.d/下的我们自定义的文件还在

附加:yum中，@符号往往表示已安装的软件包，或者安装软件包组时用上如'@Development tools'
