---
title: 使用harbor时的注意点
date: 2021-10-21 21:01:25
tags: harbor docker kubernetes
categories: docker学习笔记 k8s
---

<!--more-->

1![在这里插入图片描述](https://img-blog.csdnimg.cn/92e9b9e0a7ac4b5e9abf6fcc9737dde0.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
这是admin的密码，192.168.160.149:8089可以访问harbor，登录用户填admin

8089可以用来访问harbor,
![在这里插入图片描述](https://img-blog.csdnimg.cn/e05bbafac981473caa543b0fc95c9165.png)
docker-proxy在k8s-node上，用作端口映射，即将用户访问机器的端口映射到容器中

2.
在部署harbor时，运行安装脚本时提示
ERROR: Failed to Setup IP tables: Unable to enable SKIP DNAT rule: (iptables failed: iptables --wait -t nat -I DOCKER -i br-9843b23730bd -j RETURN: iptables: No chain/target/match by that name.
(exit status 1))

解决方法往往是systemctl restart docker即可
