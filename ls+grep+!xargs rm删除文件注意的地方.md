---
title: ls+grep+|xargs rm删除文件注意的地方
date: 2021-12-27 18:04:20
tags: linux 运维 服务器
categories: 解决错误问题
---

<!--more-->

平时用|xargs和rm一块删文件不多，今天用了一下，发现一个问题，就是|xargs处理后传递给rm的数据我觉得应该是一行数据，也就是说不能分行
如ls -l | grep -v 'serverfile.*' | xargs rm -rf，错误
正确的写法要去掉参数-l
