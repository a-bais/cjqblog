---
title: nginx配置实操:常用模块之状态监控
date: 2021-09-23 20:23:41
tags: nginx
categories: nginx配置实操（yum方式安装）
---

<!--more-->

状态监控配置区域有server和location（最常用）

```bash
vim /etc/nginx/conf.d/autoindex.html

server{
        listen 80;
        server_name a1.com;
        location / {
                root /web/autoindex;
                index index.html;
        }
        location /download{
                root /web/autoindex;
                autoindex on;
                autoindex_exact_size on;
                autoindex_localtime on;
                charset utf-8,gbk;
        }
        location /nginx_status {                      #这里必须是nginx_status
                stub_status;						  #开启监控服务，老版本还需要价格on
                access_log off;						  #必须写，表示此页面的访问记录不写入日志文件
        }
}

```

![在这里插入图片描述](https://img-blog.csdnimg.cn/f1681388d145483abcc53b195e5d9a3b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
附:restart会清空左右连接数，reload不会
