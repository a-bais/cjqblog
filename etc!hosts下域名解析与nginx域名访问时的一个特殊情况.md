---
title: /etc/hosts下域名解析与nginx域名访问时的一个特殊情况
date: 2021-10-01 20:41:19
tags: nginx
categories: nginx排错
---

<!--more-->

比如/etc/hosts下
192.168.160.100 a1.com a2.com

然后a1.com做了准确的配置，网站正常，a2.com没有做成一个独立的站点，域名访问a2.com是会跳转到a1.com站点
