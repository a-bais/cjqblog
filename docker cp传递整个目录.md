---
title: docker cp传递整个目录
date: 2021-08-29 12:00:27
tags: docker nginx 运维
categories: docker学习笔记
---

<!--more-->

本机的/web/teset_nginx/下有index.html文件
docker cp **/web/test_nginx/** test_nginx:/web/
将整个/web/test_nginx/复制到容器test_nginx下的/web里边
