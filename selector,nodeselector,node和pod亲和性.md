---
title: selector,nodeselector,node和pod亲和性
date: 2022-03-17 20:14:49
tags: kubernetes docker 容器
categories: k8s
---

<!--more-->

**selector:**

```bash
selector:
  matchLables:
    app: nginx
```

```bash
selector:
  matchExpressions:
  - key: app
    operator: In
    values:
    - nginx
 
----------
selector:
  matchExpressions:
  - key: app
    operator: Exists:

```
(In,NotIn,Exists,DoesNotExist,Gt,Lt)

**nodeselector:**
  

```bash
nodeselector:
    label: value(节点标签)
```
**node亲和性：**

```bash
spec:   #pod的spec下
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
          name: nginxweb
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:  # 硬策略
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/hostname
                operator: NotIn
                values:
                - master1
          preferredDuringSchedulingIgnoredDuringExecution:  # 软策略
          - weight: 1   #权重
            preference:
              matchExpressions:
              - key: com
                operator: In
                values:
                - youdianzhishi
```
先满足硬策略，在**尽量**满足软策略（node和pod亲和性，pod反亲和性，硬的不满足都是不能调度，一直pending状态，因为没有所谓的可调度节点，期间一直寻找可调度节点）
**nodeSelectorTerms下满足任意一个即可，matchExpressions底下如果有多个匹配规则，则要全部满足。**


**pod:**

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pod-affinity
  labels:
    app: pod-affinity
spec:
  replicas: 3
  selector:
    matchLabels:
      app: pod-affinity
  template:
    metadata:
      labels:
        app: pod-affinity
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
          name: nginxweb
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:  # 硬策略
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - busybox-pod
            topologyKey: kubernetes.io/hostname   #拓扑域

---apiVersion: apps/v1
kind: Deployment
metadata:
  name: pod-antiaffinity
  labels:
    app: pod-antiaffinity
spec:
  replicas: 3
  selector:
    matchLabels:
      app: pod-antiaffinity
  template:
    metadata:
      labels:
        app: pod-antiaffinity
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
          name: nginxweb
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:  # 硬策略
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - busybox-pod
            topologyKey: kubernetes.io/hostname

```

