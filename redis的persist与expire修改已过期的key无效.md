---
title: redis的persist与expire修改已过期的key无效
date: 2021-09-14 21:31:23
tags: redis 数据库
categories: 解决错误问题 数据库mysql mariadb redis
---

<!--more-->

persist用来设置某个key永不过期，但是这个key要求是有效的key即该key还**未过期**，
persist修改过期的key是无效的
expire修改过期的key也是无效的
