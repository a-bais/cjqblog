---
title: 浅谈linux文件系统篇1：inode，区块基础知识等
date: 2020-07-18 18:46:22
tags: 操作系统 linux 运维 centos
categories: 零散
---

<!--more-->


讲文件系统前我们先聊聊 **分区**

众所周知，过去常用的分区方式是MBR分区表，但近年来MBR逐渐淘汰，取而代之的是CPT分区表格式。
分区名：分区可以看成磁盘分出的一部分，我们都知道，在linux上，所有设备都用文件名来表示，，磁盘也不例外。**物理磁盘**文件名：/dev/sd【a-p】；**虚拟磁盘**：/dev/vda【a-q】。
取你的第一个物理磁盘/dev/sda来讲,那么在该磁盘上面的第一个分区为/dev/vda1.


**如何让你的虚拟机使用GPT分区格式**!![在这里插入图片描述](https://img-blog.csdnimg.cn/20200718151849852.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
第一步：讲光标移动到**Install CentOS 7**
第二步：按下键盘的**tab**键，光标会移动到界面下方让你输入额外内核参数
第三部：（注意，各参数间要有空格，所以输入后面的参数时你要敲击一个空格符）输入**inst.gpt**
然后回车即可。
这样就可以强制系统使用这个很好用的GPT分区格式了


**分区格式化得到文件系统**

分区可以被挂载吗？答案是不能，能被挂载的是文件系统。
文件系统中会将一份数据分成两部分，**一部分是文件的各项权限与属性放在inode，另外一部分是实际数据放在区块中（block）中**，此外，还会有个超级区块用来记录整个文件系统的整体信息。
inode：文件的属性，文件实际数据所处区块的号码（一个文件只能够有一个inode）
block：实际数据存放的地方
超级区块：记录文件系统的信息，比如inode block 的使用量，剩余量，文件系统格式等（此处格式为gpt）
inode与block在建立文件系统大小已经确定，若想改变它们的大小只能重新格式化该文件系统。
**一个文件只能占用一个inode，一个区块只能存放一个文件**
那么一个文件比区块小得话，没办法，只能放在一个区块中（一个够用了），这样就会出现所谓的磁盘空间浪费。
若果文件比区块大呢，该文件便占用多个区块咯。
inode。记录文件的：读写属性
									拥有者和用户组
									文件的大小
									mtine（最近一次的修改时间）
									ctime（建立与状态改变时间）
									atime（最近一次读取时间）
									等等
									
**系统读取一个文件时，会到文件对应的inode分析文件权限等资料看与当前用户是否符合，符合猜到inode所记录的区块中读取文件的实际数据**
如果一个文件特别大，一个inode不够用，之二是便可以拿一个或几个区块来作为记录区块号码的记录区块。

**文件系统的运行**
都知道，cpu处理的数据都来自内存，我们编辑一个文件时，若它随时将硬盘对应的数据进行更新（即将新数据实时写回硬盘），但因为硬盘读写慢，所以这样的方式比较耗时间，效率低，yincilinux对此情况采用**异步处理方式**：将文件从硬盘加载到内存中供我们编辑，如果某些文件没有被编辑过，就会被设置为【clean】，编辑过便设置为【dirty】，系统会不定时将【dirty】的文件写会硬盘。当然，你也可以任意时刻使用sync指令来收手动强制写入硬盘。
sync这个指令，当你是正常关机时系统会主动调用该指令。

**挂载点**
文件系统需要挂载后才能被使用，挂载便是将文件系统与某个目录结合，该目录就称为挂载点，是进入文件系统的入口。![在这里插入图片描述](https://img-blog.csdnimg.cn/2020071818230561.png)
观察，这两个文件都占用64这个inode（一个文件只能占用一个inode），但是除了文件名之外，文件的权限和连接数和拥有者和用户组以及修改或者创建时间都不一致，可看出他们是不同的文件系统。



![在这里插入图片描述](https://img-blog.csdnimg.cn/20200718182811943.png)
观察，这个除了文件名一切都一样的，这两个就是相同的文件。

**Virtual Filesystem Switch（VFS）**
都说linux系统在使用文件系统，那么linux通过什么来使用文件系统呢？就是VFS，linux识别的文件系统都有VFS来管理。
查询自己的linux支持的文件系统有哪些：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200718183749133.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
查询支持的文件系统中有哪些已经加载到内存中：
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020071818395620.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
后续发布浅谈文件系统篇2：xfs。
近年来xfs比较有市场，有必要与大家共同学习下这个xfs文件系统
