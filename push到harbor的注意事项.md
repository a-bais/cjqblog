---
title: push到harbor的注意事项
date: 2021-10-23 11:10:54
tags: docker 容器 运维
categories: docker学习笔记 k8s
---

<!--more-->

先在/etx/docker/daemon.json设置好私有仓库，这里最好设置了端口如
![在这里插入图片描述](https://img-blog.csdnimg.cn/806a5540c0dc4e5ebe23708e7206dd1f.png)
登录上这个私有仓库
docker login 192.168.160.149:8089(指定端口)
![在这里插入图片描述](https://img-blog.csdnimg.cn/8dd9150aafd54dd2abec511a6d6ba8d4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
在harbor建立一个项目了，叫harbor,这里是它的打标签和推送命令，按这两条命令里作即可上传成功

**错误问题总结：**
1.Error response from daemon: Get "https://192.168.160.149/v2/": dial tcp 192.168.160.149:443: connect: connection refused
登录docker login时可能会有这个错误，如docker login 192.168.160.149,docker login默认走的是443端口，而我们设置的是8089端口，解决方法就是指定端口8089，与/etc/docker/daemon.json设置的一致，

另外，如果/etc/dockerdaemon.json中只写ip不指定端口，那么docker login ip是可以登录成功的，但是docker login走的是443即https端口，而harbor页面是8089端口，因此在后续的docker push会出现因为端口不一致出现错误，原理应该是docker login获取的token,报错记得是像**http 405**这样，总之，对于私有仓库，不论/etc/docker/daemon.json还是docker login，**写明端口**

2.denied: requested access to the resource is denied
push时遇到这样的错误，很简单，还没登录私有仓库，先docker login ip:port

3.`error parsing HTTP 405 response body: invalid character '<' looking for beginning of value: "<html>\r\n<head><title>405 Not Allowed</title></head>\r\n<body>\r\n<center><h1>405 Not Allowed</h1></center>\r\n<hr><center>nginx/1.20.1</center>\r\n</body>\r\n</html>\r\n"`
往往就是想1所说的，dcoker login和docker push走的端口不一致

4.Get "http://192.168.160.149:5000/v2/": dial tcp 192.168.160.149:5000: connect: connection refused
一般是/etc/docker/daemon.json的insecure-registries没有设置这个ip及端口

**附上几个文件的大致配置：**

**/etc/docker/daemon.json:**
![在这里插入图片描述](https://img-blog.csdnimg.cn/5c5ebaaa5c1f4392a1ad0345e19064c0.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
修改后要systemctl restart docker
___________________
**~/harbor/harbor.yml:**
![在这里插入图片描述](https://img-blog.csdnimg.cn/91e2626cd1bb4656ab98d6ac18b576f6.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
修改后一般systemctl restart docker即可
_________________________________________________________________________________
**~/harbor/docker-compose.yml:**
![在这里插入图片描述](https://img-blog.csdnimg.cn/f9a930692d544908a0ebf782fbbdfdf7.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
将容器上暴露的8089端口映射到主机端口8080
修改后一般要docker-compose stop
./install.sh
