---
title: vim解决中文乱码问题
date: 2021-07-22 22:57:53
tags: vim linux
categories: vim常用配置与问题解决
---

<!--more-->

情况1：在centOS或debian等Unix系统上，使用vim编辑文件时，输入中文时，中文乱码：
#vim /root/.vimrc输入如下配置：syntax onset fileencodings=ucs-bom,utf-8,utf-16,gbk,big5,gb18030,latin1set fileencoding=utf-8:wq再次vim编辑时OK。解释下：syntax on表示语法高亮；fileencoding表示保存文件时的默认文件编码；fileencodings表示vim读取文件时，采用的编码识别序列，从左往右匹配。

情况2：在windows上编写的文件，上传至Linux上，vim编辑时乱码。原因是windows默认使用gbk编码，在Linux上，使用iconv命令可以转码：#iconv -f GBK -t UTF-8 test.txt -o test2.txt再次vim编辑时OK。

情况3：在secureCRT或xshell等SSH工具上，vim编辑文件时乱码。这个情况很办好：修改客户端的编码格式即可。以secureCRT为例：找到options---session options---terminal---Appearance，把charset encoding改为utf-8就好了。
版权声明：本文为博主原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接和本声明。
本文链接：https://blog.csdn.net/misakaqunianxiatian/article/details/52263564
————————————————
版权声明：本文为CSDN博主「misaka去年夏天」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/misakaqunianxiatian/article/details/52263564


