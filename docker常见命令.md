---
title: docker常见命令
date: 2021-08-15 11:31:53
tags: docker 运维 linux
categories: 重点实操记录 docker学习笔记
---

<!--more-->

```bash
docker search nginx

NAME:镜像名称，一般名称前没有/表示是官方的
DECRIPTOPN:镜像描述
STARTS:点赞数目
OFFICCIAL:是否官方
AUTOMATED:是否自动安装

镜像优先考虑官方的和stars数量多的
```

```bash
获取镜像

docker pull httpd:2.2
```

```bash
设置镜像加速 
vim /etc/docker/daemon.json
{
	"registry-mirrors":["https://registry.docker-cn.com"]
}
systemctl restart docker
time docker pull httpd:2.2查看速度
```

```bash
查看镜像

docker images ls 列出所有镜像
docker imagess 列出所有镜像
docker images nginx:latest 列出对应镜像

REPOSITORY:镜像名称
TAG:版本
IMAGE ID：镜像id
CREATED:创建时间
SIZE:大小
```

```bash
删除镜像

docker image rm nginx:latest
docker image rm nginx:latest --force（--force是强制删除，一般用于删除镜像时但该镜像已经开启）（镜像文件一般不可以修改，在镜像文件上层生成个容器,属于可写层，容器才可以修改，镜像文件一般是只读层）
```

```bash
备份镜像

docker save -o /tar/nginx_latest.tar nginx:latest（建议使用这个）
docker save nginx:latesst > docker_nginx_latest.tar.gz
```

```bash
恢复镜像

docker load -i /tar/nginx_latest.tar
docker image ls 查看下镜像
```

```bash
镜像硬链接

docker image tag 原镜像 新镜像
docker image tag httpd:2.2 http_test
docker image ls查看下镜像
docker image rm httpd:2.2 删除后新镜像依然在，硬链接
```

```bash
查看命令的帮助信息

docker help
docker 参数 --help
docker help 参数(建议使用这个)

docker help image
docker help run
docker help search
```

```bash
运行容器

docker run 选项 容器名 CMD
指定端口映射的-p可以多次使用，如-p 80:80 -p 21:21
同一个镜像可以多次运行生成多个容器，并且容器的配置文件和容器id和容器地址都不同
每**启动**一个容器默认会多一个veth网卡，docker默认的网络模型是Bridge，桥接模式

docker run -d -p 80:80 httpd:2.4
docker run -it --name httpd httpd:2.4 /bin/bash
-d后台运行
-p端口映射
-it分配个交互式终端，这里是/bin/bash
--name指定容器名称

不进入容器内部世界运行命令
docker run -it httpd:2.4 ps -ef
注意ps -ef 会覆盖了容器的初始命令，就是dockerfile创建镜像时指定的CMD。个人不建议初次使用这个容器使用这种写法
不指定容器名称会生成个随机名称
docker run == docker create + docker start
```

```bash
查看容器列表

对镜像进行操作例如启动镜像或者对镜像输入命令才会产生容器，导入镜像文件不会产生容器

docker ps -a 显示所有容器
docker ps -l 显示最近启动的一个容器
docker ps -q 只打印容器id
-q一般与a或l一块用
docker container ls 只查看运行中的容器
docker container ls -a 查案所有容器

COBTAINER ID 容器id
INAGE 镜像名称
COMMAND 运行的命令
CREATED 何时开启的容器
STATUS 多长时间将容器打开
PORTS 端口
NAMES 容器名称

使用docker run时，有可能会出现错误，那这时候用docker ps -a看到的容器的stat是create,就只是单纯的创建
```

```bash
删除一个或多个容器

删除容器会同时删除容器里边的数据
docker container rm 容器名1 容器名2 容器id
docker ps -aq | xargs docker stop批量删除容器前先停止容器
docker ps -a -q |xargs docker rm

```

```bash
关闭，杀死，启动，重启容器
docker container [start|kill|stop|restart] 容器名称
container可有可无

docker stop nginx
docker kill 容器id
docker start nginx
docker restart nginx
docker start -i nginx 进入交互式
杀死进程用docker ps -a查看STATUS的Exited()是非0值
```

```bash
容器改名

docker container rename oldname newname
```

```bash
容器与宿主机文件互传

因为容器中很多命令为下载，所以有时我们会将文件传到宿主机修改后传到容器。不过我一般用挂载文件的方法，后面会讲
docker container cp 容器id:文件路径 本地路径
docker container cp 本地路径 容器id:文件路径

先docker run -it nginx /bin/bash进入容器然后找出文件路径
```

```bash
进入容器

容器建立后一般不会动了，要配置时其实不常用docker run -it进入
进入方式一般还有两种:

attach
docker attch 容器id或名称
不管多少人进入都是同一个终端，输入命令是同步的，有人退出则都退出，容器随之死亡
不建议使用

exec
docker exec -it 容器id或名称 /bin/bash
分配一个新的终端

```

```bash
查看docker容器运行过程中产生的日志

docker logs 容器名称
```

