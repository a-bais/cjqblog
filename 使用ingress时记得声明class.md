---
title: 使用ingress时记得声明class
date: 2022-03-15 01:15:02
tags: kubernetes
categories: k8s
---

<!--more-->

1.14版本以上的，1.22一下，最好声明class，否则无效
![在这里插入图片描述](https://img-blog.csdnimg.cn/a2c16b3ecfc442c8a3a48f74df3a8dce.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

