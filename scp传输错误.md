---
title: scp传输错误
date: 2021-08-12 11:34:04
tags: 
categories: 解决错误问题
---

<!--more-->

scp wecenter.tar.gz root@192.168.160.160:/web
对于这条命令没意思是将压缩包文件传到/web目录下，对应主机要先建立好/web目录，如果没有，就会被机器理解为串给/下的web文件，因为文件类型不对，会有错误，/web会是乱码
