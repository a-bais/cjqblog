---
title: docker安装（1）
date: 2021-08-26 11:19:58
tags: docker centos linux
categories: docker学习笔记
---

<!--more-->

```bash
下载镜像
wget -O /etc/yum.repos.d/docker-ce.repo https://download.docker.com/linux/centos/docker-ce.repo

修改镜像，使用清华源
sed -i 's/download.docker.com/mirrors.tuna.tsinghua.edu.cn/docker-ce/' /etc/yum.repos.d/docker-ce.repo

yum缓存处理
yum makecache fast或yum repolist

安装docker
yum install -y docker-ce

启动和设置自启动docker
systemctl start docker
systemctl enable docker

查看docker进程
ps aux | grep docker

查看docker版本
docker version

```

