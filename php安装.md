---
title: php安装
date: 2021-08-09 21:38:54
tags: php centos
categories: 重点实操记录 nginx细节配置记录
---

<!--more-->

首先知道一点，默认的源不能自动升级php,我们选择第三方yum仓库

先删除系统自带的旧版本的php相关文件

```bash
yum remove php php-fpm php-mysql php-common
```
安装扩张源，webtatic
**mirror.webtatic.com**
![在这里插入图片描述](https://img-blog.csdnimg.cn/8c7e90a0d6754fb09d7c223abbfd287e.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)

> wget +连接   (将源的安装下载下来)
> yum -y localinstall +包名   (安装这个源)
>![在这里插入图片描述](https://img-blog.csdnimg.cn/0d2366cc83e34f2893a80a2d20ccc189.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTg0MzQxOQ==,size_16,color_FFFFFF,t_70)
mkdir -p ~/soft/php   (专门用来放php相关的包)

然后下载这些软件包
```bash
yum -y install php71w php71w-cli php71w-common php71w-devel php71w-embedded php71w-gd php71w-mcrypt php71w-mbstring php71w-pdo php71w-xml php71w-fpm php71w-mysqlnd php71w-opcache php71w-pecl-memcached php71w-pecl-redis php71w-pecl-mongodb --downloaddir=/root/soft/php --downloadonly

cd ~/soft/php
yum localinstall *.rpm
systemctl start php-fpm
netstat -tunlp | grep php
端口默认9000,127.0.0.1:9000，监控端口是本机
```
往往用到的是php-fpm这个管理进程，通过fastcgi协议
下载时可能会提示缺少libmcrypt.so这个库的错误，这个软件libmcrypt-devel在epel这个库中，先安装这个库一般yum install -y epel-release.noarch，然后yum install -y libmcrypt-devel


附：对于epel库的安装这里记录个问题，用yum install -y epel-release.norch安装，如果你把epel.repo移动到别的目录比如/tmp下，yum repolist是没有这个epel源生效的,使用yum install -y epel-release.noarch会提示已下载，这个问题要留意。
在移动掉centos官方源换成比如阿里源前，先安装epel源，可以使yum install -y epel-release.norch,也可以到阿里巴巴开源镜像站中弄一份epel的yum源配置文件过来。

