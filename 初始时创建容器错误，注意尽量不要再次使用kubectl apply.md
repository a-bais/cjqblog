---
title: 初始时创建容器错误，注意尽量不要再次使用kubectl apply
date: 2021-11-14 15:32:07
tags: 容器 kubernetes docker
categories: 解决错误问题 k8s
---

<!--more-->

kubectl apply虽说兼并了创建和更新与一身，但是有些场景使用它十分留意

如kubectl create -f y.yaml，生成一个pod，但比如由于镜像策略问题创建容器失败，正确修改了yaml文件后，想重新创建，注意了这时候使用apply很容易出错，先delete，再create，才是正确的流程
