---
title: kubernetes的原生的常规控制器
date: 2022-04-24 22:45:21
tags: kubernetes
categories: k8s 总复习:云原生运维开发知识点
---

<!--more-->

@[TOC]
控制器
Kubernetes 控制器会监听资源的 创建/更新/删除 事件，并触发 Reconcile 函数作为响应。整个调整过程被称作 “Reconcile Loop”（调谐循环） 或者 “Sync Loop”（同步循环）。Reconcile 是一个使用资源对象的命名空间和资源对象名称来调用的函数，使得资源对象的实际状态与 资源清单中定义的状态保持一致。调用完成后，Reconcile 会将资源对象的状态更新为当前实际状态。我们可以用下面的一段伪代码来表示这个过程：


```bash
for {
  desired := getDesiredState()  // 期望的状态
  current := getCurrentState()  // 当前实际状态
  if current == desired {  // 如果状态一致则什么都不做
    // nothing to do
  } else {  // 如果状态不一致则调整编排，到一致为止
    // change current to desired status
  }
}
```

这个编排模型就是 Kubernetes 项目中的一个通用编排模式，即：控制循环（control loop）。

# ReplicaSet
假如我们现在有一个 Pod 正在提供线上的服务，我们来想想一下我们可能会遇到的一些场景：

某次运营活动非常成功，网站访问量突然暴增
运行当前 Pod 的节点发生故障了，Pod 不能正常提供服务了
第一种情况，可能比较好应对，活动之前我们可以大概计算下会有多大的访问量，提前多启动几个 Pod 副本，活动结束后再把多余的 Pod 杀掉，虽然有点麻烦，但是还是能够应对这种情况的。

第二种情况，可能某天夜里收到大量报警说服务挂了，然后起来打开电脑在另外的节点上重新启动一个新的 Pod，问题可以解决。

但是如果我们都人工的去解决遇到的这些问题，似乎又回到了以前刀耕火种的时代了是吧？如果有一种工具能够来帮助我们自动管理 Pod 就好了，Pod 挂了自动帮我在合适的节点上重新启动一个 Pod，这样是不是遇到上面的问题我们都不需要手动去解决了。

而 ReplicaSet 这种资源对象就可以来帮助我们实现这个功能，ReplicaSet（RS） 的主要作用就是维持一组 Pod 副本的运行，保证一定数量的 Pod 在集群中正常运行，ReplicaSet 控制器会持续监听它说控制的这些 Pod 的运行状态，在 Pod 发送故障数量减少或者增加时会触发调谐过程，始终保持副本数量一定。

和 Pod 一样我们仍然还是通过 YAML 文件来描述我们的 ReplicaSet 资源对象

```bash
apiVersion: apps/v1
kind: ReplicaSet  
metadata:
  name:  nginx-rs
  namespace: default
spec:
  replicas: 3  # 期望的 Pod 副本数量，默认值为1
  selector:  # Label Selector，必须匹配 Pod 模板中的标签
    matchLabels:
      app: nginx
  template:  # Pod 模板
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
```

> 在 spec 下面描述 ReplicaSet 的基本信息，其中包含3个重要内容：
> 
> replias：表示期望的 Pod 的副本数量 selector：Label Selector，用来匹配要控制的 Pod 标签，需要和下面的
> Pod 模板中的标签一致 template：Pod 模板，实际上就是以前我们定义的 Pod 内容，相当于把一个 Pod
> 的描述以模板的形式嵌入到了 ReplicaSet 中来。


上面就是我们定义的一个普通的 ReplicaSet 资源清单文件，ReplicaSet 控制器会通过定义的 Label Selector 标签去查找集群中的 Pod 对象
![在这里插入图片描述](https://img-blog.csdnimg.cn/535428322a9245c0ad9e12d90d26d0a9.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash
kubectl get rs 
通过查看 RS 可以看到当前资源对象的描述信息，包括DESIRED、CURRENT、READY的状态值
```

我们在 YAML 文件中声明了 3 个副本，然后现在我们删除了一个副本，就变成了两个，这个时候 ReplicaSet 控制器监控到控制的 Pod 数量和期望的 3 不一致，所以就需要启动一个新的 Pod 来保持 3 个副本，这个过程上面我们说了就是调谐的过程

```bash
查看rs的详细信息
kubectl get rs
kubectl describe rs

可以查看到pod所属的控制器
kubectl describe pod
```

另外被 ReplicaSet 持有的 Pod 有一个 **metadata.ownerReferences** 指针指向当前的 ReplicaSet，表示当前 Pod 的所有者，这个引用主要会被集群中的**垃圾收集器**使用以清理失去所有者的 Pod 对象。这个 ownerReferences 和数据库中的外键是不是非常类似

如果要彻底删除 Pod，我们就只能删除 RS 对象

**Replication Controller**

Replication Controller 简称 RC，实际上 RC 和 RS 的功能几乎一致，RS 算是对 RC 的改进，目前唯一的一个区别就是 RC 只支持基于等式的 selector
没必要讲了

总结下selector:

```bash
这是最简单的标签匹配
注意有多个标签则同时满足
selector:  
  matchLabels:
    app: nginx

---
selector:
  matchExpressions:  # 该选择器要求 Pod 包含名为 app 的标签
  - key: app
    operator: In   #In NotIn Exists(标签存在) DoesNotExist Lt(标签纸小于列表中某个值) Gt(标签值大于列表中某个值)
    values:  # 并且标签的值必须是 nginx
    - nginx 

values是个列表，Exists和DoesNotExist不需要values，Gt和Lt不常用
```

# Deployment 控制器
前面我们学习了 ReplicaSet 控制器，了解到该控制器是用来维护集群中运行的 Pod 数量的，但是往往在实际操作的时候，我们反而不会去直接使用 RS，而是会使用更上层的控制器，比如我们今天要学习的主角 Deployment，Deployment 一个非常重要的功能就是实现了 Pod 的滚动更新，比如我们应用更新了，我们只需要更新我们的容器镜像，然后修改 Deployment 里面的 Pod 模板镜像，那么 Deployment 就会用**滚动更新**（Rolling Update）的方式来升级现在的 Pod，这个能力是非常重要的，因为对于线上的服务我们需要做到不中断服务，所以滚动更新就成了必须的一个功能。而 Deployment 这个能力的实现，依赖的就是上节课我们学习的 ReplicaSet 这个资源对象

```bash
格式和rs几乎一致
apiVersion: apps/v1
kind: Deployment  
metadata:
  name:  nginx-deploy
  namespace: default
spec:
  replicas: 3  # 期望的 Pod 副本数量，默认值为1
  selector:  # Label Selector，必须匹配 Pod 模板中的标签
    matchLabels:
      app: nginx
  template:  # Pod 模板
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
```

```bash
kubectl get deploy,rs,pod

用kubectl describe pod可以看到实际的控制器是某个rs
Controlled By: ReplicaSet/nginx-deploy-85ff79dd56
以为deploy其实是通过调用rs来管理pod
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/40a85c38bb2e42fb974b1de34c1e0bd8.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
通过上图我们可以很清楚的看到，定义了3个副本的 Deployment 与 ReplicaSet 和 Pod 的关系，就是一层一层进行控制的。ReplicaSet 作用和之前一样还是来保证 Pod 的个数始终保存指定的数量，所以 Deployment 中的容器 **restartPolicy=Always** 是唯一的就是这个原因，因为容器必须始终保证自己处于 Running 状态，ReplicaSet 才可以去明确调整 Pod 的个数。而 **Deployment 是通过管理 ReplicaSet 的数量和属性来实现水平扩展/收缩以及滚动更新**两个功能的

## 水平伸缩
水平扩展/收缩的功能比较简单，因为 ReplicaSet 就可以实现，所以 Deployment 控制器只需要去修改它缩控制的 ReplicaSet 的 Pod 副本数量就可以了。比如现在我们把 Pod 的副本调整到 4 个，那么 Deployment 所对应的 ReplicaSet 就会自动创建一个新的 Pod 出来，这样就水平扩展了，我们可以使用一个新的命令 kubectl scale 命令来完成这个操作

```bash
kubectl scale deployment nginx-deploy --replicas=4
scale规模
```

## 滚动更新
如果只是水平扩展/收缩这两个功能，就完全没必要设计 Deployment 这个资源对象了，Deployment 最突出的一个功能是支持滚动更新

```bash
apiVersion: apps/v1
kind: Deployment  
metadata:
  name:  nginx-deploy
  namespace: default
spec:
  replicas: 3  
  selector:  
    matchLabels:
      app: nginx
  minReadySeconds: 5
  strategy:  
    type: RollingUpdate  # 指定更新策略：RollingUpdate和Recreate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  template:  
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```
apply更新
和前面相比较，除了更改了镜像之外，我们还指定了更新策略

实际生产环境用百分比更好

```bash
    "strategy": {
      "type": "RollingUpdate",
      "rollingUpdate": {
        "maxUnavailable": "25%",
        "maxSurge": "25%"
      }
    },
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/f14744794d774b84978e78581d54cad7.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
普遍就是开一个新的，在关掉一个旧的

> minReadySeconds：表示 Kubernetes 在等待设置的时间后才进行升级，如果没有设置该值，Kubernetes 会假设该容器启动起来后就提供服务了，如果没有设置该值，在某些极端情况下可能会造成服务不正常运行，默认值就是0。
> .
type=RollingUpdate：表示设置更新策略为滚动更新，可以设置为Recreate和RollingUpdate两个值，Recreate表示全部重新创建，默认值就是RollingUpdate。
..
maxSurge：表示升级过程中最多可以比原先设置多出的 Pod 数量，例如：maxSurage=1，replicas=5，就表示Kubernetes 会先启动一个新的 Pod，然后才删掉一个旧的 Pod，整个升级过程中最多会有5+1个 Pod。
.
maxUnavaible：表示升级过程中最多有多少个 Pod 处于无法提供服务的状态，当maxSurge不为0时，该值也不能为0，例如：maxUnavaible=1，则表示 Kubernetes 整个升级过程中最多会有1个 Pod 处于无法服务的状态。

```bash
查看此次更新状态
kubectl rollout status deployment/nginx-deploy

暂停更新
kubectl rollout pause deployment/nginx-deploy

恢复更新
kubectl rollout resume deployment/nginx-deploy

获取历史版本
kubectl rollout history deployment nginx-deploy

回到上一版本
kubectl rollout undo deployment nginx-deploy

回到指定版本
kubectl rollout undo deployment nginx-deploy --to-revision=1

查看指定版本版本的信息
kubectl rollout history deployment nginx-deploy --revision=1
```

**其实 rollout history 中记录的 revision 是和 ReplicaSets 一一对应。如果我们手动删除某个 ReplicaSet，对应的rollout history就会被删除，也就是说你无法回滚到这个revison了**

回滚的操作滚动的revision始终是递增


CHANGE_CAUSE:
用注释可以添加这个change_cause

```bash
annotations:
  kubernetes.io/change-cause: image updated to 1.9.1
```

```bash
[root@master ~]# kubectl apply -f rollupdate.yaml 
deployment.apps/nginx-deploy created
[root@master ~]# kubectl get deploy
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deploy   3/3     3            3           2m4s
[root@master ~]# kubectl rollout history deployment nginx-deploy
deployment.apps/nginx-deploy 
REVISION  CHANGE-CAUSE
1         <none>      #最新的数字表示当前版本
[root@master ~]# vim rollupdate.yaml      #加入注释
[root@master ~]# kubectl apply -f rollupdate.yaml 
deployment.apps/nginx-deploy configured
[root@master ~]# kubectl rollout history deploy nginx-deploy
deployment.apps/nginx-deploy 
REVISION  CHANGE-CAUSE
1         <none>
2         image updated to 1.9.1
[root@master ~]# kubectl rollout undo deploy nginx-deploy --to-revision=1
deployment.apps/nginx-deploy rolled back
[root@master ~]# kubectl rollout history
error: required resource not specified
[root@master ~]# kubectl rollout history deploy nginx-deploy
deployment.apps/nginx-deploy 
REVISION  CHANGE-CAUSE
2         image updated to 1.9.1
3         <none>

```

> 在很早之前的 Kubernetes 版本中，默认情况下会为我们暴露下所有滚动升级的历史记录，也就是 ReplicaSet对象，但一般情况下没必要保留所有的版本，毕竟会存在 etcd 中，我们可以通过配置 spec.revisionHistoryLimit属性来设置保留的历史记录数量，不过新版本中该值默认为 10，如果希望多保存几个版本可以设置该字段。

# StatefulSet 控制器
前面我们学习了 Deployment 和 ReplicaSet 两种资源对象得使用，在实际使用的过程中，**Deployment** 并不能编排所有类型的应用，对**无状态服务**编排是非常容易的，但是对于有状态服务就无能为力了。我们需要先明白一个概念：什么是有状态服务，什么是无状态服务。

无状态服务（Stateless Service）：该服务运行的实例**不会在本地存储需要持久化的数据**，并且多个实例对于同一个请求响应的结果是完全一致的，比如前面我们讲解的 WordPress 实例，我们是不是可以同时启动多个实例，但是我们访问任意一个实例得到的结果都是一样的吧？因为他唯一需要持久化的数据是存储在MySQL数据库中的，所以我们可以说 WordPress 这个应用是无状态服务，但是 MySQL 数据库就不是了，因为他需要把数据持久化到本地。
有状态服务（Stateful Service）：就和上面的概念是对立的了，该服务运行的实例需要在本地存储持久化数据，比如上面的 MySQL 数据库，你现在运行在节点 A，那么他的数据就存储在节点 A 上面的，如果这个时候你把该服务迁移到节点 B 去的话，那么就没有之前的数据了，因为他需要去对应的数据目录里面恢复数据，而此时没有任何数据。
现在对有状态和无状态有一定的认识了吧，比如我们常见的 WEB 应用，是通过 Session 来保持用户的登录状态的，如果我们将 Session 持久化到节点上，那么该应用就是一个有状态的服务了，因为我现在登录进来你把我的 Session 持久化到节点 A 上了，下次我登录的时候可能会将请求路由到节点 B 上去了，但是节点 B 上根本就没有我当前的 Session 数据，就会被认为是未登录状态了，这样就导致我前后两次请求得到的结果不一致了。所以一般为了横向扩展，我们都会把这类 WEB 应用改成无状态的服务，怎么改？将 Session 数据存入一个公共的地方，比如 Redis 里面，是不是就可以了，对于一些客户端请求 API 的情况，我们就不使用 Session 来保持用户状态，改成用 Token 也是可以的。

无状态服务利用我们前面的 Deployment 可以很好的进行编排，对应有状态服务，需要考虑的细节就要多很多了，容器化应用程序最困难的任务之一，就是设计**有状态分布式组件的部署体系结构**。由于**无状态组件没有预定义的启动顺序、集群要求、点对点 TCP 连接、唯一的网络标识符、正常的启动和终止要求**等，因此可以很容易地进行容器化。诸如数据库，大数据分析系统，分布式 key/value 存储、消息中间件需要有复杂的分布式体系结构，都可能会用到上述功能。为此，Kubernetes 引入了 StatefulSet 这种资源对象来支持这种复杂的需求。StatefulSet 类似于 ReplicaSet，但是它可以**处理 Pod 的启动顺序，为保留每个 Pod 的状态设置唯一标识**，具有以下几个功能特性：

> 稳定的、唯一的网络标识符 
> 稳定的、持久化的存储 
> 有序的、优雅的部署和缩放 
> 有序的、优雅的删除和终止 
> 有序的、自动滚动更新

**判断使用无状态服务还是有状态服务的架构类型，主要看他是否对在本地数据持久化有较大依赖**

## Headless Service
Service 是应用服务的抽象，通过 Labels 为应用提供负载均衡和服务发现，每个 Service 都会自动分配**一个 cluster IP 和 DNS 名**，在集群内部我们可以**通过该地址或者通过 FDQN** 的形式来访问服务。(有coredns管理这些clusterip对域名的映射)
headless service和statefulset并不是一定要一起搭配使用的，都是互相独立的，只是搭配只用效果较好

> 比如，一个 Deployment 有 3 个 Pod，那么我就可以定义一个 Service，有如下两种方式来访问这个 Service：
> cluster IP 的方式，比如：当我访问 10.109.169.155 这个 Service 的 IP
> 地址时，10.109.169.155 其实就是一个 VIP，它会把请求转发到该 Service 所代理的 Endpoints 列表中的某一个
> Pod 上。(kube-proxy监听每个svc的服务端口，将流量进行转发映射) Service 的 DNS方式，比如我们访问“mysvc.mynamespace.svc.cluster.local”这条 DNS 记录，就可以访问到mynamespace 这个命名空间下面名为 mysvc 的 Service 所代理的某一个 Pod。

集群内的FQDN(全局完整唯一域名):

```bash
mysvc.mynamespace.svc.cluster.local
```


> 对于 DNS 这种方式实际上也有两种情况：
> 
> 第一种就是普通的 Service，我们访问“mysvc.mynamespace.svc.cluster.local”的时候是通过集群中的
> DNS 服务解析到的 mysvc 这个 Service 的 cluster IP 的 
> 第二种情况就是Headless Service，对于这种情况，我们访问“mysvc.mynamespace.svc.cluster.local”的时候是直接解析到的mysvc 代理的某一个具体的 Pod 的 IP 地址，中间少了 cluster IP 的转发，这就是二者的最大区别，Headless Service 不需要分配一个 VIP，而是可以直接以 DNS 的记录方式解析到后面的 Pod 的 IP 地址


**headless service没有clusterIP，所以访问时直接解析到实际的pod的ip**(不能直接通过clusterip访问)

```bash
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
  labels:
    app: nginx
spec:
  ports:
  - name: http
    port: 80
  clusterIP: None
  selector:
    app: nginx
```

实际上 Headless Service 在定义上和普通的 Service 几乎一致, 只是他的 clusterIP=None，所以，这个 Service 被创建后并不会被分配一个 cluster IP，而是会**以 DNS 记录的方式暴露出它所代理的 Pod**，而且还有一个非常重要的特性，**对于 Headless Service 所代理的所有 Pod 的 IP 地址都会绑定一个如下所示的 DNS 记录**：


```bash
<pod-name>.<svc-name>.<namespace>.svc.cluster.local
```

这个 DNS 记录正是 Kubernetes 集群为 Pod 分配的一个唯一标识，只要我们知道 Pod 的名字，以及它对应的 Service 名字，就可以组装出这样一条 DNS 记录访问到 Pod 的 IP 地址，这个能力是非常重要的，接下来我们就来看下 StatefulSet 资源对象是如何结合 Headless Service 提供服务的。(适用于某种请求指定要到某个固定的pod)

**而statefulset控制器的pod是有着命名顺序的，所以statefulset和headless service搭配使用较好
这里注意statefulset不是一定要绑定headless service，普通的service也行**

```bash
普通的service：域名访问->解析到clusterip->转发到podip
headless：域名访问->解析到podip
```

## StatefulSet控制器
准备两个pv，要在节点创建/tmp/pv{1,2}
```bash
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv1
spec:
  capacity:
    storage: 1Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /tmp/pv1

---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv2
spec:
  capacity:
    storage: 1Gi
  accessModes:
  - ReadWriteOnce
  hostPath:
    path: /tmp/pv2
```

```bash
statefulset资源清单

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
  namespace: default
spec:
  serviceName: "nginx"
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - name: web
          containerPort: 80
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

从上面的资源清单中可以看出和我们前面的 Deployment 基本上也是一致的，也是通过声明的 Pod 模板来创建 Pod 的，另外上面资源清单中和 volumeMounts 进行关联的不是 volumes 而是一个新的属性：**volumeClaimTemplates，该属性会自动创建一个 PVC 对象，其实这里就是一个 PVC 的模板，和 Pod 模板类似，PVC 被创建后会自动去关联当前系统中和他合适的 PV 进行绑定**。除此之外，还多了一个 serviceName: "nginx" 的字段，serviceName 就是管理当前 StatefulSet 的服务名称，该服务必须在 StatefulSet 之前存在，并且负责该集合的网络标识，Pod 会遵循以下格式获取 DNS/主机名：`pod-specific-string.serviceName.default.svc.cluster.local`，其中 pod-specific-string 由 StatefulSet 控制器管理。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d647c014a82848e9b56b2d0f58467a8d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

StatefulSet 的拓扑结构和其他用于部署的资源对象其实比较类似，比较大的区别在于 StatefulSet 引入了 PV 和 PVC 对象来持久存储服务产生的状态，这样所有的服务虽然可以被杀掉或者重启，但是其中的数据由于 PV 的原因不会丢失。

> 注意
> 
> 由于我们这里用volumeClaimTemplates声明的模板是挂载点的方式，并不是 volume，所有实际上上当于把 PV
> 的存储挂载到容器中，所以会覆盖掉容器中的数据，在容器启动完成后我们可以手动在 PV 的存储里面新建 index.html
> 文件来保证容器的正常访问，当然也可以进入到容器中去创建，这样更加方便：
> 
> 
> $ for i in 0 1; do kubectl exec web-$i -- sh -c 'echo hello
> $(hostname) > /usr/share/nginx/html/index.html';


apply上边的statefulset的资源清单文件即可

这两个 Pod 是按照顺序进行创建的，web-0 启动起来后 web-1 才开始创建。如同上面 StatefulSet 概念中所提到的，StatefulSet 中的 Pod 拥有一个具有稳定的、独一无二的身份标志。这个标志基于 StatefulSet 控制器分配给每个 Pod 的唯一顺序索引。Pod 的名称的形式为**<statefulset name>-<ordinal index>**。我们这里的对象拥有两个副本，所以它创建了两个 Pod 名称分别为：web-0 和 web-1，我们可以使用 kubectl exec 命令进入到容器中查看它们的 hostname：


```bash
$ kubectl exec web-0 hostname
web-0
$ kubectl exec web-1 hostname
web-1
```



**StatefulSet 中 Pod 副本的创建会按照序列号升序处理，副本的更新和删除会按照序列号降序处理。**

kubectl describe pod可以看到pod由statefulset控制器管理

现在我们创建一个 busybox（该镜像中有一系列的工具）的容器，在容器中用 DNS 的方式来访问一下这个 Headless Service，由于我们这里只是单纯的为了测试，所以没必要写资源清单文件来声明，用kubectl run命令启动一个测试的容器即可(经常会用busybox来测试，busybox是个工具集，不给命令会报错)


```bash
kubectl run -it --image busybox:1.28.3 test --restart=Never --rm /bin/sh
```

### kubectl run 和docker run
**kubectl run跟docker run不同，kubectl run 的容器实际也是pod管理，最主要是它是属于集群的，docker run则是docker自己管理的。**

```bash
kubectl run -it --image image_name container_name -- /bin/bash(bash不行就换sh)
docker run -it --name container_name image_name /bin/bash

kubectl run NAME --image=image [--env="key=value"] [--port=port] [--dry-run=server|client] [--overrides=inline-json]
[--command] -- [COMMAND] [args...] [options]

```

现在我们在这个 Pod 容器里面可以使用 nslookup 命令来尝试解析下上面我们创建的 Headless Service：
(nslookup 域名，这里测试了service和pod的域名)


```bash
/ # nslookup nginx
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      nginx
Address 1: 10.244.1.175 web-1.nginx.default.svc.cluster.local
Address 2: 10.244.4.83 web-0.nginx.default.svc.cluster.local
/ # ping nginx
PING nginx (10.244.1.175): 56 data bytes
64 bytes from 10.244.1.175: seq=0 ttl=62 time=1.076 ms
64 bytes from 10.244.1.175: seq=1 ttl=62 time=1.029 ms
64 bytes from 10.244.1.175: seq=2 ttl=62 time=1.075 ms
```

我们直接解析 Headless Service 的名称，可以看到得到的是两个 Pod 的解析记录，但实际上如果我们**通过nginx这个 DNS 去访问我们的服务的话，并不会随机或者轮询背后的两个 Pod，而是访问到一个固定的 Pod**，所以不能代替普通的 Service。如果分别解析对应的 Pod 呢？


```bash
$ / # nslookup web-0.nginx
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-0.nginx
Address 1: 10.244.4.83 web-0.nginx.default.svc.cluster.local
/ # nslookup web-1.nginx
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-1.nginx
Address 1: 10.244.1.175 web-1.nginx.default.svc.cluster.local
```

可以看到解析 web-0.nginx 的时候解析到了 web-0 这个 Pod 的 IP，web-1.nginx 解析到了 web-1 这个 Pod 的 IP，而且这个 DNS 地址还是稳定的，因为 Pod 名称就是固定的，比如我们这个时候去删掉 web-0 和 web-1 这两个 Pod：


```bash
$ kubectl delete pod -l app=nginx
pod "web-0" deleted
pod "web-1" deleted
```

删除完成后才看 Pod 状态：


```bash
$ kubectl get pods -l app=nginx  
NAME    READY   STATUS    RESTARTS   AGE
web-0   1/1     Running   0          42s
web-1   1/1     Running   0          39s
```

可以看到 StatefulSet 控制器仍然会安装顺序创建出两个 Pod 副本出来，**而且 Pod 的唯一标识依然没变，所以这两个 Pod 的网络标识还是固定的，我们依然可以通过web-0.nginx去访问到web-0这个 Pod，虽然 Pod 已经重建了，对应 Pod IP 已经变化了，但是访问这个 Pod 的地址依然没变，并且他们依然还是关联的之前的 PVC**(pod重建之后ip就变了，依然使用这个volume，自然数据会持久化)，数据并不会丢失：


```bash
/ # nslookup web-0.nginx
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-0.nginx
Address 1: 10.244.3.98 web-0.nginx.default.svc.cluster.local
/ # nslookup web-1.nginx
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-1.nginx
Address 1: 10.244.1.176 web-1.nginx.default.svc.cluster.local
```

通过 Headless Service，StatefulSet 就保证了 Pod 网络标识的唯一稳定性，由于 Pod IP 并不是固定的，所以我们访问有状态应用实例的时候，就必须使用 DNS 记录的方式来访问了，所以很多同学偶尔有**固定的 Pod IP 的需求，或许可以用这种方式来代替**。

最后我们可以通过删除 StatefulSet 对象来删除所有的 Pod，仔细观察也会发现是按照倒序的方式进行删除的：


```bash
$ kubectl delete statefulsets  web
statefulset.apps "web" deleted
$ kubectl get pods --watch
NAME    READY   STATUS    RESTARTS   AGE
web-1   1/1   Terminating   0     3h/31m
web-0   1/1   Terminating   0     3h/31m
```

## 管理策略
对于某些分布式系统来说，StatefulSet 的顺序性保证是不必要和/或者不应该的，这些系统仅仅要求唯一性和身份标志。为了解决这个问题，我们只需要在声明 StatefulSet 的时候重新设置 spec.podManagementPolicy 的策略即可。

默认的管理策略是 **OrderedReady**，表示让 StatefulSet 控制器遵循上文演示的顺序性保证。除此之外，还可以设置为 **Parallel** 管理模式，表示让 StatefulSet 控制器并行的终止所有 Pod，在启动或终止另一个 Pod 前，不必等待这些 Pod 变成 Running 和 Ready 或者完全终止状态。

## 更新策略
前面课程中我们学习了 Deployment 的升级策略，在 StatefulSet 中同样也支持两种升级策略：onDelete 和 RollingUpdate，同样可以通过设置 .spec.updateStrategy.type 进行指定。
(更新策略很多时候默认滚动更新，一般我们也是使用滚动更新)

OnDelete: 该策略表示当更新了 StatefulSet 的模板后，只有手动删除旧的 Pod 才会创建新的 Pod。
RollingUpdate：该策略表示当更新 StatefulSet 模板后会自动删除旧的 Pod 并创建新的Pod，如果更新发生了错误，这次“滚动更新”就会停止。不过需要注意 StatefulSet 的 Pod 在部署时是顺序从 0~n 的，而在滚动更新时，这些 Pod 则是按逆序的方式即 n~0 一次删除并创建。
另外SatefulSet 的滚动升级还支持 Partitions的特性，可以通过.spec.updateStrategy.rollingUpdate.partition 进行设置，在设置 partition 后，SatefulSet 的 Pod 中序号大于或等于 partition 的 Pod 会在 StatefulSet 的模板更新后进行滚动升级，而其余的 Pod 保持不变，这个功能是不是可以实现灰度发布？大家可以去手动验证下。

在实际的项目中，其实我们还是**很少会去直接通过 StatefulSet 来部署我们的有状态服务的，除非你自己能够完全能够 hold 住，对于一些特定的服务，我们可能会使用更加高级的 Operator 来部署**，比如 **etcd-operator、prometheus-operator** 等等，这些应用都能够很好的来管理有状态的服务，而不是单纯的使用一个 StatefulSet 来部署一个 Pod 就行，因为对于有状态的应用最重要的还是数据恢复、故障转移等等。

# DaemonSet 控制器
通过该控制器的名称我们可以看出它的用法：Daemon，就是用来部署守护进程的，DaemonSet用于在**每个 Kubernetes 节点中将守护进程的副本作为后台进程运行，说白了就是在每个节点部署一个 Pod副本，当节点加入到 Kubernetes 集群中，Pod 会被调度到该节点上运行，当节点从集群只能够被移除后，该节点上的这个 Pod 也会被移除**，当然，如果我们删除 DaemonSet，所有和这个对象相关的 Pods都会被删除。那么在哪种情况下我们会需要用到这种业务场景呢？其实这种场景还是比较普通的，比如：

> 集群存储守护程序，如 glusterd、ceph 要部署在每个节点上以提供持久性存储； 
> 节点监控守护进程，如 Prometheus监控集群，可以在每个节点上运行一个 node-exporter 进程来收集监控节点的信息； 
> 日志收集守护程序，如 fluentd 或logstash，在每个节点上运行以收集容器的日志 
> 节点网络插件，比如 flannel、calico，在每个节点上运行为 Pod提供网络服务。

这里需要特别说明的一个就是关于 DaemonSet 运行的 Pod 的调度问题，正常情况下，Pod 运行在哪个节点上是由 Kubernetes 的调度器策略来决定的，然而，由 DaemonSet 控制器创建的 Pod 实际上提前已经确定了在哪个节点上了（Pod创建时指定了.spec.nodeName），所以：

**DaemonSet 并不关心一个节点的 unshedulable 字段，DaemonSet 可以创建 Pod，即使调度器还没有启动**。
**daemonset对pod的调度，不依赖scheduler**

```bash
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nginx-ds
  namespace: default
spec:
  selector:
    matchLabels:
      k8s-app: nginx
  template:
    metadata:
      labels:
        k8s-app: nginx
    spec:
      containers:
      - image: nginx:1.7.9
        name: nginx
        ports:
        - name: http
          containerPort: 80
```

**因为 master 节点上默认被打上了污点，所以默认情况下不能调度普通的 Pod 上去**
![在这里插入图片描述](https://img-blog.csdnimg.cn/cb6c8c46306d4b2499620a95e3c35bd8.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

集群中的 Pod 和 Node 是一一对应d的，而 DaemonSet 会管理全部机器上的 Pod 副本，负责对它们进行更新和删除。

那么，DaemonSet 控制器是如何保证每个 Node 上有且只有一个被管理的 Pod 

> 首先控制器从 Etcd 获取到所有的 Node 列表，然后遍历所有的 Node。 
> 根据资源对象定义是否有调度相关的配置，然后分别检查Node 是否符合要求。 
> 在可运行 Pod 的节点上检查是否已有对应的 Pod，如果没有，则在这个 Node 上创建该Pod；如果有，并且数量大于 1，那就把多余的 Pod 从这个节点上删除；如果有且只有一个 Pod，那就说明是正常情况。

实际上当我们学习了资源调度后，我们也可以自己用 Deployment 来实现 DaemonSet 的效果，这里我们明白 DaemonSet 如何使用的即可，当然该资源对象也有对应的更新策略，有OnDelete和RollingUpdate两种方式，默认是滚动更新。

# Job 与 CronJob
日常的工作中经常都会遇到一些需要进行批量数据处理和分析的需求，当然也会有按时间来进行调度的工作，在我们的 Kubernetes 集群中为我们提供了 Job 和 CronJob 两种资源对象来应对我们的这种需求。


> Job 负责处理任务，即仅执行一次的任务，它保证批处理任务的一个或多个 Pod 成功结束。而CronJob 则就是在 Job上加上了时间调度。

## Job
我们用 Job 这个资源对象来创建一个任务，我们定义一个 Job 来执行一个倒计时的任务

```bash
apiVersion: batch/v1
kind: Job
metadata:
  name: job-demo
spec:
  template:
    spec:
      restartPolicy: Never
      containers:
      - name: counter
        image: busybox
        command:
        - "bin/sh"
        - "-c"
        - "for i in 9 8 7 6 5 4 3 2 1; do echo $i; done"
```
Job 中也是一个 Pod 模板，和之前的 Deployment、StatefulSet 之类的是一致的，只是 **Pod 中的容器要求是一个任务，而不是一个常驻前台的进程了**(一般都是用busybox)，因为需要退出，另外值得注意的是 Job 的 RestartPolicy 仅支持 Never 和 OnFailure 两种，不支持 Always，我们知道 Job 就相当于来执行一个批处理任务，执行完就结束了

运行的内容可以用kubectl logs查看

Job 对象在创建后，它的 Pod 模板，被自动加上了一个 controller-uid=< 一个随机字符串 > 这样的 Label 标签，而这个 Job 对象本身，则被自动加上了这个 Label 对应的 Selector，从而 保证了 Job 与它所管理的 Pod 之间的匹配关系。而 Job 控制器之所以要使用这种携带了 UID 的 Label，就是为了避免不同 Job 对象所管理的 Pod 发生重合
(**Job和其他控制器一样，都是使用label来将控制器和pod绑定，但Job的pod的label和Job的selector不用自己创建**)

如果的任务执行失败了，如果定义了 restartPolicy=OnFailure，那么任务在执行失败后 Job 控制器就会不断地尝试**创建一个新 Pod**，当然，这个尝试肯定不能无限进行下去。我们可以通过 Job 对象的 **spec.backoffLimit** 字段来定义重试次数，另外需要注意的是 Job 控制器重新创建 Pod 的间隔是呈指数增加的，即下一次重新创建 Pod 的动作会分别发生在 10s、20s、40s… 后。

**定义的 restartPolicy=Never，那么任务执行失败后，Job 控制器就不会去尝试创建新的 Pod 了，它会不断地尝试重启 Pod 里的容器**
**(所以restartPolicy是针对pod的，叫pod的重启策略**)

上面我们这里的 Job 任务对应的 Pod 在运行结束后，会变成 Completed 状态

如果Job由于某些原因运行时间过长，那就没意义，在 Job 对象中通过设置字段 spec.activeDeadlineSeconds 来限制任务运行的最长时间：

```bash
spec:
 activeDeadlineSeconds: 100
```

任务 Pod 运行超过了 100s 后，这个 Job 的所有 Pod 都会被终止，并且， Pod 的终止原因会变成 DeadlineExceeded。

除此之外，我们还可以通过设置 spec.parallelism 参数来进行并行控制，该参数定义了一个 Job 在任意时间最多可以有多少个 Pod 同时运行。spec.completions 参数可以定义 Job 至少要完成的 Pod 数目。

## CronJob
CronJob 其实就是在 Job 的基础上加上了时间调度，我们可以在给定的时间点运行一个任务，也可以周期性地在给定时间点运行。这个实际上和我们 Linux 中的 crontab 就非常类似了。

一个 CronJob 对象其实就对应中 crontab 文件中的一行，它根据配置的时间格式周期性地运行一个Job，格式和 crontab 也是一样的。
(分时日月周 要运行的命令)

```bash
第1列分钟0～59
第2列小时0～23）
第3列日1～31
第4列月1～12
第5列星期0～7（0和7表示星期天）
第6列要运行的命令
```

```bash
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: cronjob-demo
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          restartPolicy: OnFailure
          containers:
          - name: hello
            image: busybox
            args:
            - "bin/sh"
            - "-c"
            - "for i in 9 8 7 6 5 4 3 2 1; do echo $i; done"
```

.spec.schedule字段是必须填写的，用来指定任务运行的周期，格式就和 crontab 一样，另外一个字段是.spec.jobTemplate, 用来指定需要运行的任务，格式当然和 Job 是一致的。还有一些值得我们关注的字段 **.spec.successfulJobsHistoryLimit(默认为3) 和 .spec.failedJobsHistoryLimit(默认为1)**，表示历史限制，是可选的字段，指定可以保留多少完成和失败的 Job。然而，当运行一个 CronJob 时，Job 可以很快就堆积很多，所以一般推荐设置这两个字段的值。如果设置限制的值为 0，那么相关类型的 Job 完成后将不会被保留(**一个CronJob容易生成多个Job，不会收干净，是对资源的浪费**)

一旦不再需要 CronJob，我们可以使用 kubectl 命令删除它：


```bash
$ kubectl delete cronjob cronjob-demo
```

不过需要注意的是这将会终止正在创建的 Job，但是运行中的 Job 将不会被终止，不会删除 Job 或 它们的 Pod。

(最好自己再手动查看下Job，回收干净)

# HPA
Horizontal Pod Autoscaling（Pod 水平自动伸缩），简称HPA，两个字自动，前面的控制器的水平伸缩是手动式的，kubectl scale也是，既然是自动，那必然要有判断标准，也就是指标
自动化去感知业务，来自动进行扩缩容

![在这里插入图片描述](https://img-blog.csdnimg.cn/c9dc34fcfce64f3fa293045136de7802.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

## Metrics Server
在 HPA 的第一个版本中，我们需要 Heapster 提供 CPU 和内存指标，在 HPA v2 过后就需要安装 Metrcis Server 了，Metrics Server 可以通过标准的 Kubernetes API 把监控数据暴露出来，有了 Metrics Server 之后，我们就完全可以通过标准的 **Kubernetes API** 来访问我们想要获取的监控数据了：


```bash
https://10.96.0.1/apis/metrics.k8s.io/v1beta1/namespaces/<namespace-name>/pods/<pod-name>
这里是kubernetes这个svc的ip
10.96一般是svc的
10.244一般是pod的
```

比如当我们访问上面的 API 的时候，我们就可以获取到该 Pod 的资源数据，这些数据其实是来自于 kubelet 的 Summary API 采集而来的。不过需要说明的是我们这里可以通过标准的 API 来获取资源监控数据，并不是因为 Metrics Server 就是 APIServer 的一部分，而是通过 Kubernetes 提供的 Aggregator 汇聚插件来实现的，是独立于 APIServer 之外运行的。

## 聚合 API
Aggregator 允许开发人员编写一个自己的服务，把这个服务注册到 Kubernetes 的 APIServer 里面去，这样我们就可以像原生的 APIServer 提供的 API 使用自己的 API 了，我们把自己的服务运行在 Kubernetes 集群里面，然后 Kubernetes 的 Aggregator 通过 Service 名称就可以转发到我们自己写的 Service 里面去了。这样这个聚合层就带来了很多好处：

增加了 API 的扩展性，开发人员可以编写自己的 API 服务来暴露他们想要的 API。
丰富了 API，核心 kubernetes 团队阻止了很多新的 API 提案，通过允许开发人员将他们的 API 作为单独的服务公开，这样就无须社区繁杂的审查了。
开发分阶段实验性 API，新的 API 可以在单独的聚合服务中开发，当它稳定之后，在合并会 APIServer 就很容易了。
确保新 API 遵循 Kubernetes 约定，如果没有这里提出的机制，社区成员可能会被迫推出自己的东西，这样很可能造成社区成员和社区约定不一致。
## 开启aggregator和安装metrics server
所以现在我们要使用 HPA，就需要在集群中安装 Metrics Server 服务，要安装 Metrics Server 就需要开启 Aggregator，因为 Metrics Server 就是通过该代理进行扩展的，不过我们集群是通过 Kubeadm 搭建的，默认已经开启了，如果是二进制方式安装的集群，需要单独配置 kube-apsierver 添加如下所示的参数：

```bash
--requestheader-client-ca-file=<path to aggregator CA cert>
--requestheader-allowed-names=aggregator
--requestheader-extra-headers-prefix=X-Remote-Extra-
--requestheader-group-headers=X-Remote-Group
--requestheader-username-headers=X-Remote-User
--proxy-client-cert-file=<path to aggregator proxy cert>
--proxy-client-key-file=<path to aggregator proxy key>
```

如果 kube-proxy 没有和 APIServer 运行在同一台主机上，那么需要确保启用了如下 kube-apsierver 的参数：


```bash
--enable-aggregator-routing=true
```

对于这些证书的生成方式，我们可以查看官方文档：[https://github.com/kubernetes-sigs/apiserver-builder-alpha/blob/master/docs/concepts/auth.md](https://github.com/kubernetes-sigs/apiserver-builder-alpha/blob/master/docs/concepts/auth.md)

Aggregator 聚合层启动完成后，就可以来安装 Metrics Server 了，我们可以获取该仓库的官方安装资源清单：


```bash
#官方仓库地址：https://github.com/kubernetes-sigs/metrics-server
$ wget https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.4.0/components.yaml
```

如果你实在下载不了，估计是raw.githubusercontent.com被墙了，可以去ipaddress网站解析个ip出来配置进/etc/hosts

在部署之前，修改 components.yaml 的镜像地址为：


```bash
hostNetwork: true  # 使用hostNetwork模式
containers:
- name: metrics-server
  image: cnych/metrics-server:v0.4.0
```

因为部署集群的时候，CA 证书并没有把各个节点的 IP 签上去，所以这里 Metrics Server 通过 IP 去请求时，提示签的证书没有对应的 IP（错误：x509: cannot validate certificate for 10.151.30.22 because it doesn’t contain any IP SANs），我们可以在上面的yaml文件添加一个--kubelet-insecure-tls参数跳过证书校验：


```bash
args:
- --cert-dir=/tmp
- --secure-port=4443
- --kubelet-insecure-tls
- --kubelet-preferred-address-types=InternalIP
```

可以通过 kubectl top 命令来获取到资源数据了，证明 Metrics Server 已经安装成功了

```bash
kubectl top nodes
kubectl top pod
```

## HPA控制器
现在我们用 Deployment 来创建一个 Nginx Pod，然后利用 HPA 来进行自动扩缩容

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hpa-demo
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
```

创建一个 HPA 资源对象，可以使用kubectl autoscale命令来创建：

```bash
kubectl autoscale deployment hpa-demo --cpu-percent=10 --min=1 --max=10
kubectl get hpa
```
此命令创建了一个关联资源 hpa-demo 的 HPA，最小的 Pod 副本数为1，最大为10。HPA 会根据设定的 cpu 使用率（10%）动态的增加或者减少 Pod 数量。

可以通过创建 YAML 文件的形式来创建 HPA 资源对象，不熟悉hpa的yaml文件的编写，可以kubectl get 上面创建的hpa来查看

kubectl describe hpa hpa_name来查看hpa的工作情况：
我们可以看到上面的事件信息里面出现了 failed to get cpu utilization: missing request for cpu 这样的错误信息。这是因为我们上面创建的 Pod 对象没有添加 request 资源声明，这样导致 HPA 读取不到 CPU 指标信息，所以如果要想让 HPA 生效，对应的 Pod 资源必须添加 requests 资源声明，更新我们的资源清单文件：

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hpa-demo
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        resources:
          requests:
            memory: 50Mi
            cpu: 50m
```
重新更新 Deployment，重新创建 HPA 对象

增大负载进行测试，我们来创建一个 busybox 的 Pod，并且循环访问上面创建的 Pod：


```bash
kubectl run -it --image busybox test-hpa --restart=Never --rm /bin/sh
while true; do wget -q -O- http://10.244.1.113; done

```

```bash
kubectl get hpa
kubectl get deploy
kubectl get pod --watch
```

> 缩放间隙
> 
> 从 Kubernetes v1.12 版本开始我们可以通过设置 kube-controller-manager
> 组件的--horizontal-pod-autoscaler-downscale-stabilization
> 参数来设置一个持续时间，用于指定在当前操作完成后，HPA
> 必须等待多长时间才能执行另一次缩放操作。默认为5分钟，也就是默认需要等待5分钟后才会开始自动缩放。


要使用基于**内存或者自定义指标**进行扩缩容（现在的版本都必须依赖 metrics-server 这个项目）。现在我们再用 Deployment 来创建一个 Nginx Pod，然后利用 HPA 来进行自动扩缩容

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hpa-mem-demo
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      volumes:
      - name: increase-mem-script
        configMap:
          name: increase-mem-config
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        volumeMounts:
        - name: increase-mem-script
          mountPath: /etc/script
        resources:
          requests:
            memory: 50Mi
            cpu: 50m
        securityContext:
          privileged: true
```

这里和前面普通的应用有一些区别，我们将一个名为 increase-mem-config 的 ConfigMap 资源对象挂载到了容器中，该配置文件是用于后面增加容器内存占用的脚本

```bash
apiVersion: v1
kind: ConfigMap
metadata:
  name: increase-mem-config
data:
  increase-mem.sh: |
    #!/bin/bash  
    mkdir /tmp/memory  
    mount -t tmpfs -o size=40M tmpfs /tmp/memory  
    dd if=/dev/zero of=/tmp/memory/block  
    sleep 60 
    rm /tmp/memory/block  
    umount /tmp/memory  
    rmdir /tmp/memory
```
这里congigmap是个脚本

> 由于这里增加内存的脚本需要使用到 mount 命令，这需要声明为特权模式，所以我们添加了
> securityContext.privileged=true 这个配置


然后需要创建一个基于内存的 HPA 资源对象

```bash
apiVersion: autoscaling/v2beta1
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-hpa
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: hpa-mem-demo
  minReplicas: 1
  maxReplicas: 5
  metrics:
  - type: Resource   #资源类型指标
    resource:
      name: memory
      targetAverageUtilization: 60   #百分比
```


要注意这里使用的 apiVersion 是 autoscaling/v2beta1

到这里证明 HPA 资源对象已经部署成功了，接下来我们对应用进行**压测**，将内存压上去，直接执行上面我们挂载到容器中的 increase-mem.sh 脚本即可：


```bash
$ kubectl exec -it hpa-mem-demo-66944b79bf-tqrn9 /bin/bash
root@hpa-mem-demo-66944b79bf-tqrn9:/# ls /etc/script/
increase-mem.sh
root@hpa-mem-demo-66944b79bf-tqrn9:/# source /etc/script/increase-mem.sh 
dd: writing to '/tmp/memory/block': No space left on device
81921+0 records in
81920+0 records out
41943040 bytes (42 MB, 40 MiB) copied, 0.584029 s, 71.8 MB/s
```

然后打开另外一个终端观察 HPA 资源对象的变化情况

当内存释放掉后，controller-manager 默认5分钟过后会进行缩放。

以上就是控制器的内容，主要是deployment，像statefulset和hpa有更好的方案实现，比如operator

附加：

```bash
[root@master tmp]# kubectl api-versions
admissionregistration.k8s.io/v1
admissionregistration.k8s.io/v1beta1
apiextensions.k8s.io/v1
apiextensions.k8s.io/v1beta1
apiregistration.k8s.io/v1
apiregistration.k8s.io/v1beta1
apps/v1
authentication.k8s.io/v1
authentication.k8s.io/v1beta1
authorization.k8s.io/v1
authorization.k8s.io/v1beta1
autoscaling/v1
autoscaling/v2beta1
autoscaling/v2beta2
batch/v1
batch/v1beta1
certificates.k8s.io/v1
certificates.k8s.io/v1beta1
coordination.k8s.io/v1
coordination.k8s.io/v1beta1
discovery.k8s.io/v1beta1
events.k8s.io/v1
events.k8s.io/v1beta1
extensions/v1beta1
flowcontrol.apiserver.k8s.io/v1beta1
networking.k8s.io/v1
networking.k8s.io/v1beta1
node.k8s.io/v1
node.k8s.io/v1beta1
policy/v1beta1
rbac.authorization.k8s.io/v1
rbac.authorization.k8s.io/v1beta1
scheduling.k8s.io/v1
scheduling.k8s.io/v1beta1
storage.k8s.io/v1
storage.k8s.io/v1beta1
v1

[root@master tmp]# kubectl api-resources
NAME                              SHORTNAMES   APIVERSION                             NAMESPACED   KIND
bindings                                       v1                                     true         Binding
componentstatuses                 cs           v1                                     false        ComponentStatus
configmaps                        cm           v1                                     true         ConfigMap
endpoints                         ep           v1                                     true         Endpoints
events                            ev           v1                                     true         Event
limitranges                       limits       v1                                     true         LimitRange
namespaces                        ns           v1                                     false        Namespace
nodes                             no           v1                                     false        Node
persistentvolumeclaims            pvc          v1                                     true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                     false        PersistentVolume
pods                              po           v1                                     true         Pod
podtemplates                                   v1                                     true         PodTemplate
replicationcontrollers            rc           v1                                     true         ReplicationController
resourcequotas                    quota        v1                                     true         ResourceQuota
secrets                                        v1                                     true         Secret
serviceaccounts                   sa           v1                                     true         ServiceAccount
services                          svc          v1                                     true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1        false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1        false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1                false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1              false        APIService
controllerrevisions                            apps/v1                                true         ControllerRevision
daemonsets                        ds           apps/v1                                true         DaemonSet
deployments                       deploy       apps/v1                                true         Deployment
replicasets                       rs           apps/v1                                true         ReplicaSet
statefulsets                      sts          apps/v1                                true         StatefulSet
tokenreviews                                   authentication.k8s.io/v1               false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1                true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1                false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1                false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1                false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v1                         true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1beta1                          true         CronJob
jobs                                           batch/v1                               true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1                 false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1                 true         Lease
endpointslices                                 discovery.k8s.io/v1beta1               true         EndpointSlice
events                            ev           events.k8s.io/v1                       true         Event
ingresses                         ing          extensions/v1beta1                     true         Ingress
flowschemas                                    flowcontrol.apiserver.k8s.io/v1beta1   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1beta1   false        PriorityLevelConfiguration
ingressclasses                                 networking.k8s.io/v1                   false        IngressClass
ingresses                         ing          networking.k8s.io/v1                   true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1                   true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                         false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1beta1                         true         PodDisruptionBudget
podsecuritypolicies               psp          policy/v1beta1                         false        PodSecurityPolicy
clusterrolebindings                            rbac.authorization.k8s.io/v1           false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1           false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1           true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1           true         Role
priorityclasses                   pc           scheduling.k8s.io/v1                   false        PriorityClass
csidrivers                                     storage.k8s.io/v1                      false        CSIDriver
csinodes                                       storage.k8s.io/v1                      false        CSINode
storageclasses                    sc           storage.k8s.io/v1                      false        StorageClass
volumeattachments                              storage.k8s.io/v1                      false        VolumeAttachment

[root@master tmp]# kubectl explain hpa --api-version=autoscaling/v2beta1
KIND:     HorizontalPodAutoscaler
VERSION:  autoscaling/v2beta1

DESCRIPTION:
     HorizontalPodAutoscaler is the configuration for a horizontal pod
     autoscaler, which automatically manages the replica count of any resource
     implementing the scale subresource based on the metrics specified.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     metadata is the standard object metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     spec is the specification for the behaviour of the autoscaler. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status.

   status       <Object>
     status is the current information about the autoscaler.

```

**HPA控制扩缩容，要使用到metrics server的来计算指标比如cpu、内存，hps得知道自己控制了那些pod，通过控制器能够指定了deployment或者rs，但实际上不是通过控制器这层来匹配pod的，真正匹配pod的是标签，那么有个问题，对于原本就存在切有特殊任务设置的pod的标签跟你打算用来给hpa匹配pod的标签一样是，你的hpa能否可以影响这些原本就存在而且设置特殊不愿意被扩缩容的pod呢?我就遇到过这个坑，发现是会有错误的，所以，使用hpa时，最好标签是唯一的不同的**
