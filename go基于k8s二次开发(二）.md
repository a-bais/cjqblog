---
title: go基于k8s二次开发(二）
date: 2022-06-14 14:43:12
tags: golang kubernetes docker
categories: go和微服务零散
---

<!--more-->

@[TOC]
# client创建资源
接下来用代码创建资源，先将前面的main函数改个名比如list()，作为一个模块，都是在同一个目录下，同一个main包，下面创建main函数，实现这个创建资源的操作，因为go run需要有main函数存在
(模块化编程)

## 创建deployment
先放源码
```bash
package main

import (
        "context"
        "fmt"
        appsV1 "k8s.io/api/apps/v1"
        coreV1 "k8s.io/api/core/v1"
        metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
        "k8s.io/client-go/kubernetes"
        "k8s.io/client-go/tools/clientcmd"
)

func cretaeDeploy() {
        configPath := "etc/kube.conf"
        config, _ := clientcmd.BuildConfigFromFlags("", configPath)
        clientset, _ := kubernetes.NewForConfig(config)
        namespace := "default"
        var replicas int32 = 3
        deployment := &appsV1.Deployment{
                ObjectMeta: metaV1.ObjectMeta{
                        Name: "nginx",
                        Labels: map[string]string{
                                "app": "nginx",
                                "env": "dev",
                        },
                },
                Spec: appsV1.DeploymentSpec{
                        Replicas: &replicas,   //这里要传递的是指针
                        Selector: &metaV1.LabelSelector{
                                MatchLabels: map[string]string{
                                        "app": "nginx",
                                        "env": "dev",
                                },
                        },
                        Template: coreV1.PodTemplateSpec{
                                ObjectMeta: metaV1.ObjectMeta{
                                        Name: "nginx",
                                        Labels: map[string]string{
                                                "app": "nginx",
                                                "env": "dev",
                                        },
                                },
                                Spec: coreV1.PodSpec{
                                        Containers: []coreV1.Container{
                                                {
                                                        Name:  "nginx",
                                                        Image: "nginx:1.20.1",
                                                        Ports: []coreV1.ContainerPort{
                                                                {
                                                                        Name:          "http",
                                                                        Protocol:      coreV1.ProtocolTCP,
                                                                        ContainerPort: 80,
                                                                },
                                                                {
                                                                        Name:          "https",
                                                                        Protocol:      coreV1.ProtocolTCP,
                                                                        ContainerPort: 443,
                                                                },
                                                        },
                                                },
                                        },
                                },
                        },
                },
        }
        deployment, _ = clientset.AppsV1().Deployments(namespace).Create(context.TODO(), deployment, metaV1.CreateOptions{})
        fmt.Println(deployment)
}

```

create方法需要的参
![在这里插入图片描述](https://img-blog.csdnimg.cn/e0c07e8bd961407d9ea3418451f76aaa.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6db75b9406ca40c4b35d75194f9e8908.png)
看源码可以知道client-go创建deployment主要是写这些信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/e7060443d73e42d5a23646278ab35ed2.png)
可以看到的ObjectMeta结构体中可以定义哪些内容

都是这样，详细的内容记不住就看源码或者帮助文档
## 创建service

```bash
package main

import (
        "client-go/tools/clientcmd"
        "context"
        "fmt"
        coreV1 "k8s.io/api/core/v1"
        metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
        "k8s.io/client-go/kubernetes"
)

func createService() {
        configPath := "etc/kube.conf"
        config, _ := clientcmd.BuildConfigFromFlags("", configPath)
        clientset, _ := kubernetes.NewForConfig(config)
        namespace := "default"
        service := &coreV1.Service{
                ObjectMeta: metaV1.ObjectMeta{
                        Name: "nginx-service",
                        Labels: map[string]string{
                                "env": "dev",
                        },
                },
                Spec: coreV1.ServiceSpec{
                        Type: coreV1.ServiceTypeNodePort,
                        Selector: map[string]string{
                                "env": "dev",
                                "app": "nginx",
                        },
                        Ports: []coreV1.ServicePort{
                                {
                                        Name:     "http",
                                        Port:     80,
                                        Protocol: coreV1.ProtocolTCP,
                                },
                        },
                },
        }
        service, _ = clientset.CoreV1().Services(namespace).Create(context.TODO(), service, metaV1.CreateOptions{})
        fmt.Println(service)
}
```

