---
title: repository与registry与harbor关系理清
date: 2021-10-11 10:47:49
tags: docker
categories: docker学习笔记
---

<!--more-->

repository,docker的镜像仓库。
registry,注册登记处，即注册服务器，存放着多个docker镜像仓库。
每个镜像仓库中一般存放着某一类的镜像，其中有多个镜像文件，不同的文件用tag进行区分
"harbor是用来存储和分发docker镜像的企业级registry服务器"
一般会用harbor替代registry

docker默认的仓库docker hub有docker-registry的镜像，用来生成registry容器，其中存放镜像的目录一般是/var/lib/registry
