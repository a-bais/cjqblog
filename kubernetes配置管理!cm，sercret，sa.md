---
title: kubernetes配置管理:cm，sercret，sa
date: 2022-04-25 22:50:51
tags: kubernetes
categories: 总复习:云原生运维开发知识点
---

<!--more-->

@[TOC]
# ConfigMap
可变配置管理

前面我们学习了一些常用的资源对象的使用，但是单纯依靠这些资源对象，还不足以满足我们的日常需求，一个重要的需求就是应用的配置管理、敏感信息的存储和使用（如：密码、Token 等）、容器运行资源的配置、安全管控、身份认证等等。

对于应用的可变配置在 Kubernetes 中是通过一个 ConfigMap 资源对象来实现的，我们知道许多应用经常会有从**配置文件**、命令行参数或者环境变量中读取一些配置信息的需求，这些配置信息我们肯定不会直接写死到应用程序中去的，比如你一个应用连接一个 redis 服务，下一次想更换一个了的，还得重新去修改代码，重新制作一个镜像，这肯定是不可取的，而 ConfigMap 就给我们提供了向容器中注入配置信息的能力，不仅可以用来保存单个属性，还可以用来保存整个配置文件，比如我们可以用来配置一个 redis 服务的访问地址，也可以用来保存整个 redis 的配置文件。接下来我们就来了解下 ConfigMap 这种资源对象的使用方法。

**一般都是用来做配置文件，或者存放些铭感信息密码或者token**

## 创建
ConfigMap 资源对象使用 key-value 形式的键值对来配置数据，这些数据可以在 Pod 里面使用

```bash
kind: ConfigMap
apiVersion: v1
metadata:
  name: cm-demo
  namespace: default
data:
  data.1: hello
  data.2: world
  config: |
    property.1=value-1
    property.2=value-2
    property.3=value-3
```

文件名是config，是key，文件内容是value

其中配置数据在 data 属性下面进行配置，前两个被用来保存单个属性，后面一个被用来保存一个**配置文件**

create或apply yaml文件即可

### 命令行创建cm
通过命令行来创建cm也不少：

```bash
可以使用kubectl create configmap -h来查看关于创建 ConfigMap 的帮助信息：


Examples:
  # Create a new configmap named my-config based on folder bar
  kubectl create configmap my-config --from-file=path/to/bar

  # Create a new configmap named my-config with specified keys instead of file basenames on disk
  kubectl create configmap my-config --from-file=key1=/path/to/bar/file1.txt --from-file=key2=/path/to/bar/file2.txt

  # Create a new configmap named my-config with key1=config1 and key2=config2
  kubectl create configmap my-config --from-literal=key1=config1 --from-literal=key2=config2
```

我们可以看到可以从一个给定的目录来创建一个 ConfigMap 对象，比如我们有一个 testcm 的目录，该目录下面包含一些配置文件，redis 和 mysql 的连接信息，如下：


```bash
$ ls testcm
redis.conf
mysql.conf

$ cat testcm/redis.conf
host=127.0.0.1
port=6379

$ cat testcm/mysql.conf
host=127.0.0.1
port=3306
```

```bash
kubectl create configmap cm-demo1 --from-file=testcm

其中 from-file 参数指定在该目录下面的所有文件都会被用在 ConfigMap 里面创建一个键值对，键的名字就是文件名，值就是文件的内容

kubectl describe configmap cm-demo1
Name:         cm-demo1
Namespace:    default
Labels:       <none>
Annotations:  <none>

Data
====
mysql.conf:
----
host=127.0.0.1
port=3306

redis.conf:
----
host=127.0.0.1
port=6379

Events:  <none>
```

如果信息太大，展示不完整，用kubectl get(建议使用它查看)

```bash
$ kubectl get configmap cm-demo1 -o yaml
apiVersion: v1
data:
  mysql.conf: |
    host=127.0.0.1
    port=3306
  redis.conf: |
    host=127.0.0.1
    port=6379
kind: ConfigMap
metadata:
  creationTimestamp: 2018-06-14T16:24:36Z
  name: cm-demo1
  namespace: default
  resourceVersion: "3109975"
  selfLink: /api/v1/namespaces/default/configmaps/cm-demo1
  uid: 6e0f4d82-6fef-11e8-a101-525400db4df7
```

```bash
除了通过文件目录进行创建，我们也可以使用指定的文件进行创建 ConfigMap，同样的，以上面的配置文件为例，我们创建一个 redis 的配置的一个单独 ConfigMap 对象：


$ kubectl create configmap cm-demo2 --from-file=testcm/redis.conf
configmap "cm-demo2" created
$ kubectl get configmap cm-demo2 -o yaml
apiVersion: v1
data:
  redis.conf: |
    host=127.0.0.1
    port=6379
kind: ConfigMap
metadata:
  creationTimestamp: 2018-06-14T16:34:29Z
  name: cm-demo2
  namespace: default
  resourceVersion: "3110758"
  selfLink: /api/v1/namespaces/default/configmaps/cm-demo2
  uid: cf59675d-6ff0-11e8-a101-525400db4df7
  
我们可以看到一个关联 redis.conf 文件配置信息的 ConfigMap 对象创建成功了，另外值得注意的是 --from-file 这个参数可以使用多次，比如我们这里使用两次分别指定 redis.conf 和 mysql.conf 文件，就和直接指定整个目录是一样的效果了。

通过 --from-literal 参数传递配置信息，同样的，这个参数可以使用多次，格式如下：


$ kubectl create configmap cm-demo3 --from-literal=db.host=localhost --from-literal=db.port=3306
configmap "cm-demo3" created
$ kubectl get configmap cm-demo3 -o yaml
apiVersion: v1
data:
  db.host: localhost
  db.port: "3306"
kind: ConfigMap
metadata:
  creationTimestamp: 2018-06-14T16:43:12Z
  name: cm-demo3
  namespace: default
  resourceVersion: "3111447"
  selfLink: /api/v1/namespaces/default/configmaps/cm-demo3
  uid: 06eeec7e-6ff2-11e8-a101-525400db4df7
```

## 使用

ConfigMap 创建成功了，那么我们应该怎么在 Pod 中来使用呢？我们说 ConfigMap 这些配置数据可以通过很多种方式在 Pod 里使用，主要有以下几种方式：

> 设置环境变量的值 
> 在容器里设置命令行参数 
> 在数据卷里面挂载配置文件

使用 ConfigMap 来填充我们的环境变量


```bash
apiVersion: v1
kind: Pod
metadata:
  name: testcm1-pod
spec:
  containers:
    - name: testcm1
      image: busybox
      command: [ "/bin/sh", "-c", "env" ]
      env:
        - name: DB_HOST
          valueFrom:
            configMapKeyRef:
              name: cm-demo3
              key: db.host
        - name: DB_PORT
          valueFrom:
            configMapKeyRef:
              name: cm-demo3
              key: db.port
      envFrom:
        - configMapRef:
            name: cm-demo1
```

```bash
env:
- name:
  valueFrom:
    filedRef:
      filedPath:

env:
- name:
  valueFrom:
    configMapKeyRef:
      name:
      key:
```

这个 Pod 运行后会输出如下所示的信息：


```bash
$ kubectl logs testcm1-pod
......
DB_HOST=localhost
DB_PORT=3306
mysql.conf=host=127.0.0.1
port=3306
redis.conf=host=127.0.0.1
port=6379
......
```

我们可以看到 DB_HOST 和 DB_PORT 都已经正常输出了，另外的环境变量是因为我们这里直接把 cm-demo1 给注入进来了，所以把他们的整个键值给输出出来了，这也是符合预期的。

另外我们也可以使用 ConfigMap来设置命令行参数，ConfigMap 也可以被用来设置容器中的命令或者参数值，如下 Pod:


```bash
apiVersion: v1
kind: Pod
metadata:
  name: testcm2-pod
spec:
  containers:
    - name: testcm2
      image: busybox
      command: [ "/bin/sh", "-c", "echo $(DB_HOST) $(DB_PORT)" ]
      env:
        - name: DB_HOST
          valueFrom:
            configMapKeyRef:
              name: cm-demo3
              key: db.host
        - name: DB_PORT
          valueFrom:
            configMapKeyRef:
              name: cm-demo3
              key: db.port
```

运行这个 Pod 后会输出如下信息：


```bash
$ kubectl logs testcm2-pod
localhost 3306
```

**另外一种是非常常见的使用 ConfigMap 的方式**：通过数据卷使用，在数据卷里面使用 ConfigMap，就是将文件填入数据卷，在这个文件中，键就是文件名，键值就是文件内容


```bash
apiVersion: v1
kind: Pod
metadata:
  name: testcm3-pod
spec:
  volumes:
    - name: config-volume
      configMap:   #configmap类型的volume，包括前面的有hostpath,emptydir,configmap
        name: cm-demo2
  containers:
    - name: testcm3
      image: busybox
      command: [ "/bin/sh", "-c", "cat /etc/config/redis.conf" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
```

运行这个 Pod 的，查看日志：


```bash
$ kubectl logs testcm3-pod
host=127.0.0.1
port=6379
```

当然我们也可以在 ConfigMap 值被映射的数据卷里去控制路径，如下 Pod 定义：


```bash
apiVersion: v1
kind: Pod
metadata:
  name: testcm4-pod
spec:
  volumes:
    - name: config-volume
      configMap:
        name: cm-demo1
        items:
        - key: mysql.conf
          path: path/to/msyql.conf
  containers:
    - name: testcm4
      image: busybox
      command: [ "/bin/sh","-c","cat /etc/config/path/to/msyql.conf" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
```

运行这个Pod的，查看日志：


```bash
$ kubectl logs testcm4-pod
host=127.0.0.1
port=3306
```

另外需要注意的是，当 ConfigMap 以数据卷的形式挂载进 Pod 的时，这时更新 ConfigMap（或删掉重建ConfigMap），Pod 内挂载的配置信息会热更新。这时可以增加一些监测配置文件变更的脚本，然后重加载对应服务就可以实现应用的热更新。

> 使用注意
> 
> 只有通过 Kubernetes API 创建的 Pod 才能使用 ConfigMap，其他方式创建的（比如静态
> Pod）不能使用；ConfigMap 文件大小限制为 1MB（ETCD 的要求）。

# Secret
敏感信息配置管理

前文我们学习 ConfigMap 的时候，我们说 ConfigMap 这个资源对象是 Kubernetes 当中非常重要的一个资源对象，一般情况下 ConfigMap 是用来存储一些非安全的配置信息，如果涉及到一些安全相关的数据的话用 ConfigMap 就非常不妥了，**因为 ConfigMap 是明文存储的，这个时候我们就需要用到另外一个资源对象了：Secret，Secret用来保存敏感信息，例如密码、OAuth 令牌和 ssh key 等等**，将这些信息放在 Secret 中比放在 Pod 的定义中或者 Docker 镜像中要更加安全和灵活。

> Secret 主要使用的有以下三种类型：
> 
> Opaque：base64 编码格式的 Secret，用来存储密码、密钥等；但数据也可以通过base64–decode解码得到原始数据，所有加密性很弱。 kubernetes.io/dockerconfigjson：用来存储私有docker registry的认证信息。 
> kubernetes.io/service-account-token：用于 ServiceAccount,ServiceAccount 创建时 Kubernetes 会默认创建一个对应的 Secret 对象，Pod 如果使用了ServiceAccount，对应的 Secret 会自动挂载到 Pod 目录/run/secrets/kubernetes.io/serviceaccount 中。
> bootstrap.kubernetes.io/token：用于节点接入集群的校验的 Secret

## Opaque Secret
opaque 不透明的
Opaque 类型的数据是一个 map 类型(字典类型，键值对），要求 value 必须是 base64 编码格式，比如我们来创建一个用户名为 admin，密码为 admin321 的 Secret 对象，首先我们需要先把用户名和密码做 base64 编码

**进行base64编码时，如果使用echo要有-n去掉结尾的换行符，base64解码则不用**

```bash
$ echo -n "admin" | base64
YWRtaW4=
$ echo -n "admin321" | base64
YWRtaW4zMjE=
```

```bash
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  username: YWRtaW4=
  password: YWRtaW4zMjE=
```
apply即可

```bash
查看
kubectl get secret

一样可以用describe和get -o yaml查看详情
```

> 创建好 Secret对象后，有两种方式来使用它：
> 
> 以环境变量的形式 
> 以Volume的形式挂载


## 使用方式环境变量

```bash
apiVersion: v1
kind: Pod
metadata:
  name: secret1-pod
spec:
  containers:
  - name: secret1
    image: busybox
    command: [ "/bin/sh", "-c", "env" ]
    env:
    - name: USERNAME
      valueFrom:
        secretKeyRef:
          name: mysecret
          key: username
    - name: PASSWORD
      valueFrom:
        secretKeyRef:
          name: mysecret
          key: password

apply即可，这里用了命令env列出，所以可以用logs列出

```

## 使用方式volume挂载

```bash
apiVersion: v1
kind: Pod
metadata:
  name: secret2-pod
spec:
  containers:
  - name: secret2
    image: busybox
    command: ["/bin/sh", "-c", "ls /etc/secrets"]
    volumeMounts:
    - name: secrets
      mountPath: /etc/secrets
  volumes:
  - name: secrets
    secret:
     secretName: mysecret
```

可以看到 Secret 把两个 key 挂载成了两个对应的文件

在 secretName 下面添加 items 指定 key 和 path，可以用来改变挂载进来后文件的路径，当然是给予挂载的目录，在该目录下的路径，比如目录是/tmp/1，挂载的文件是test，可以改成/tmp/1/2/test

# kubernetes.io/dockerconfigjson
除了上面的 Opaque 这种类型外，我们还可以来创建用户 **docker registry** 认证的 Secret，直接使用`kubectl create 命令创建即可，如下：


```bash
$ kubectl create secret docker-registry myregistry --docker-server=DOCKER_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
secret "myregistry" created
```

除了上面这种方法之外，我们也可以通过指定文件的方式来创建镜像仓库认证信息，需要注意对应的 KEY 和 TYPE：


```bash
$ kubectl create secret generic myregistry --from-file=.dockerconfigjson=/root/.docker/config.json --type=kubernetes.io/dockerconfigjson
本来文件名是key，这里用.dockerconfigjson，value是/root/.docker/config.json的内容，就是写私有仓库的登录信息，用户名，仓库地址，密码。email
```

**创建secret时不指定类型那么就是opaque类型，generic是却缺省的**还有一个tls的命令，用来创建tls secret

然后查看 Secret 列表：


```bash
$ kubectl get secret
NAME                  TYPE                                  DATA      AGE
default-token-n9w2d   kubernetes.io/service-account-token   3         33d
myregistry            kubernetes.io/dockerconfigjson        1         15s
mysecret              Opaque                                2         34m
```


```bash
$ kubectl get secret myregistry -o yaml
apiVersion: v1
data:
  .dockerconfigjson: eyJhdXRocyI6eyJET0NLRVJfU0VSVkVSIjp7InVzZXJuYW1lIjoiRE9DS0VSX1VTRVIiLCJwYXNzd29yZCI6IkRPQ0tFUl9QQVNTV09SRCIsImVtYWlsIjoiRE9DS0VSX0VNQUlMIiwiYXV0aCI6IlJFOURTMFZTWDFWVFJWSTZSRTlEUzBWU1gxQkJVMU5YVDFKRSJ9fX0=
kind: Secret
metadata:
  creationTimestamp: 2018-06-19T16:01:05Z
  name: myregistry
  namespace: default
  resourceVersion: "3696966"
  selfLink: /api/v1/namespaces/default/secrets/myregistry
  uid: f91db707-73d9-11e8-a101-525400db4df7
type: kubernetes.io/dockerconfigjson

$ echo eyJhdXRocyI6eyJET0NLRVJfU0VSVkVSIjp7InVzZXJuYW1lIjoiRE9DS0VSX1VTRVIiLCJwYXNzd29yZCI6IkRPQ0tFUl9QQVNTV09SRCIsImVtYWlsIjoiRE9DS0VSX0VNQUlMIiwiYXV0aCI6IlJFOURTMFZTWDFWVFJWSTZSRTlEUzBWU1gxQkJVMU5YVDFKRSJ9fX0= | base64 -d
{"auths":{"DOCKER_SERVER":{"username":"DOCKER_USER","password":"DOCKER_PASSWORD","email":"DOCKER_EMAIL","auth":"RE9DS0VSX1VTRVI6RE9DS0VSX1BBU1NXT1JE"}}}
```

```bash
如果我们需要拉取私有仓库中的 Docker 镜像的话就需要使用到上面的 myregistry 这个 Secret：


apiVersion: v1
kind: Pod
metadata:
  name: foo
spec:
  containers:
  - name: foo
    image: 192.168.1.100:5000/test:v1
  imagePullSecrets:
  - name: myregistry
```
**ImagePullSecrets 与 Secrets 不同，因为 Secrets 可以挂载到 Pod 中，但是 ImagePullSecrets 只能由 Kubelet 访问。镜像下载也是y由kubelet控制(容器的一切都是由kubelet控制)，这里不用挂载secret进去。**
我们需要拉取私有仓库镜像 192.168.1.100:5000/test:v1，我们就需要针对该私有仓库来创建一个如上的 Secret，然后在 Pod 中指定 imagePullSecrets。当然你要改成拉取公有的镜像仓库也可以，公有的仓库不用secret。

除了设置 Pod.spec.imagePullSecrets 这种方式来获取私有镜像之外，我们还可以通过**在 ServiceAccount 中设置 imagePullSecrets，然后就会自动为使用该 SA 的 Pod 注入 imagePullSecrets** 信息：


```bash
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: "2019-11-08T12:00:04Z"
  name: default
  namespace: default
  resourceVersion: "332"
  selfLink: /api/v1/namespaces/default/serviceaccounts/default
  uid: cc37a719-c4fe-4ebf-92da-e92c3e24d5d0
secrets:
- name: default-token-5tsh4
imagePullSecrets:
- name: myregistry
```

# kubernetes.io/service-account-token
另外一种 Secret 类型就是 kubernetes.io/service-account-token，**用于被 ServiceAccount 引用**。**ServiceAccout 创建时 Kubernetes 会默认创建对应的 Secret。Pod 如果使用了 ServiceAccount，对应的 Secret 会自动挂载到 Pod 的 /var/run/secrets/kubernetes.io/serviceaccount/ 目录中**(每个ns创建时会默认创建一个ns的default的sa)

```bash
$ kubectl run secret-pod3 --image nginx:1.7.9
deployment.apps "secret-pod3" created
$ kubectl get pods
NAME                           READY     STATUS    RESTARTS   AGE
...
secret-pod3-78c8c76db8-7zmqm   1/1       Running   0          13s
...
我们可以直接查看这个 Pod 的详细信息：


    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: default-token-5tsh4
      readOnly: true
  ......
  serviceAccount: default
  serviceAccountName: default
  volumes:
  - name: default-token-5tsh4
    secret:
      defaultMode: 420
      secretName: default-token-5tsh4
可以看到默认把名为 default（自动创建的）的 ServiceAccount 对应的 Secret 对象通过 Volume 挂载到了容器的 /var/run/secrets/kubernetes.io/serviceaccount 的目录中，对于该目录的作用我们会在后面的安全章节中继续和大家学习。
```

**Secret vs ConfigMap**
最后我们来对比下 Secret 和 ConfigMap这两种资源对象的异同点：

相同点
key/value的形式
属于某个特定的命名空间
可以导出到环境变量
可以通过目录/文件形式挂载
通过 volume 挂载的配置信息均可热更新

不同点
Secret 可以被 ServerAccount 关联
Secret 可以存储 docker register 的鉴权信息，用在 ImagePullSecret 参数中，用于拉取私有仓库的镜像
Secret 支持 Base64 加密
Secret 分为 kubernetes.io/service-account-token、kubernetes.io/dockerconfigjson、Opaque 三种类型，而 Configmap 不区分类型

> 使用注意
> 
> 同样 Secret 文件大小限制为 1MB（ETCD 的要求）；Secret 虽然采用 Base64
> 编码，但是我们还是可以很方便解码获取到原始信息，所以对于非常重要的数据还是需要慎重考虑，可以考虑使用 Vault 来进行加密管理。

**base64其实不应该看成一种加密方式，应该当成编码方式来看待**

# ServiceAccount
ServiceAccount 主要是用于解决 Pod 在集群中的**身份认证问题**的。认证使用的授权信息其实就是利用前面我们讲到的一个类型为 kubernetes.io/service-account-token 进行管理的。

**介绍**
ServiceAccount 是命名空间级别的，每一个命名空间创建的时候就会自动创建一个名为 default 的 ServiceAccount 对象:


```bash
$ kubectl create ns kube-test
namespace/kube-test created
$ kubectl get sa -n kube-test
NAME      SECRETS   AGE
default   1         9s
$ kubectl get secret -n kube-test
NAME                  TYPE                                  DATA   AGE
default-token-vn4tr   kubernetes.io/service-account-token   3      2m27s
```

这个 ServiceAccount 会自动关联到一个 Secret 对象上：


```bash
$ kubectl get sa default -n kube-test  -o yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: "2019-11-23T04:19:47Z"
  name: default
  namespace: kube-test
  resourceVersion: "4297522"
  selfLink: /api/v1/namespaces/kube-test/serviceaccounts/default
  uid: 75b3314b-e949-4f7b-9450-9bcd89c8c972
secrets:
- name: default-token-vn4tr
```

这个 Secret 对象是 ServiceAccount 控制器自动创建的，我们可以查看这个关联的 Secret 对象信息：


```bash
$ kubectl get secret default-token-vn4tr -n kube-test -o yaml
apiVersion: v1
data:
  ca.crt: LS0tLS...
  namespace: a3ViZS10ZXN0
  token: ZXlKaG...
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: default
    kubernetes.io/service-account.uid: 75b3314b-e949-4f7b-9450-9bcd89c8c972
  creationTimestamp: "2019-11-23T04:19:47Z"
  name: default-token-vn4tr
  namespace: kube-test
  resourceVersion: "4297521"
  selfLink: /api/v1/namespaces/kube-test/secrets/default-token-vn4tr
  uid: e3e60f95-f255-471b-a6c0-600a3c0ee53a
type: kubernetes.io/service-account-token
```

在 data 区域我们可以看到有3个信息：

**ca.crt：用于校验服务端的证书信息
namespace：表示当前管理的命名空间
token：用于 Pod 身份认证的 Token**

前面我们也提到了默认情况下当前 namespace 下面的 Pod 会默认使用 default 这个 ServiceAccount，对应的 Secret(token,ca.crt,namespace) 会自动挂载到 Pod 的 /var/run/secrets/kubernetes.io/serviceaccount/ 目录中，这样我们就可以在 Pod 里面获取到用于身份认证的信息了。

**实现原理**
实际上这个自动挂载过程是在 Pod 创建的时候通过 Admisson Controller（准入控制器） 来实现的

**Admission Controller（准入控制）是 Kubernetes API Server 用于拦截请求的一种手段。Admission 可以做到对请求的资源对象进行校验，修改，Pod 创建时 Admission Controller 会根据指定的的 ServiceAccount（默认的 default）把对应的 Secret 挂载到容器中的固定目录下 /var/run/secrets/kubernetes.io/serviceaccount/。**

**然后当我们在 Pod 里面访问集群的时候，就可以默认利用挂载到 Pod 内部的 token 文件来认证 Pod 的身份，ca.crt 则用来校验服务端**。


代码中我们指定了 ServiceAccount 背后的 Secret 挂载到 Pod 里面的两个文件：token 和 ca.crt，然后通过环境变量获取到 APIServer 的访问地址（**前面我们提到过会把 Service 信息通过环境变量的方式注入到 Pod 中，就是downward api的内容，kubernetes会将当前的命名空间的service的信息注入到pod中，这些信息也默认以变量的方式注入到容器中，而pod的某些信息不是自动注入到容器的而是需要手动注入**），然后通过 ca.cart 校验服务端是否可信，最后服务端会根据我们提供的 token 文件对 Pod 进行认证。

**Pod 身份被认证合法过后，具体具有哪些资源的访问权限，就需要通过后面的 RBAC 来进行声明了**。所以我们在学习 Kubernetes 的权限认证的时候需要把整个流程弄清楚，ServiceAccount 是干嘛的？为什么还需要 RBAC？这些都是上下文关联的。

configmap，secret都是有namespace限制的
