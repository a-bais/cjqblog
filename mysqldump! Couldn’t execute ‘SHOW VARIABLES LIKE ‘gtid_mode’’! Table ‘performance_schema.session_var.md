---
title: mysqldump: Couldn’t execute ‘SHOW VARIABLES LIKE ‘gtid_mode’’: Table ‘performance_schema.session_var
date: 2021-08-11 21:36:57
tags: 
categories: 解决错误问题
---

<!--more-->

mysqldump: Couldn’t execute ‘SHOW VARIABLES LIKE ‘gtid_mode’’: Table ‘performance_schema.session_variables’ doesn’t exist (1146)

解决方法:

```bash
更新，我的用的是mysql5.7
mysql_upgrade -uroot -p --force
重启服务
systemctl restart mysqld
在执行mysqldump，正常运行
```

