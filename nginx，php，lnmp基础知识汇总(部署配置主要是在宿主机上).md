---
title: nginx，php，lnmp基础知识汇总(部署配置主要是在宿主机上)
date: 2022-05-04 12:53:56
tags: nginx
categories: 总复习:云原生运维开发知识点
---

<!--more-->

@[TOC]
最近要对写云原生应用设置各类负载均衡等，想起之前学的nginx，就将笔记汇总下
# nginx
## 实战记录
[nginx常用模块之目录索引](https://blog.csdn.net/weixin_45843419/article/details/120360918)

[nginx常用模块之状态监控](https://blog.csdn.net/weixin_45843419/article/details/120442558)

[nginx常用模块之访问控制](https://blog.csdn.net/weixin_45843419/article/details/120443455)

[nginx常用模块之访问限制](https://blog.csdn.net/weixin_45843419/article/details/120444726)

[nginx日志](https://blog.csdn.net/weixin_45843419/article/details/120453580)

[虚拟主机](https://blog.csdn.net/weixin_45843419/article/details/120464795)

[nginx的location详解](https://blog.csdn.net/weixin_45843419/article/details/120477273)

## 常见错误和注意事项
[lnmp拆分出php注意事项](https://blog.csdn.net/weixin_45843419/article/details/119643790)

[https://blog.csdn.net/weixin_45843419/article/details/119619430](https://blog.csdn.net/weixin_45843419/article/details/119619430)

[nginx做负载均衡时注意几点](https://blog.csdn.net/weixin_45843419/article/details/119777786)

[/etc/hosts下域名解析与nginx域名访问时的一个特殊情况](https://blog.csdn.net/weixin_45843419/article/details/120580665)

[配置nginx文件的server时使用if的注意](https://blog.csdn.net/weixin_45843419/article/details/119147789)

[nginx启动失败问题](https://blog.csdn.net/weixin_45843419/article/details/119147789)

[403forbiden](https://blog.csdn.net/weixin_45843419/article/details/119886902)

[keepalived中，开机后发现处于工作状态机的是BAVKUP,而MASTER正常](https://blog.csdn.net/weixin_45843419/article/details/119873047)

[nginx: [error] invalid PID number ““ in “/run/nginx.pid“](https://blog.csdn.net/weixin_45843419/article/details/119776675)

[nginx配置的一些注意点](https://blog.csdn.net/weixin_45843419/article/details/119449501)

# php
[php安装](https://blog.csdn.net/weixin_45843419/article/details/119546182)

# 学习lnmp的笔记
[LA/NMP架构](https://blog.csdn.net/weixin_45843419/article/details/118438228)

[web中间件，web服务器apache与nginx介绍](https://blog.csdn.net/weixin_45843419/article/details/118421715)

[Nginx服务搭建与配置(简单篇)](https://blog.csdn.net/weixin_45843419/article/details/118438016)

**这些nginx的配置，lnmp的搭建等等基本上环境都是在实体主机上，在当下的互联网的真实的生产环境，这种在主机上的部署方式已经显然跟不上云原生的发展趋势了，主要是理解负载均衡的理念及相关集群的架构，大致了解nginx的配置。**
# 专栏推荐
[推荐](https://blog.csdn.net/weixin_44953658/category_9968945.html)
