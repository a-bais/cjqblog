---
title: yum info，search,rpm -ql
date: 2021-07-30 10:24:55
tags: 
categories: 解决错误问题
---

<!--more-->

yum info :查询对应包名的包的信息,会区分Avariable和Installed packages
yum search:查询与包名相关的所有包的信息
rpm -ql:查询已经安装的包的信息
yum gorpulist:列出可安装的环境包组和普通包组以及已安装的包组
yum groupinstall,update,remove:操作包组时要添加@如'@Development tools'
