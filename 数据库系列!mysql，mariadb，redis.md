---
title: 数据库系列:mysql，mariadb，redis
date: 2022-05-23 19:25:04
tags: mariadb mysql 数据库
categories: 总复习:云原生运维开发知识点
---

<!--more-->

[mariadb部署](https://blog.csdn.net/weixin_45843419/article/details/120247960)
[mysql(笔记个人用)](https://blog.csdn.net/weixin_45843419/article/details/118410378)
[安装mysql的几种方法汇总与keepcache用法](https://blog.csdn.net/weixin_45843419/article/details/119543438)
[redis部署](https://blog.csdn.net/weixin_45843419/article/details/120270321)
[mysql安装](https://blog.csdn.net/weixin_45843419/article/details/120506740)
[redis学习](https://blog.csdn.net/weixin_44953658/category_11037354.html?spm=1001.2014.3001.5482)


# 接"mysql(笔记个人用)"

