---
title: mariadb安装·
date: 2021-09-12 11:27:08
tags: mysql
categories: 数据库mysql mariadb redis 常用软件的安装
---

<!--more-->

![在这里插入图片描述](https://img-blog.csdnimg.cn/690d89e9712d4abeb6c5d9a41c34c3f9.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f04a679f61f24d5b8f0a8bc66a31eda8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
看清楚说明，按顺序来
![在这里插入图片描述](https://img-blog.csdnimg.cn/2b7c5e214545499a81dc2f5ac78e326d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6d38a25820c34d47b00b6e8efa816cc4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)
执行前面的截图中的sed命令

![在这里插入图片描述](https://img-blog.csdnimg.cn/b5f5be24e55142f8999a5b82e526e15f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA6Zi_55m977yM,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash
[mariadb]
name = MariaDB
baseurl = http://mirror.ustc.edu.cn/yum/10.4/centos-amd64
gpgkey=https://mirrors.ustc.edu.cn/RPM-GPG-KEY-MariaDB
gpgcheck=1

```

> yum repolist
> yum install -y MariaDB-server MariaDB-client
> systemctl start mariadb
> systemctl enable mariadb
>
>初始化数据库
>mysql_secure_installation


```bash
[root@localhost yum.repos.d]# mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):              #可以直接回车
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n         #切换到UNIX套接字身份验证
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y      #设置密码
New password:                        #密码
Re-enter new password:               #密码
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y                      #移除匿名用户
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y                         #是否禁止root远程登录
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y                    #移测试士库
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y                                          #刷新
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[root@localhost yum.repos.d]# 

```

完毕

附:https://blog.csdn.net/weixin_45843419/article/details/119615804


